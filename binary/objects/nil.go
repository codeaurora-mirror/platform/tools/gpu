// Copyright (C) 2014 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package objects

import (
	"android.googlesource.com/platform/tools/gpu/binary"
	"android.googlesource.com/platform/tools/gpu/binary/registry"
)

type binaryClassNil struct{}

var (
	NilClass = &binaryClassNil{}
)

func init() {
	registry.Global.Add(NilClass)
}

func (class *binaryClassNil) ID() binary.ID                                { return binary.ID{} }
func (*binaryClassNil) New() binary.Object                                 { return nil }
func (*binaryClassNil) Encode(e binary.Encoder, obj binary.Object) error   { return nil }
func (*binaryClassNil) Decode(d binary.Decoder) (binary.Object, error)     { return nil, nil }
func (*binaryClassNil) DecodeTo(d binary.Decoder, obj binary.Object) error { return nil }
func (*binaryClassNil) Skip(d binary.Decoder) error                        { return nil }
