// Copyright (C) 2015 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cpp

import (
	"errors"
	"io/ioutil"
	"strings"

	"android.googlesource.com/platform/tools/gpu/build"
	"android.googlesource.com/platform/tools/gpu/log"
)

// ParseDepFile loads and parses the makefile dependency file generated by the
// C++ compiler. If parsing succeeds and all dependencies exist then
// ParseDepFile returns the list of file dependencies in deps and depsValid is
// true, otherwise depsValid is false.
func ParseDepFile(file build.File, env build.Environment) (deps build.FileSet, depsValid bool) {
	if !file.Exists() {
		// Dependency file not found - needs building.
		return build.FileSet{}, false
	}

	f, err := ioutil.ReadFile(file.Absolute())
	if err != nil {
		log.Errorf(env.Logger, "Could not read dependencies from '%s': %v", file, err)
		return build.FileSet{}, false
	}

	deps, err = parseDeps(string(f))
	if err != nil {
		log.Errorf(env.Logger, "Could not parse dependencies from '%s': %v", file, err)
		return build.FileSet{}, false
	}

	return deps, true
}

func parseDeps(s string) (build.FileSet, error) {
	p := strings.Split(s, ": ")
	if len(p) != 2 {
		return build.FileSet{}, errors.New("Parse failure - no colon found")
	}
	s = p[1]

	// Remove trailing '\' then newline
	s = strings.Replace(s, "\\\n", "", -1)
	s = strings.Replace(s, "\\\r\n", "", -1)

	deps := build.FileSet{}
	for _, s := range strings.Fields(s) {
		deps = deps.Append(build.File(s))
	}
	return deps, nil
}
