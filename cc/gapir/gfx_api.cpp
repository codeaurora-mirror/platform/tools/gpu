////////////////////////////////////////////////////////////////////////////////
// Automatically generated file. Do not modify!
////////////////////////////////////////////////////////////////////////////////

#include "gfx_api.h"
#include "interpreter.h"
#include "stack.h"

#include <gapic/get_gfx_proc_address.h>
#include <gapic/log.h>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

namespace gapir {
namespace gfxapi {
namespace {

bool callEglInitialize(Stack* stack, bool pushReturn) {
    int* minor = stack->pop<int*>();
    int* major = stack->pop<int*>();
    void* dpy = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("eglInitialize(%p, %p, %p)\n", dpy, major, minor);
        if (eglInitialize != nullptr) {
            int return_value = eglInitialize(dpy, major, minor);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<int>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("eglInitialize returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function eglInitialize\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function eglInitialize\n");
        return false;
    }
}

bool callEglCreateContext(Stack* stack, bool pushReturn) {
    int* attrib_list = stack->pop<int*>();
    void* share_context = stack->pop<void*>();
    void* config = stack->pop<void*>();
    void* display = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("eglCreateContext(%p, %p, %p, %p)\n", display, config, share_context,
                   attrib_list);
        if (eglCreateContext != nullptr) {
            void* return_value = eglCreateContext(display, config, share_context, attrib_list);
            GAPID_INFO("Returned: %p\n", return_value);
            if (pushReturn) {
                stack->push<void*>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("eglCreateContext returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function eglCreateContext\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function eglCreateContext\n");
        return false;
    }
}

bool callEglMakeCurrent(Stack* stack, bool pushReturn) {
    void* context = stack->pop<void*>();
    void* read = stack->pop<void*>();
    void* draw = stack->pop<void*>();
    void* display = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("eglMakeCurrent(%p, %p, %p, %p)\n", display, draw, read, context);
        if (eglMakeCurrent != nullptr) {
            int return_value = eglMakeCurrent(display, draw, read, context);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<int>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("eglMakeCurrent returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function eglMakeCurrent\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function eglMakeCurrent\n");
        return false;
    }
}

bool callEglSwapBuffers(Stack* stack, bool pushReturn) {
    void* surface = stack->pop<void*>();
    void* display = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("eglSwapBuffers(%p, %p)\n", display, surface);
        if (eglSwapBuffers != nullptr) {
            int return_value = eglSwapBuffers(display, surface);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<int>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("eglSwapBuffers returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function eglSwapBuffers\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function eglSwapBuffers\n");
        return false;
    }
}

bool callEglQuerySurface(Stack* stack, bool pushReturn) {
    int* value = stack->pop<int*>();
    int attribute = stack->pop<int>();
    void* surface = stack->pop<void*>();
    void* display = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("eglQuerySurface(%p, %p, %d, %p)\n", display, surface, attribute, value);
        if (eglQuerySurface != nullptr) {
            int return_value = eglQuerySurface(display, surface, attribute, value);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<int>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("eglQuerySurface returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function eglQuerySurface\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function eglQuerySurface\n");
        return false;
    }
}

bool callGlXCreateContext(Stack* stack, bool pushReturn) {
    bool direct = stack->pop<bool>();
    void* shareList = stack->pop<void*>();
    void* vis = stack->pop<void*>();
    void* dpy = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("glXCreateContext(%p, %p, %p, %d)\n", dpy, vis, shareList, direct);
        if (glXCreateContext != nullptr) {
            void* return_value = glXCreateContext(dpy, vis, shareList, direct);
            GAPID_INFO("Returned: %p\n", return_value);
            if (pushReturn) {
                stack->push<void*>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glXCreateContext returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glXCreateContext\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glXCreateContext\n");
        return false;
    }
}

bool callGlXCreateNewContext(Stack* stack, bool pushReturn) {
    bool direct = stack->pop<bool>();
    void* shared = stack->pop<void*>();
    uint32_t type = stack->pop<uint32_t>();
    void* fbconfig = stack->pop<void*>();
    void* display = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("glXCreateNewContext(%p, %p, %" PRIu32 ", %p, %d)\n", display, fbconfig, type,
                   shared, direct);
        if (glXCreateNewContext != nullptr) {
            void* return_value = glXCreateNewContext(display, fbconfig, type, shared, direct);
            GAPID_INFO("Returned: %p\n", return_value);
            if (pushReturn) {
                stack->push<void*>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glXCreateNewContext returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glXCreateNewContext\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glXCreateNewContext\n");
        return false;
    }
}

bool callGlXMakeContextCurrent(Stack* stack, bool pushReturn) {
    void* ctx = stack->pop<void*>();
    void* read = stack->pop<void*>();
    void* draw = stack->pop<void*>();
    void* display = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("glXMakeContextCurrent(%p, %p, %p, %p)\n", display, draw, read, ctx);
        if (glXMakeContextCurrent != nullptr) {
            int return_value = glXMakeContextCurrent(display, draw, read, ctx);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<int>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glXMakeContextCurrent returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glXMakeContextCurrent\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glXMakeContextCurrent\n");
        return false;
    }
}

bool callGlXMakeCurrent(Stack* stack, bool pushReturn) {
    void* ctx = stack->pop<void*>();
    void* drawable = stack->pop<void*>();
    void* display = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("glXMakeCurrent(%p, %p, %p)\n", display, drawable, ctx);
        if (glXMakeCurrent != nullptr) {
            int return_value = glXMakeCurrent(display, drawable, ctx);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<int>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glXMakeCurrent returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glXMakeCurrent\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glXMakeCurrent\n");
        return false;
    }
}

bool callGlXSwapBuffers(Stack* stack, bool pushReturn) {
    void* drawable = stack->pop<void*>();
    void* display = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("glXSwapBuffers(%p, %p)\n", display, drawable);
        if (glXSwapBuffers != nullptr) {
            glXSwapBuffers(display, drawable);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glXSwapBuffers returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glXSwapBuffers\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glXSwapBuffers\n");
        return false;
    }
}

bool callGlXQueryDrawable(Stack* stack, bool pushReturn) {
    int* value = stack->pop<int*>();
    int attribute = stack->pop<int>();
    void* draw = stack->pop<void*>();
    void* display = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("glXQueryDrawable(%p, %p, %d, %p)\n", display, draw, attribute, value);
        if (glXQueryDrawable != nullptr) {
            int return_value = glXQueryDrawable(display, draw, attribute, value);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<int>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glXQueryDrawable returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glXQueryDrawable\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glXQueryDrawable\n");
        return false;
    }
}

bool callWglCreateContext(Stack* stack, bool pushReturn) {
    void* hdc = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("wglCreateContext(%p)\n", hdc);
        if (wglCreateContext != nullptr) {
            void* return_value = wglCreateContext(hdc);
            GAPID_INFO("Returned: %p\n", return_value);
            if (pushReturn) {
                stack->push<void*>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("wglCreateContext returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function wglCreateContext\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function wglCreateContext\n");
        return false;
    }
}

bool callWglCreateContextAttribsARB(Stack* stack, bool pushReturn) {
    int* attribList = stack->pop<int*>();
    void* hShareContext = stack->pop<void*>();
    void* hdc = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("wglCreateContextAttribsARB(%p, %p, %p)\n", hdc, hShareContext, attribList);
        if (wglCreateContextAttribsARB != nullptr) {
            void* return_value = wglCreateContextAttribsARB(hdc, hShareContext, attribList);
            GAPID_INFO("Returned: %p\n", return_value);
            if (pushReturn) {
                stack->push<void*>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("wglCreateContextAttribsARB returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function wglCreateContextAttribsARB\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function wglCreateContextAttribsARB\n");
        return false;
    }
}

bool callWglMakeCurrent(Stack* stack, bool pushReturn) {
    void* hglrc = stack->pop<void*>();
    void* hdc = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("wglMakeCurrent(%p, %p)\n", hdc, hglrc);
        if (wglMakeCurrent != nullptr) {
            int return_value = wglMakeCurrent(hdc, hglrc);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<int>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("wglMakeCurrent returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function wglMakeCurrent\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function wglMakeCurrent\n");
        return false;
    }
}

bool callWglSwapBuffers(Stack* stack, bool pushReturn) {
    void* hdc = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("wglSwapBuffers(%p)\n", hdc);
        if (wglSwapBuffers != nullptr) {
            wglSwapBuffers(hdc);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("wglSwapBuffers returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function wglSwapBuffers\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function wglSwapBuffers\n");
        return false;
    }
}

bool callCGLCreateContext(Stack* stack, bool pushReturn) {
    void** ctx = stack->pop<void**>();
    void* share = stack->pop<void*>();
    void* pix = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("CGLCreateContext(%p, %p, %p)\n", pix, share, ctx);
        if (CGLCreateContext != nullptr) {
            int return_value = CGLCreateContext(pix, share, ctx);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<int>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("CGLCreateContext returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function CGLCreateContext\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function CGLCreateContext\n");
        return false;
    }
}

bool callCGLSetCurrentContext(Stack* stack, bool pushReturn) {
    void* ctx = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("CGLSetCurrentContext(%p)\n", ctx);
        if (CGLSetCurrentContext != nullptr) {
            int return_value = CGLSetCurrentContext(ctx);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<int>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("CGLSetCurrentContext returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function CGLSetCurrentContext\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function CGLSetCurrentContext\n");
        return false;
    }
}

bool callCGLGetSurface(Stack* stack, bool pushReturn) {
    int32_t* sid = stack->pop<int32_t*>();
    int32_t* wid = stack->pop<int32_t*>();
    void** cid = stack->pop<void**>();
    void* ctx = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("CGLGetSurface(%p, %p, %p, %p)\n", ctx, cid, wid, sid);
        if (CGLGetSurface != nullptr) {
            int return_value = CGLGetSurface(ctx, cid, wid, sid);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<int>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("CGLGetSurface returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function CGLGetSurface\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function CGLGetSurface\n");
        return false;
    }
}

bool callCGSGetSurfaceBounds(Stack* stack, bool pushReturn) {
    double* bounds = stack->pop<double*>();
    int32_t sid = stack->pop<int32_t>();
    int32_t wid = stack->pop<int32_t>();
    void* cid = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("CGSGetSurfaceBounds(%p, %" PRId32 ", %" PRId32 ", %p)\n", cid, wid, sid,
                   bounds);
        if (CGSGetSurfaceBounds != nullptr) {
            int return_value = CGSGetSurfaceBounds(cid, wid, sid, bounds);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<int>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("CGSGetSurfaceBounds returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function CGSGetSurfaceBounds\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function CGSGetSurfaceBounds\n");
        return false;
    }
}

bool callCGLFlushDrawable(Stack* stack, bool pushReturn) {
    void* ctx = stack->pop<void*>();
    if (stack->isValid()) {
        GAPID_INFO("CGLFlushDrawable(%p)\n", ctx);
        if (CGLFlushDrawable != nullptr) {
            int return_value = CGLFlushDrawable(ctx);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<int>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("CGLFlushDrawable returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function CGLFlushDrawable\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function CGLFlushDrawable\n");
        return false;
    }
}

bool callGlEnableClientState(Stack* stack, bool pushReturn) {
    GLenum type = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glEnableClientState(%u)\n", type);
        if (glEnableClientState != nullptr) {
            glEnableClientState(type);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glEnableClientState returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glEnableClientState\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glEnableClientState\n");
        return false;
    }
}

bool callGlDisableClientState(Stack* stack, bool pushReturn) {
    GLenum type = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glDisableClientState(%u)\n", type);
        if (glDisableClientState != nullptr) {
            glDisableClientState(type);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDisableClientState returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDisableClientState\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDisableClientState\n");
        return false;
    }
}

bool callGlGetProgramBinaryOES(Stack* stack, bool pushReturn) {
    void* binary = stack->pop<void*>();
    uint32_t* binary_format = stack->pop<uint32_t*>();
    int32_t* bytes_written = stack->pop<int32_t*>();
    int32_t buffer_size = stack->pop<int32_t>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetProgramBinaryOES(%" PRIu32 ", %" PRId32 ", %p, %p, %p)\n", program,
                   buffer_size, bytes_written, binary_format, binary);
        if (glGetProgramBinaryOES != nullptr) {
            glGetProgramBinaryOES(program, buffer_size, bytes_written, binary_format, binary);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetProgramBinaryOES returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetProgramBinaryOES\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetProgramBinaryOES\n");
        return false;
    }
}

bool callGlProgramBinaryOES(Stack* stack, bool pushReturn) {
    int32_t binary_size = stack->pop<int32_t>();
    void* binary = stack->pop<void*>();
    uint32_t binary_format = stack->pop<uint32_t>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glProgramBinaryOES(%" PRIu32 ", %" PRIu32 ", %p, %" PRId32 ")\n", program,
                   binary_format, binary, binary_size);
        if (glProgramBinaryOES != nullptr) {
            glProgramBinaryOES(program, binary_format, binary, binary_size);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glProgramBinaryOES returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glProgramBinaryOES\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glProgramBinaryOES\n");
        return false;
    }
}

bool callGlStartTilingQCOM(Stack* stack, bool pushReturn) {
    GLbitfield preserveMask = stack->pop<GLbitfield>();
    int32_t height = stack->pop<int32_t>();
    int32_t width = stack->pop<int32_t>();
    int32_t y = stack->pop<int32_t>();
    int32_t x = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glStartTilingQCOM(%" PRId32 ", %" PRId32 ", %" PRId32 ", %" PRId32 ", %u)\n", x,
                   y, width, height, preserveMask);
        if (glStartTilingQCOM != nullptr) {
            glStartTilingQCOM(x, y, width, height, preserveMask);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glStartTilingQCOM returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glStartTilingQCOM\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glStartTilingQCOM\n");
        return false;
    }
}

bool callGlEndTilingQCOM(Stack* stack, bool pushReturn) {
    GLbitfield preserve_mask = stack->pop<GLbitfield>();
    if (stack->isValid()) {
        GAPID_INFO("glEndTilingQCOM(%u)\n", preserve_mask);
        if (glEndTilingQCOM != nullptr) {
            glEndTilingQCOM(preserve_mask);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glEndTilingQCOM returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glEndTilingQCOM\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glEndTilingQCOM\n");
        return false;
    }
}

bool callGlDiscardFramebufferEXT(Stack* stack, bool pushReturn) {
    GLenum* attachments = stack->pop<GLenum*>();
    int32_t numAttachments = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glDiscardFramebufferEXT(%u, %" PRId32 ", %p)\n", target, numAttachments,
                   attachments);
        if (glDiscardFramebufferEXT != nullptr) {
            glDiscardFramebufferEXT(target, numAttachments, attachments);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDiscardFramebufferEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDiscardFramebufferEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDiscardFramebufferEXT\n");
        return false;
    }
}

bool callGlInsertEventMarkerEXT(Stack* stack, bool pushReturn) {
    char* marker = stack->pop<char*>();
    int32_t length = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glInsertEventMarkerEXT(%" PRId32 ", %p)\n", length, marker);
        if (glInsertEventMarkerEXT != nullptr) {
            glInsertEventMarkerEXT(length, marker);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glInsertEventMarkerEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glInsertEventMarkerEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glInsertEventMarkerEXT\n");
        return false;
    }
}

bool callGlPushGroupMarkerEXT(Stack* stack, bool pushReturn) {
    char* marker = stack->pop<char*>();
    int32_t length = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glPushGroupMarkerEXT(%" PRId32 ", %p)\n", length, marker);
        if (glPushGroupMarkerEXT != nullptr) {
            glPushGroupMarkerEXT(length, marker);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glPushGroupMarkerEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glPushGroupMarkerEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glPushGroupMarkerEXT\n");
        return false;
    }
}

bool callGlPopGroupMarkerEXT(Stack* stack, bool pushReturn) {
    if (stack->isValid()) {
        GAPID_INFO("glPopGroupMarkerEXT()\n");
        if (glPopGroupMarkerEXT != nullptr) {
            glPopGroupMarkerEXT();
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glPopGroupMarkerEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glPopGroupMarkerEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glPopGroupMarkerEXT\n");
        return false;
    }
}

bool callGlTexStorage1DEXT(Stack* stack, bool pushReturn) {
    int32_t width = stack->pop<int32_t>();
    GLenum format = stack->pop<GLenum>();
    int32_t levels = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glTexStorage1DEXT(%u, %" PRId32 ", %u, %" PRId32 ")\n", target, levels, format,
                   width);
        if (glTexStorage1DEXT != nullptr) {
            glTexStorage1DEXT(target, levels, format, width);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glTexStorage1DEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glTexStorage1DEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glTexStorage1DEXT\n");
        return false;
    }
}

bool callGlTexStorage2DEXT(Stack* stack, bool pushReturn) {
    int32_t height = stack->pop<int32_t>();
    int32_t width = stack->pop<int32_t>();
    GLenum format = stack->pop<GLenum>();
    int32_t levels = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glTexStorage2DEXT(%u, %" PRId32 ", %u, %" PRId32 ", %" PRId32 ")\n", target,
                   levels, format, width, height);
        if (glTexStorage2DEXT != nullptr) {
            glTexStorage2DEXT(target, levels, format, width, height);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glTexStorage2DEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glTexStorage2DEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glTexStorage2DEXT\n");
        return false;
    }
}

bool callGlTexStorage3DEXT(Stack* stack, bool pushReturn) {
    int32_t depth = stack->pop<int32_t>();
    int32_t height = stack->pop<int32_t>();
    int32_t width = stack->pop<int32_t>();
    GLenum format = stack->pop<GLenum>();
    int32_t levels = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glTexStorage3DEXT(%u, %" PRId32 ", %u, %" PRId32 ", %" PRId32 ", %" PRId32
                   ")\n",
                   target, levels, format, width, height, depth);
        if (glTexStorage3DEXT != nullptr) {
            glTexStorage3DEXT(target, levels, format, width, height, depth);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glTexStorage3DEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glTexStorage3DEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glTexStorage3DEXT\n");
        return false;
    }
}

bool callGlTextureStorage1DEXT(Stack* stack, bool pushReturn) {
    int32_t width = stack->pop<int32_t>();
    GLenum format = stack->pop<GLenum>();
    int32_t levels = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    uint32_t texture = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glTextureStorage1DEXT(%" PRIu32 ", %u, %" PRId32 ", %u, %" PRId32 ")\n",
                   texture, target, levels, format, width);
        if (glTextureStorage1DEXT != nullptr) {
            glTextureStorage1DEXT(texture, target, levels, format, width);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glTextureStorage1DEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glTextureStorage1DEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glTextureStorage1DEXT\n");
        return false;
    }
}

bool callGlTextureStorage2DEXT(Stack* stack, bool pushReturn) {
    int32_t height = stack->pop<int32_t>();
    int32_t width = stack->pop<int32_t>();
    GLenum format = stack->pop<GLenum>();
    int32_t levels = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    uint32_t texture = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glTextureStorage2DEXT(%" PRIu32 ", %u, %" PRId32 ", %u, %" PRId32 ", %" PRId32
                   ")\n",
                   texture, target, levels, format, width, height);
        if (glTextureStorage2DEXT != nullptr) {
            glTextureStorage2DEXT(texture, target, levels, format, width, height);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glTextureStorage2DEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glTextureStorage2DEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glTextureStorage2DEXT\n");
        return false;
    }
}

bool callGlTextureStorage3DEXT(Stack* stack, bool pushReturn) {
    int32_t depth = stack->pop<int32_t>();
    int32_t height = stack->pop<int32_t>();
    int32_t width = stack->pop<int32_t>();
    GLenum format = stack->pop<GLenum>();
    int32_t levels = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    uint32_t texture = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glTextureStorage3DEXT(%" PRIu32 ", %u, %" PRId32 ", %u, %" PRId32 ", %" PRId32
                   ", %" PRId32 ")\n",
                   texture, target, levels, format, width, height, depth);
        if (glTextureStorage3DEXT != nullptr) {
            glTextureStorage3DEXT(texture, target, levels, format, width, height, depth);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glTextureStorage3DEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glTextureStorage3DEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glTextureStorage3DEXT\n");
        return false;
    }
}

bool callGlGenVertexArraysOES(Stack* stack, bool pushReturn) {
    uint32_t* arrays = stack->pop<uint32_t*>();
    int32_t count = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGenVertexArraysOES(%" PRId32 ", %p)\n", count, arrays);
        if (glGenVertexArraysOES != nullptr) {
            glGenVertexArraysOES(count, arrays);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGenVertexArraysOES returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGenVertexArraysOES\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGenVertexArraysOES\n");
        return false;
    }
}

bool callGlBindVertexArrayOES(Stack* stack, bool pushReturn) {
    uint32_t array = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glBindVertexArrayOES(%" PRIu32 ")\n", array);
        if (glBindVertexArrayOES != nullptr) {
            glBindVertexArrayOES(array);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBindVertexArrayOES returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBindVertexArrayOES\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBindVertexArrayOES\n");
        return false;
    }
}

bool callGlDeleteVertexArraysOES(Stack* stack, bool pushReturn) {
    uint32_t* arrays = stack->pop<uint32_t*>();
    int32_t count = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glDeleteVertexArraysOES(%" PRId32 ", %p)\n", count, arrays);
        if (glDeleteVertexArraysOES != nullptr) {
            glDeleteVertexArraysOES(count, arrays);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDeleteVertexArraysOES returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDeleteVertexArraysOES\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDeleteVertexArraysOES\n");
        return false;
    }
}

bool callGlIsVertexArrayOES(Stack* stack, bool pushReturn) {
    uint32_t array = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glIsVertexArrayOES(%" PRIu32 ")\n", array);
        if (glIsVertexArrayOES != nullptr) {
            bool return_value = glIsVertexArrayOES(array);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<bool>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glIsVertexArrayOES returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glIsVertexArrayOES\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glIsVertexArrayOES\n");
        return false;
    }
}

bool callGlEGLImageTargetTexture2DOES(Stack* stack, bool pushReturn) {
    void* image = stack->pop<void*>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glEGLImageTargetTexture2DOES(%u, %p)\n", target, image);
        if (glEGLImageTargetTexture2DOES != nullptr) {
            glEGLImageTargetTexture2DOES(target, image);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glEGLImageTargetTexture2DOES returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glEGLImageTargetTexture2DOES\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glEGLImageTargetTexture2DOES\n");
        return false;
    }
}

bool callGlEGLImageTargetRenderbufferStorageOES(Stack* stack, bool pushReturn) {
    void* image = stack->pop<void*>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glEGLImageTargetRenderbufferStorageOES(%u, %p)\n", target, image);
        if (glEGLImageTargetRenderbufferStorageOES != nullptr) {
            glEGLImageTargetRenderbufferStorageOES(target, image);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glEGLImageTargetRenderbufferStorageOES returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING(
                    "Attempted to call unsupported function "
                    "glEGLImageTargetRenderbufferStorageOES\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glEGLImageTargetRenderbufferStorageOES\n");
        return false;
    }
}

bool callGlGetGraphicsResetStatusEXT(Stack* stack, bool pushReturn) {
    if (stack->isValid()) {
        GAPID_INFO("glGetGraphicsResetStatusEXT()\n");
        if (glGetGraphicsResetStatusEXT != nullptr) {
            GLenum return_value = glGetGraphicsResetStatusEXT();
            GAPID_INFO("Returned: %u\n", return_value);
            if (pushReturn) {
                stack->push<GLenum>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetGraphicsResetStatusEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetGraphicsResetStatusEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetGraphicsResetStatusEXT\n");
        return false;
    }
}

bool callGlBindAttribLocation(Stack* stack, bool pushReturn) {
    char* name = stack->pop<char*>();
    int32_t location = stack->pop<int32_t>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glBindAttribLocation(%" PRIu32 ", %" PRId32 ", %s)\n", program, location, name);
        if (glBindAttribLocation != nullptr) {
            glBindAttribLocation(program, location, name);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBindAttribLocation returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBindAttribLocation\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBindAttribLocation\n");
        return false;
    }
}

bool callGlBlendFunc(Stack* stack, bool pushReturn) {
    GLenum dst_factor = stack->pop<GLenum>();
    GLenum src_factor = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glBlendFunc(%u, %u)\n", src_factor, dst_factor);
        if (glBlendFunc != nullptr) {
            glBlendFunc(src_factor, dst_factor);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBlendFunc returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBlendFunc\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBlendFunc\n");
        return false;
    }
}

bool callGlBlendFuncSeparate(Stack* stack, bool pushReturn) {
    GLenum dst_factor_alpha = stack->pop<GLenum>();
    GLenum src_factor_alpha = stack->pop<GLenum>();
    GLenum dst_factor_rgb = stack->pop<GLenum>();
    GLenum src_factor_rgb = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glBlendFuncSeparate(%u, %u, %u, %u)\n", src_factor_rgb, dst_factor_rgb,
                   src_factor_alpha, dst_factor_alpha);
        if (glBlendFuncSeparate != nullptr) {
            glBlendFuncSeparate(src_factor_rgb, dst_factor_rgb, src_factor_alpha, dst_factor_alpha);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBlendFuncSeparate returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBlendFuncSeparate\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBlendFuncSeparate\n");
        return false;
    }
}

bool callGlBlendEquation(Stack* stack, bool pushReturn) {
    GLenum equation = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glBlendEquation(%u)\n", equation);
        if (glBlendEquation != nullptr) {
            glBlendEquation(equation);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBlendEquation returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBlendEquation\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBlendEquation\n");
        return false;
    }
}

bool callGlBlendEquationSeparate(Stack* stack, bool pushReturn) {
    GLenum alpha = stack->pop<GLenum>();
    GLenum rgb = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glBlendEquationSeparate(%u, %u)\n", rgb, alpha);
        if (glBlendEquationSeparate != nullptr) {
            glBlendEquationSeparate(rgb, alpha);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBlendEquationSeparate returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBlendEquationSeparate\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBlendEquationSeparate\n");
        return false;
    }
}

bool callGlBlendColor(Stack* stack, bool pushReturn) {
    float alpha = stack->pop<float>();
    float blue = stack->pop<float>();
    float green = stack->pop<float>();
    float red = stack->pop<float>();
    if (stack->isValid()) {
        GAPID_INFO("glBlendColor(%f, %f, %f, %f)\n", red, green, blue, alpha);
        if (glBlendColor != nullptr) {
            glBlendColor(red, green, blue, alpha);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBlendColor returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBlendColor\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBlendColor\n");
        return false;
    }
}

bool callGlEnableVertexAttribArray(Stack* stack, bool pushReturn) {
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glEnableVertexAttribArray(%" PRId32 ")\n", location);
        if (glEnableVertexAttribArray != nullptr) {
            glEnableVertexAttribArray(location);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glEnableVertexAttribArray returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glEnableVertexAttribArray\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glEnableVertexAttribArray\n");
        return false;
    }
}

bool callGlDisableVertexAttribArray(Stack* stack, bool pushReturn) {
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glDisableVertexAttribArray(%" PRId32 ")\n", location);
        if (glDisableVertexAttribArray != nullptr) {
            glDisableVertexAttribArray(location);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDisableVertexAttribArray returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDisableVertexAttribArray\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDisableVertexAttribArray\n");
        return false;
    }
}

bool callGlVertexAttribPointer(Stack* stack, bool pushReturn) {
    void* data = stack->pop<void*>();
    int32_t stride = stack->pop<int32_t>();
    bool normalized = stack->pop<bool>();
    GLenum type = stack->pop<GLenum>();
    int32_t size = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glVertexAttribPointer(%" PRId32 ", %" PRId32 ", %u, %d, %" PRId32 ", %p)\n",
                   location, size, type, normalized, stride, data);
        if (glVertexAttribPointer != nullptr) {
            glVertexAttribPointer(location, size, type, normalized, stride, data);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glVertexAttribPointer returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glVertexAttribPointer\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glVertexAttribPointer\n");
        return false;
    }
}

bool callGlGetActiveAttrib(Stack* stack, bool pushReturn) {
    char* name = stack->pop<char*>();
    GLenum* type = stack->pop<GLenum*>();
    int32_t* vector_count = stack->pop<int32_t*>();
    int32_t* buffer_bytes_written = stack->pop<int32_t*>();
    int32_t buffer_size = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetActiveAttrib(%" PRIu32 ", %" PRId32 ", %" PRId32 ", %p, %p, %p, %p)\n",
                   program, location, buffer_size, buffer_bytes_written, vector_count, type, name);
        if (glGetActiveAttrib != nullptr) {
            glGetActiveAttrib(program, location, buffer_size, buffer_bytes_written, vector_count,
                              type, name);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetActiveAttrib returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetActiveAttrib\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetActiveAttrib\n");
        return false;
    }
}

bool callGlGetActiveUniform(Stack* stack, bool pushReturn) {
    char* name = stack->pop<char*>();
    GLenum* type = stack->pop<GLenum*>();
    int32_t* vector_count = stack->pop<int32_t*>();
    int32_t* buffer_bytes_written = stack->pop<int32_t*>();
    int32_t buffer_size = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetActiveUniform(%" PRIu32 ", %" PRId32 ", %" PRId32 ", %p, %p, %p, %p)\n",
                   program, location, buffer_size, buffer_bytes_written, vector_count, type, name);
        if (glGetActiveUniform != nullptr) {
            glGetActiveUniform(program, location, buffer_size, buffer_bytes_written, vector_count,
                               type, name);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetActiveUniform returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetActiveUniform\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetActiveUniform\n");
        return false;
    }
}

bool callGlGetError(Stack* stack, bool pushReturn) {
    if (stack->isValid()) {
        GAPID_INFO("glGetError()\n");
        if (glGetError != nullptr) {
            GLenum return_value = glGetError();
            GAPID_INFO("Returned: %u\n", return_value);
            if (pushReturn) {
                stack->push<GLenum>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetError returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetError\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetError\n");
        return false;
    }
}

bool callGlGetProgramiv(Stack* stack, bool pushReturn) {
    int32_t* value = stack->pop<int32_t*>();
    GLenum parameter = stack->pop<GLenum>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetProgramiv(%" PRIu32 ", %u, %p)\n", program, parameter, value);
        if (glGetProgramiv != nullptr) {
            glGetProgramiv(program, parameter, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetProgramiv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetProgramiv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetProgramiv\n");
        return false;
    }
}

bool callGlGetShaderiv(Stack* stack, bool pushReturn) {
    int32_t* value = stack->pop<int32_t*>();
    GLenum parameter = stack->pop<GLenum>();
    uint32_t shader = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetShaderiv(%" PRIu32 ", %u, %p)\n", shader, parameter, value);
        if (glGetShaderiv != nullptr) {
            glGetShaderiv(shader, parameter, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetShaderiv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetShaderiv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetShaderiv\n");
        return false;
    }
}

bool callGlGetUniformLocation(Stack* stack, bool pushReturn) {
    char* name = stack->pop<char*>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetUniformLocation(%" PRIu32 ", %s)\n", program, name);
        if (glGetUniformLocation != nullptr) {
            int32_t return_value = glGetUniformLocation(program, name);
            GAPID_INFO("Returned: %" PRId32 "\n", return_value);
            if (pushReturn) {
                stack->push<int32_t>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetUniformLocation returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetUniformLocation\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetUniformLocation\n");
        return false;
    }
}

bool callGlGetAttribLocation(Stack* stack, bool pushReturn) {
    char* name = stack->pop<char*>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetAttribLocation(%" PRIu32 ", %s)\n", program, name);
        if (glGetAttribLocation != nullptr) {
            int32_t return_value = glGetAttribLocation(program, name);
            GAPID_INFO("Returned: %" PRId32 "\n", return_value);
            if (pushReturn) {
                stack->push<int32_t>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetAttribLocation returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetAttribLocation\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetAttribLocation\n");
        return false;
    }
}

bool callGlPixelStorei(Stack* stack, bool pushReturn) {
    int32_t value = stack->pop<int32_t>();
    GLenum parameter = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glPixelStorei(%u, %" PRId32 ")\n", parameter, value);
        if (glPixelStorei != nullptr) {
            glPixelStorei(parameter, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glPixelStorei returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glPixelStorei\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glPixelStorei\n");
        return false;
    }
}

bool callGlTexParameteri(Stack* stack, bool pushReturn) {
    int32_t value = stack->pop<int32_t>();
    GLenum parameter = stack->pop<GLenum>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glTexParameteri(%u, %u, %" PRId32 ")\n", target, parameter, value);
        if (glTexParameteri != nullptr) {
            glTexParameteri(target, parameter, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glTexParameteri returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glTexParameteri\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glTexParameteri\n");
        return false;
    }
}

bool callGlTexParameterf(Stack* stack, bool pushReturn) {
    float value = stack->pop<float>();
    GLenum parameter = stack->pop<GLenum>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glTexParameterf(%u, %u, %f)\n", target, parameter, value);
        if (glTexParameterf != nullptr) {
            glTexParameterf(target, parameter, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glTexParameterf returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glTexParameterf\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glTexParameterf\n");
        return false;
    }
}

bool callGlGetTexParameteriv(Stack* stack, bool pushReturn) {
    int32_t* values = stack->pop<int32_t*>();
    GLenum parameter = stack->pop<GLenum>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glGetTexParameteriv(%u, %u, %p)\n", target, parameter, values);
        if (glGetTexParameteriv != nullptr) {
            glGetTexParameteriv(target, parameter, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetTexParameteriv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetTexParameteriv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetTexParameteriv\n");
        return false;
    }
}

bool callGlGetTexParameterfv(Stack* stack, bool pushReturn) {
    float* values = stack->pop<float*>();
    GLenum parameter = stack->pop<GLenum>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glGetTexParameterfv(%u, %u, %p)\n", target, parameter, values);
        if (glGetTexParameterfv != nullptr) {
            glGetTexParameterfv(target, parameter, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetTexParameterfv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetTexParameterfv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetTexParameterfv\n");
        return false;
    }
}

bool callGlUniform1i(Stack* stack, bool pushReturn) {
    int32_t value = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniform1i(%" PRId32 ", %" PRId32 ")\n", location, value);
        if (glUniform1i != nullptr) {
            glUniform1i(location, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniform1i returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniform1i\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniform1i\n");
        return false;
    }
}

bool callGlUniform2i(Stack* stack, bool pushReturn) {
    int32_t value1 = stack->pop<int32_t>();
    int32_t value0 = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniform2i(%" PRId32 ", %" PRId32 ", %" PRId32 ")\n", location, value0,
                   value1);
        if (glUniform2i != nullptr) {
            glUniform2i(location, value0, value1);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniform2i returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniform2i\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniform2i\n");
        return false;
    }
}

bool callGlUniform3i(Stack* stack, bool pushReturn) {
    int32_t value2 = stack->pop<int32_t>();
    int32_t value1 = stack->pop<int32_t>();
    int32_t value0 = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniform3i(%" PRId32 ", %" PRId32 ", %" PRId32 ", %" PRId32 ")\n", location,
                   value0, value1, value2);
        if (glUniform3i != nullptr) {
            glUniform3i(location, value0, value1, value2);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniform3i returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniform3i\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniform3i\n");
        return false;
    }
}

bool callGlUniform4i(Stack* stack, bool pushReturn) {
    int32_t value3 = stack->pop<int32_t>();
    int32_t value2 = stack->pop<int32_t>();
    int32_t value1 = stack->pop<int32_t>();
    int32_t value0 = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniform4i(%" PRId32 ", %" PRId32 ", %" PRId32 ", %" PRId32 ", %" PRId32 ")\n",
                   location, value0, value1, value2, value3);
        if (glUniform4i != nullptr) {
            glUniform4i(location, value0, value1, value2, value3);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniform4i returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniform4i\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniform4i\n");
        return false;
    }
}

bool callGlUniform1iv(Stack* stack, bool pushReturn) {
    int32_t* values = stack->pop<int32_t*>();
    int32_t count = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniform1iv(%" PRId32 ", %" PRId32 ", %p)\n", location, count, values);
        if (glUniform1iv != nullptr) {
            glUniform1iv(location, count, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniform1iv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniform1iv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniform1iv\n");
        return false;
    }
}

bool callGlUniform2iv(Stack* stack, bool pushReturn) {
    int32_t* values = stack->pop<int32_t*>();
    int32_t count = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniform2iv(%" PRId32 ", %" PRId32 ", %p)\n", location, count, values);
        if (glUniform2iv != nullptr) {
            glUniform2iv(location, count, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniform2iv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniform2iv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniform2iv\n");
        return false;
    }
}

bool callGlUniform3iv(Stack* stack, bool pushReturn) {
    int32_t* values = stack->pop<int32_t*>();
    int32_t count = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniform3iv(%" PRId32 ", %" PRId32 ", %p)\n", location, count, values);
        if (glUniform3iv != nullptr) {
            glUniform3iv(location, count, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniform3iv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniform3iv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniform3iv\n");
        return false;
    }
}

bool callGlUniform4iv(Stack* stack, bool pushReturn) {
    int32_t* values = stack->pop<int32_t*>();
    int32_t count = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniform4iv(%" PRId32 ", %" PRId32 ", %p)\n", location, count, values);
        if (glUniform4iv != nullptr) {
            glUniform4iv(location, count, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniform4iv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniform4iv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniform4iv\n");
        return false;
    }
}

bool callGlUniform1f(Stack* stack, bool pushReturn) {
    float value = stack->pop<float>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniform1f(%" PRId32 ", %f)\n", location, value);
        if (glUniform1f != nullptr) {
            glUniform1f(location, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniform1f returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniform1f\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniform1f\n");
        return false;
    }
}

bool callGlUniform2f(Stack* stack, bool pushReturn) {
    float value1 = stack->pop<float>();
    float value0 = stack->pop<float>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniform2f(%" PRId32 ", %f, %f)\n", location, value0, value1);
        if (glUniform2f != nullptr) {
            glUniform2f(location, value0, value1);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniform2f returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniform2f\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniform2f\n");
        return false;
    }
}

bool callGlUniform3f(Stack* stack, bool pushReturn) {
    float value2 = stack->pop<float>();
    float value1 = stack->pop<float>();
    float value0 = stack->pop<float>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniform3f(%" PRId32 ", %f, %f, %f)\n", location, value0, value1, value2);
        if (glUniform3f != nullptr) {
            glUniform3f(location, value0, value1, value2);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniform3f returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniform3f\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniform3f\n");
        return false;
    }
}

bool callGlUniform4f(Stack* stack, bool pushReturn) {
    float value3 = stack->pop<float>();
    float value2 = stack->pop<float>();
    float value1 = stack->pop<float>();
    float value0 = stack->pop<float>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniform4f(%" PRId32 ", %f, %f, %f, %f)\n", location, value0, value1, value2,
                   value3);
        if (glUniform4f != nullptr) {
            glUniform4f(location, value0, value1, value2, value3);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniform4f returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniform4f\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniform4f\n");
        return false;
    }
}

bool callGlUniform1fv(Stack* stack, bool pushReturn) {
    float* values = stack->pop<float*>();
    int32_t count = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniform1fv(%" PRId32 ", %" PRId32 ", %p)\n", location, count, values);
        if (glUniform1fv != nullptr) {
            glUniform1fv(location, count, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniform1fv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniform1fv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniform1fv\n");
        return false;
    }
}

bool callGlUniform2fv(Stack* stack, bool pushReturn) {
    float* values = stack->pop<float*>();
    int32_t count = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniform2fv(%" PRId32 ", %" PRId32 ", %p)\n", location, count, values);
        if (glUniform2fv != nullptr) {
            glUniform2fv(location, count, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniform2fv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniform2fv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniform2fv\n");
        return false;
    }
}

bool callGlUniform3fv(Stack* stack, bool pushReturn) {
    float* values = stack->pop<float*>();
    int32_t count = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniform3fv(%" PRId32 ", %" PRId32 ", %p)\n", location, count, values);
        if (glUniform3fv != nullptr) {
            glUniform3fv(location, count, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniform3fv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniform3fv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniform3fv\n");
        return false;
    }
}

bool callGlUniform4fv(Stack* stack, bool pushReturn) {
    float* values = stack->pop<float*>();
    int32_t count = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniform4fv(%" PRId32 ", %" PRId32 ", %p)\n", location, count, values);
        if (glUniform4fv != nullptr) {
            glUniform4fv(location, count, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniform4fv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniform4fv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniform4fv\n");
        return false;
    }
}

bool callGlUniformMatrix2fv(Stack* stack, bool pushReturn) {
    float* values = stack->pop<float*>();
    bool transpose = stack->pop<bool>();
    int32_t count = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniformMatrix2fv(%" PRId32 ", %" PRId32 ", %d, %p)\n", location, count,
                   transpose, values);
        if (glUniformMatrix2fv != nullptr) {
            glUniformMatrix2fv(location, count, transpose, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniformMatrix2fv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniformMatrix2fv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniformMatrix2fv\n");
        return false;
    }
}

bool callGlUniformMatrix3fv(Stack* stack, bool pushReturn) {
    float* values = stack->pop<float*>();
    bool transpose = stack->pop<bool>();
    int32_t count = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniformMatrix3fv(%" PRId32 ", %" PRId32 ", %d, %p)\n", location, count,
                   transpose, values);
        if (glUniformMatrix3fv != nullptr) {
            glUniformMatrix3fv(location, count, transpose, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniformMatrix3fv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniformMatrix3fv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniformMatrix3fv\n");
        return false;
    }
}

bool callGlUniformMatrix4fv(Stack* stack, bool pushReturn) {
    float* values = stack->pop<float*>();
    bool transpose = stack->pop<bool>();
    int32_t count = stack->pop<int32_t>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniformMatrix4fv(%" PRId32 ", %" PRId32 ", %d, %p)\n", location, count,
                   transpose, values);
        if (glUniformMatrix4fv != nullptr) {
            glUniformMatrix4fv(location, count, transpose, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniformMatrix4fv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniformMatrix4fv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniformMatrix4fv\n");
        return false;
    }
}

bool callGlGetUniformfv(Stack* stack, bool pushReturn) {
    float* values = stack->pop<float*>();
    int32_t location = stack->pop<int32_t>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetUniformfv(%" PRIu32 ", %" PRId32 ", %p)\n", program, location, values);
        if (glGetUniformfv != nullptr) {
            glGetUniformfv(program, location, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetUniformfv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetUniformfv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetUniformfv\n");
        return false;
    }
}

bool callGlGetUniformiv(Stack* stack, bool pushReturn) {
    int32_t* values = stack->pop<int32_t*>();
    int32_t location = stack->pop<int32_t>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetUniformiv(%" PRIu32 ", %" PRId32 ", %p)\n", program, location, values);
        if (glGetUniformiv != nullptr) {
            glGetUniformiv(program, location, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetUniformiv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetUniformiv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetUniformiv\n");
        return false;
    }
}

bool callGlVertexAttrib1f(Stack* stack, bool pushReturn) {
    float value0 = stack->pop<float>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glVertexAttrib1f(%" PRId32 ", %f)\n", location, value0);
        if (glVertexAttrib1f != nullptr) {
            glVertexAttrib1f(location, value0);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glVertexAttrib1f returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glVertexAttrib1f\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glVertexAttrib1f\n");
        return false;
    }
}

bool callGlVertexAttrib2f(Stack* stack, bool pushReturn) {
    float value1 = stack->pop<float>();
    float value0 = stack->pop<float>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glVertexAttrib2f(%" PRId32 ", %f, %f)\n", location, value0, value1);
        if (glVertexAttrib2f != nullptr) {
            glVertexAttrib2f(location, value0, value1);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glVertexAttrib2f returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glVertexAttrib2f\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glVertexAttrib2f\n");
        return false;
    }
}

bool callGlVertexAttrib3f(Stack* stack, bool pushReturn) {
    float value2 = stack->pop<float>();
    float value1 = stack->pop<float>();
    float value0 = stack->pop<float>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glVertexAttrib3f(%" PRId32 ", %f, %f, %f)\n", location, value0, value1, value2);
        if (glVertexAttrib3f != nullptr) {
            glVertexAttrib3f(location, value0, value1, value2);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glVertexAttrib3f returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glVertexAttrib3f\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glVertexAttrib3f\n");
        return false;
    }
}

bool callGlVertexAttrib4f(Stack* stack, bool pushReturn) {
    float value3 = stack->pop<float>();
    float value2 = stack->pop<float>();
    float value1 = stack->pop<float>();
    float value0 = stack->pop<float>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glVertexAttrib4f(%" PRId32 ", %f, %f, %f, %f)\n", location, value0, value1,
                   value2, value3);
        if (glVertexAttrib4f != nullptr) {
            glVertexAttrib4f(location, value0, value1, value2, value3);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glVertexAttrib4f returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glVertexAttrib4f\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glVertexAttrib4f\n");
        return false;
    }
}

bool callGlVertexAttrib1fv(Stack* stack, bool pushReturn) {
    float* value = stack->pop<float*>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glVertexAttrib1fv(%" PRId32 ", %p)\n", location, value);
        if (glVertexAttrib1fv != nullptr) {
            glVertexAttrib1fv(location, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glVertexAttrib1fv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glVertexAttrib1fv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glVertexAttrib1fv\n");
        return false;
    }
}

bool callGlVertexAttrib2fv(Stack* stack, bool pushReturn) {
    float* value = stack->pop<float*>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glVertexAttrib2fv(%" PRId32 ", %p)\n", location, value);
        if (glVertexAttrib2fv != nullptr) {
            glVertexAttrib2fv(location, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glVertexAttrib2fv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glVertexAttrib2fv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glVertexAttrib2fv\n");
        return false;
    }
}

bool callGlVertexAttrib3fv(Stack* stack, bool pushReturn) {
    float* value = stack->pop<float*>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glVertexAttrib3fv(%" PRId32 ", %p)\n", location, value);
        if (glVertexAttrib3fv != nullptr) {
            glVertexAttrib3fv(location, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glVertexAttrib3fv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glVertexAttrib3fv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glVertexAttrib3fv\n");
        return false;
    }
}

bool callGlVertexAttrib4fv(Stack* stack, bool pushReturn) {
    float* value = stack->pop<float*>();
    int32_t location = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glVertexAttrib4fv(%" PRId32 ", %p)\n", location, value);
        if (glVertexAttrib4fv != nullptr) {
            glVertexAttrib4fv(location, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glVertexAttrib4fv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glVertexAttrib4fv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glVertexAttrib4fv\n");
        return false;
    }
}

bool callGlGetShaderPrecisionFormat(Stack* stack, bool pushReturn) {
    int32_t* precision = stack->pop<int32_t*>();
    int32_t* range = stack->pop<int32_t*>();
    GLenum precision_type = stack->pop<GLenum>();
    GLenum shader_type = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glGetShaderPrecisionFormat(%u, %u, %p, %p)\n", shader_type, precision_type,
                   range, precision);
        if (glGetShaderPrecisionFormat != nullptr) {
            glGetShaderPrecisionFormat(shader_type, precision_type, range, precision);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetShaderPrecisionFormat returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetShaderPrecisionFormat\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetShaderPrecisionFormat\n");
        return false;
    }
}

bool callGlDepthMask(Stack* stack, bool pushReturn) {
    bool enabled = stack->pop<bool>();
    if (stack->isValid()) {
        GAPID_INFO("glDepthMask(%d)\n", enabled);
        if (glDepthMask != nullptr) {
            glDepthMask(enabled);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDepthMask returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDepthMask\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDepthMask\n");
        return false;
    }
}

bool callGlDepthFunc(Stack* stack, bool pushReturn) {
    GLenum function = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glDepthFunc(%u)\n", function);
        if (glDepthFunc != nullptr) {
            glDepthFunc(function);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDepthFunc returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDepthFunc\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDepthFunc\n");
        return false;
    }
}

bool callGlDepthRangef(Stack* stack, bool pushReturn) {
    float far = stack->pop<float>();
    float near = stack->pop<float>();
    if (stack->isValid()) {
        GAPID_INFO("glDepthRangef(%f, %f)\n", near, far);
        if (glDepthRangef != nullptr) {
            glDepthRangef(near, far);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDepthRangef returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDepthRangef\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDepthRangef\n");
        return false;
    }
}

bool callGlColorMask(Stack* stack, bool pushReturn) {
    bool alpha = stack->pop<bool>();
    bool blue = stack->pop<bool>();
    bool green = stack->pop<bool>();
    bool red = stack->pop<bool>();
    if (stack->isValid()) {
        GAPID_INFO("glColorMask(%d, %d, %d, %d)\n", red, green, blue, alpha);
        if (glColorMask != nullptr) {
            glColorMask(red, green, blue, alpha);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glColorMask returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glColorMask\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glColorMask\n");
        return false;
    }
}

bool callGlStencilMask(Stack* stack, bool pushReturn) {
    uint32_t mask = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glStencilMask(%" PRIu32 ")\n", mask);
        if (glStencilMask != nullptr) {
            glStencilMask(mask);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glStencilMask returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glStencilMask\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glStencilMask\n");
        return false;
    }
}

bool callGlStencilMaskSeparate(Stack* stack, bool pushReturn) {
    uint32_t mask = stack->pop<uint32_t>();
    GLenum face = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glStencilMaskSeparate(%u, %" PRIu32 ")\n", face, mask);
        if (glStencilMaskSeparate != nullptr) {
            glStencilMaskSeparate(face, mask);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glStencilMaskSeparate returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glStencilMaskSeparate\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glStencilMaskSeparate\n");
        return false;
    }
}

bool callGlStencilFuncSeparate(Stack* stack, bool pushReturn) {
    int32_t mask = stack->pop<int32_t>();
    int32_t reference_value = stack->pop<int32_t>();
    GLenum function = stack->pop<GLenum>();
    GLenum face = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glStencilFuncSeparate(%u, %u, %" PRId32 ", %" PRId32 ")\n", face, function,
                   reference_value, mask);
        if (glStencilFuncSeparate != nullptr) {
            glStencilFuncSeparate(face, function, reference_value, mask);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glStencilFuncSeparate returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glStencilFuncSeparate\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glStencilFuncSeparate\n");
        return false;
    }
}

bool callGlStencilOpSeparate(Stack* stack, bool pushReturn) {
    GLenum stencil_pass_depth_pass = stack->pop<GLenum>();
    GLenum stencil_pass_depth_fail = stack->pop<GLenum>();
    GLenum stencil_fail = stack->pop<GLenum>();
    GLenum face = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glStencilOpSeparate(%u, %u, %u, %u)\n", face, stencil_fail,
                   stencil_pass_depth_fail, stencil_pass_depth_pass);
        if (glStencilOpSeparate != nullptr) {
            glStencilOpSeparate(face, stencil_fail, stencil_pass_depth_fail,
                                stencil_pass_depth_pass);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glStencilOpSeparate returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glStencilOpSeparate\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glStencilOpSeparate\n");
        return false;
    }
}

bool callGlFrontFace(Stack* stack, bool pushReturn) {
    GLenum orientation = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glFrontFace(%u)\n", orientation);
        if (glFrontFace != nullptr) {
            glFrontFace(orientation);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glFrontFace returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glFrontFace\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glFrontFace\n");
        return false;
    }
}

bool callGlViewport(Stack* stack, bool pushReturn) {
    int32_t height = stack->pop<int32_t>();
    int32_t width = stack->pop<int32_t>();
    int32_t y = stack->pop<int32_t>();
    int32_t x = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glViewport(%" PRId32 ", %" PRId32 ", %" PRId32 ", %" PRId32 ")\n", x, y, width,
                   height);
        if (glViewport != nullptr) {
            glViewport(x, y, width, height);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glViewport returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glViewport\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glViewport\n");
        return false;
    }
}

bool callGlScissor(Stack* stack, bool pushReturn) {
    int32_t height = stack->pop<int32_t>();
    int32_t width = stack->pop<int32_t>();
    int32_t y = stack->pop<int32_t>();
    int32_t x = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glScissor(%" PRId32 ", %" PRId32 ", %" PRId32 ", %" PRId32 ")\n", x, y, width,
                   height);
        if (glScissor != nullptr) {
            glScissor(x, y, width, height);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glScissor returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glScissor\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glScissor\n");
        return false;
    }
}

bool callGlActiveTexture(Stack* stack, bool pushReturn) {
    GLenum unit = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glActiveTexture(%u)\n", unit);
        if (glActiveTexture != nullptr) {
            glActiveTexture(unit);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glActiveTexture returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glActiveTexture\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glActiveTexture\n");
        return false;
    }
}

bool callGlGenTextures(Stack* stack, bool pushReturn) {
    uint32_t* textures = stack->pop<uint32_t*>();
    int32_t count = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGenTextures(%" PRId32 ", %p)\n", count, textures);
        if (glGenTextures != nullptr) {
            glGenTextures(count, textures);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGenTextures returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGenTextures\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGenTextures\n");
        return false;
    }
}

bool callGlDeleteTextures(Stack* stack, bool pushReturn) {
    uint32_t* textures = stack->pop<uint32_t*>();
    int32_t count = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glDeleteTextures(%" PRId32 ", %p)\n", count, textures);
        if (glDeleteTextures != nullptr) {
            glDeleteTextures(count, textures);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDeleteTextures returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDeleteTextures\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDeleteTextures\n");
        return false;
    }
}

bool callGlIsTexture(Stack* stack, bool pushReturn) {
    uint32_t texture = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glIsTexture(%" PRIu32 ")\n", texture);
        if (glIsTexture != nullptr) {
            bool return_value = glIsTexture(texture);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<bool>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glIsTexture returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glIsTexture\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glIsTexture\n");
        return false;
    }
}

bool callGlBindTexture(Stack* stack, bool pushReturn) {
    uint32_t texture = stack->pop<uint32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glBindTexture(%u, %" PRIu32 ")\n", target, texture);
        if (glBindTexture != nullptr) {
            glBindTexture(target, texture);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBindTexture returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBindTexture\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBindTexture\n");
        return false;
    }
}

bool callGlTexImage2D(Stack* stack, bool pushReturn) {
    void* data = stack->pop<void*>();
    GLenum type = stack->pop<GLenum>();
    GLenum format = stack->pop<GLenum>();
    int32_t border = stack->pop<int32_t>();
    int32_t height = stack->pop<int32_t>();
    int32_t width = stack->pop<int32_t>();
    GLenum internal_format = stack->pop<GLenum>();
    int32_t level = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glTexImage2D(%u, %" PRId32 ", %u, %" PRId32 ", %" PRId32 ", %" PRId32
                   ", %u, %u, %p)\n",
                   target, level, internal_format, width, height, border, format, type, data);
        if (glTexImage2D != nullptr) {
            glTexImage2D(target, level, internal_format, width, height, border, format, type, data);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glTexImage2D returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glTexImage2D\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glTexImage2D\n");
        return false;
    }
}

bool callGlTexSubImage2D(Stack* stack, bool pushReturn) {
    void* data = stack->pop<void*>();
    GLenum type = stack->pop<GLenum>();
    GLenum format = stack->pop<GLenum>();
    int32_t height = stack->pop<int32_t>();
    int32_t width = stack->pop<int32_t>();
    int32_t yoffset = stack->pop<int32_t>();
    int32_t xoffset = stack->pop<int32_t>();
    int32_t level = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glTexSubImage2D(%u, %" PRId32 ", %" PRId32 ", %" PRId32 ", %" PRId32
                   ", %" PRId32 ", %u, %u, %p)\n",
                   target, level, xoffset, yoffset, width, height, format, type, data);
        if (glTexSubImage2D != nullptr) {
            glTexSubImage2D(target, level, xoffset, yoffset, width, height, format, type, data);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glTexSubImage2D returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glTexSubImage2D\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glTexSubImage2D\n");
        return false;
    }
}

bool callGlCopyTexImage2D(Stack* stack, bool pushReturn) {
    int32_t border = stack->pop<int32_t>();
    int32_t height = stack->pop<int32_t>();
    int32_t width = stack->pop<int32_t>();
    int32_t y = stack->pop<int32_t>();
    int32_t x = stack->pop<int32_t>();
    GLenum format = stack->pop<GLenum>();
    int32_t level = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glCopyTexImage2D(%u, %" PRId32 ", %u, %" PRId32 ", %" PRId32 ", %" PRId32
                   ", %" PRId32 ", %" PRId32 ")\n",
                   target, level, format, x, y, width, height, border);
        if (glCopyTexImage2D != nullptr) {
            glCopyTexImage2D(target, level, format, x, y, width, height, border);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glCopyTexImage2D returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glCopyTexImage2D\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glCopyTexImage2D\n");
        return false;
    }
}

bool callGlCopyTexSubImage2D(Stack* stack, bool pushReturn) {
    int32_t height = stack->pop<int32_t>();
    int32_t width = stack->pop<int32_t>();
    int32_t y = stack->pop<int32_t>();
    int32_t x = stack->pop<int32_t>();
    int32_t yoffset = stack->pop<int32_t>();
    int32_t xoffset = stack->pop<int32_t>();
    int32_t level = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glCopyTexSubImage2D(%u, %" PRId32 ", %" PRId32 ", %" PRId32 ", %" PRId32
                   ", %" PRId32 ", %" PRId32 ", %" PRId32 ")\n",
                   target, level, xoffset, yoffset, x, y, width, height);
        if (glCopyTexSubImage2D != nullptr) {
            glCopyTexSubImage2D(target, level, xoffset, yoffset, x, y, width, height);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glCopyTexSubImage2D returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glCopyTexSubImage2D\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glCopyTexSubImage2D\n");
        return false;
    }
}

bool callGlCompressedTexImage2D(Stack* stack, bool pushReturn) {
    void* data = stack->pop<void*>();
    int32_t image_size = stack->pop<int32_t>();
    int32_t border = stack->pop<int32_t>();
    int32_t height = stack->pop<int32_t>();
    int32_t width = stack->pop<int32_t>();
    GLenum format = stack->pop<GLenum>();
    int32_t level = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glCompressedTexImage2D(%u, %" PRId32 ", %u, %" PRId32 ", %" PRId32 ", %" PRId32
                   ", %" PRId32 ", %p)\n",
                   target, level, format, width, height, border, image_size, data);
        if (glCompressedTexImage2D != nullptr) {
            glCompressedTexImage2D(target, level, format, width, height, border, image_size, data);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glCompressedTexImage2D returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glCompressedTexImage2D\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glCompressedTexImage2D\n");
        return false;
    }
}

bool callGlCompressedTexSubImage2D(Stack* stack, bool pushReturn) {
    void* data = stack->pop<void*>();
    int32_t image_size = stack->pop<int32_t>();
    GLenum format = stack->pop<GLenum>();
    int32_t height = stack->pop<int32_t>();
    int32_t width = stack->pop<int32_t>();
    int32_t yoffset = stack->pop<int32_t>();
    int32_t xoffset = stack->pop<int32_t>();
    int32_t level = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glCompressedTexSubImage2D(%u, %" PRId32 ", %" PRId32 ", %" PRId32 ", %" PRId32
                   ", %" PRId32 ", %u, %" PRId32 ", %p)\n",
                   target, level, xoffset, yoffset, width, height, format, image_size, data);
        if (glCompressedTexSubImage2D != nullptr) {
            glCompressedTexSubImage2D(target, level, xoffset, yoffset, width, height, format,
                                      image_size, data);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glCompressedTexSubImage2D returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glCompressedTexSubImage2D\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glCompressedTexSubImage2D\n");
        return false;
    }
}

bool callGlGenerateMipmap(Stack* stack, bool pushReturn) {
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glGenerateMipmap(%u)\n", target);
        if (glGenerateMipmap != nullptr) {
            glGenerateMipmap(target);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGenerateMipmap returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGenerateMipmap\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGenerateMipmap\n");
        return false;
    }
}

bool callGlReadPixels(Stack* stack, bool pushReturn) {
    void* data = stack->pop<void*>();
    GLenum type = stack->pop<GLenum>();
    GLenum format = stack->pop<GLenum>();
    int32_t height = stack->pop<int32_t>();
    int32_t width = stack->pop<int32_t>();
    int32_t y = stack->pop<int32_t>();
    int32_t x = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glReadPixels(%" PRId32 ", %" PRId32 ", %" PRId32 ", %" PRId32 ", %u, %u, %p)\n",
                   x, y, width, height, format, type, data);
        if (glReadPixels != nullptr) {
            glReadPixels(x, y, width, height, format, type, data);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glReadPixels returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glReadPixels\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glReadPixels\n");
        return false;
    }
}

bool callGlGenFramebuffers(Stack* stack, bool pushReturn) {
    uint32_t* framebuffers = stack->pop<uint32_t*>();
    int32_t count = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGenFramebuffers(%" PRId32 ", %p)\n", count, framebuffers);
        if (glGenFramebuffers != nullptr) {
            glGenFramebuffers(count, framebuffers);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGenFramebuffers returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGenFramebuffers\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGenFramebuffers\n");
        return false;
    }
}

bool callGlBindFramebuffer(Stack* stack, bool pushReturn) {
    uint32_t framebuffer = stack->pop<uint32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glBindFramebuffer(%u, %" PRIu32 ")\n", target, framebuffer);
        if (glBindFramebuffer != nullptr) {
            glBindFramebuffer(target, framebuffer);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBindFramebuffer returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBindFramebuffer\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBindFramebuffer\n");
        return false;
    }
}

bool callGlCheckFramebufferStatus(Stack* stack, bool pushReturn) {
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glCheckFramebufferStatus(%u)\n", target);
        if (glCheckFramebufferStatus != nullptr) {
            GLenum return_value = glCheckFramebufferStatus(target);
            GAPID_INFO("Returned: %u\n", return_value);
            if (pushReturn) {
                stack->push<GLenum>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glCheckFramebufferStatus returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glCheckFramebufferStatus\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glCheckFramebufferStatus\n");
        return false;
    }
}

bool callGlDeleteFramebuffers(Stack* stack, bool pushReturn) {
    uint32_t* framebuffers = stack->pop<uint32_t*>();
    int32_t count = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glDeleteFramebuffers(%" PRId32 ", %p)\n", count, framebuffers);
        if (glDeleteFramebuffers != nullptr) {
            glDeleteFramebuffers(count, framebuffers);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDeleteFramebuffers returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDeleteFramebuffers\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDeleteFramebuffers\n");
        return false;
    }
}

bool callGlIsFramebuffer(Stack* stack, bool pushReturn) {
    uint32_t framebuffer = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glIsFramebuffer(%" PRIu32 ")\n", framebuffer);
        if (glIsFramebuffer != nullptr) {
            bool return_value = glIsFramebuffer(framebuffer);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<bool>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glIsFramebuffer returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glIsFramebuffer\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glIsFramebuffer\n");
        return false;
    }
}

bool callGlGenRenderbuffers(Stack* stack, bool pushReturn) {
    uint32_t* renderbuffers = stack->pop<uint32_t*>();
    int32_t count = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGenRenderbuffers(%" PRId32 ", %p)\n", count, renderbuffers);
        if (glGenRenderbuffers != nullptr) {
            glGenRenderbuffers(count, renderbuffers);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGenRenderbuffers returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGenRenderbuffers\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGenRenderbuffers\n");
        return false;
    }
}

bool callGlBindRenderbuffer(Stack* stack, bool pushReturn) {
    uint32_t renderbuffer = stack->pop<uint32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glBindRenderbuffer(%u, %" PRIu32 ")\n", target, renderbuffer);
        if (glBindRenderbuffer != nullptr) {
            glBindRenderbuffer(target, renderbuffer);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBindRenderbuffer returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBindRenderbuffer\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBindRenderbuffer\n");
        return false;
    }
}

bool callGlRenderbufferStorage(Stack* stack, bool pushReturn) {
    int32_t height = stack->pop<int32_t>();
    int32_t width = stack->pop<int32_t>();
    GLenum format = stack->pop<GLenum>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glRenderbufferStorage(%u, %u, %" PRId32 ", %" PRId32 ")\n", target, format,
                   width, height);
        if (glRenderbufferStorage != nullptr) {
            glRenderbufferStorage(target, format, width, height);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glRenderbufferStorage returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glRenderbufferStorage\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glRenderbufferStorage\n");
        return false;
    }
}

bool callGlDeleteRenderbuffers(Stack* stack, bool pushReturn) {
    uint32_t* renderbuffers = stack->pop<uint32_t*>();
    int32_t count = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glDeleteRenderbuffers(%" PRId32 ", %p)\n", count, renderbuffers);
        if (glDeleteRenderbuffers != nullptr) {
            glDeleteRenderbuffers(count, renderbuffers);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDeleteRenderbuffers returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDeleteRenderbuffers\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDeleteRenderbuffers\n");
        return false;
    }
}

bool callGlIsRenderbuffer(Stack* stack, bool pushReturn) {
    uint32_t renderbuffer = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glIsRenderbuffer(%" PRIu32 ")\n", renderbuffer);
        if (glIsRenderbuffer != nullptr) {
            bool return_value = glIsRenderbuffer(renderbuffer);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<bool>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glIsRenderbuffer returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glIsRenderbuffer\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glIsRenderbuffer\n");
        return false;
    }
}

bool callGlGetRenderbufferParameteriv(Stack* stack, bool pushReturn) {
    int32_t* values = stack->pop<int32_t*>();
    GLenum parameter = stack->pop<GLenum>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glGetRenderbufferParameteriv(%u, %u, %p)\n", target, parameter, values);
        if (glGetRenderbufferParameteriv != nullptr) {
            glGetRenderbufferParameteriv(target, parameter, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetRenderbufferParameteriv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetRenderbufferParameteriv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetRenderbufferParameteriv\n");
        return false;
    }
}

bool callGlGenBuffers(Stack* stack, bool pushReturn) {
    uint32_t* buffers = stack->pop<uint32_t*>();
    int32_t count = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGenBuffers(%" PRId32 ", %p)\n", count, buffers);
        if (glGenBuffers != nullptr) {
            glGenBuffers(count, buffers);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGenBuffers returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGenBuffers\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGenBuffers\n");
        return false;
    }
}

bool callGlBindBuffer(Stack* stack, bool pushReturn) {
    uint32_t buffer = stack->pop<uint32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glBindBuffer(%u, %" PRIu32 ")\n", target, buffer);
        if (glBindBuffer != nullptr) {
            glBindBuffer(target, buffer);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBindBuffer returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBindBuffer\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBindBuffer\n");
        return false;
    }
}

bool callGlBufferData(Stack* stack, bool pushReturn) {
    GLenum usage = stack->pop<GLenum>();
    void* data = stack->pop<void*>();
    int32_t size = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glBufferData(%u, %" PRId32 ", %p, %u)\n", target, size, data, usage);
        if (glBufferData != nullptr) {
            glBufferData(target, size, data, usage);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBufferData returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBufferData\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBufferData\n");
        return false;
    }
}

bool callGlBufferSubData(Stack* stack, bool pushReturn) {
    void* data = stack->pop<void*>();
    int32_t size = stack->pop<int32_t>();
    int32_t offset = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glBufferSubData(%u, %" PRId32 ", %" PRId32 ", %p)\n", target, offset, size,
                   data);
        if (glBufferSubData != nullptr) {
            glBufferSubData(target, offset, size, data);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBufferSubData returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBufferSubData\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBufferSubData\n");
        return false;
    }
}

bool callGlDeleteBuffers(Stack* stack, bool pushReturn) {
    uint32_t* buffers = stack->pop<uint32_t*>();
    int32_t count = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glDeleteBuffers(%" PRId32 ", %p)\n", count, buffers);
        if (glDeleteBuffers != nullptr) {
            glDeleteBuffers(count, buffers);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDeleteBuffers returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDeleteBuffers\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDeleteBuffers\n");
        return false;
    }
}

bool callGlIsBuffer(Stack* stack, bool pushReturn) {
    uint32_t buffer = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glIsBuffer(%" PRIu32 ")\n", buffer);
        if (glIsBuffer != nullptr) {
            bool return_value = glIsBuffer(buffer);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<bool>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glIsBuffer returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glIsBuffer\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glIsBuffer\n");
        return false;
    }
}

bool callGlGetBufferParameteriv(Stack* stack, bool pushReturn) {
    int32_t* value = stack->pop<int32_t*>();
    GLenum parameter = stack->pop<GLenum>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glGetBufferParameteriv(%u, %u, %p)\n", target, parameter, value);
        if (glGetBufferParameteriv != nullptr) {
            glGetBufferParameteriv(target, parameter, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetBufferParameteriv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetBufferParameteriv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetBufferParameteriv\n");
        return false;
    }
}

bool callGlCreateShader(Stack* stack, bool pushReturn) {
    GLenum type = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glCreateShader(%u)\n", type);
        if (glCreateShader != nullptr) {
            uint32_t return_value = glCreateShader(type);
            GAPID_INFO("Returned: %" PRIu32 "\n", return_value);
            if (pushReturn) {
                stack->push<uint32_t>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glCreateShader returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glCreateShader\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glCreateShader\n");
        return false;
    }
}

bool callGlDeleteShader(Stack* stack, bool pushReturn) {
    uint32_t shader = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glDeleteShader(%" PRIu32 ")\n", shader);
        if (glDeleteShader != nullptr) {
            glDeleteShader(shader);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDeleteShader returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDeleteShader\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDeleteShader\n");
        return false;
    }
}

bool callGlShaderSource(Stack* stack, bool pushReturn) {
    int32_t* length = stack->pop<int32_t*>();
    char** source = stack->pop<char**>();
    int32_t count = stack->pop<int32_t>();
    uint32_t shader = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glShaderSource(%" PRIu32 ", %" PRId32 ", %p, %p)\n", shader, count, source,
                   length);
        if (glShaderSource != nullptr) {
            glShaderSource(shader, count, source, length);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glShaderSource returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glShaderSource\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glShaderSource\n");
        return false;
    }
}

bool callGlShaderBinary(Stack* stack, bool pushReturn) {
    int32_t binary_size = stack->pop<int32_t>();
    void* binary = stack->pop<void*>();
    uint32_t binary_format = stack->pop<uint32_t>();
    uint32_t* shaders = stack->pop<uint32_t*>();
    int32_t count = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glShaderBinary(%" PRId32 ", %p, %" PRIu32 ", %p, %" PRId32 ")\n", count,
                   shaders, binary_format, binary, binary_size);
        if (glShaderBinary != nullptr) {
            glShaderBinary(count, shaders, binary_format, binary, binary_size);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glShaderBinary returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glShaderBinary\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glShaderBinary\n");
        return false;
    }
}

bool callGlGetShaderInfoLog(Stack* stack, bool pushReturn) {
    char* info = stack->pop<char*>();
    int32_t* string_length_written = stack->pop<int32_t*>();
    int32_t buffer_length = stack->pop<int32_t>();
    uint32_t shader = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetShaderInfoLog(%" PRIu32 ", %" PRId32 ", %p, %p)\n", shader, buffer_length,
                   string_length_written, info);
        if (glGetShaderInfoLog != nullptr) {
            glGetShaderInfoLog(shader, buffer_length, string_length_written, info);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetShaderInfoLog returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetShaderInfoLog\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetShaderInfoLog\n");
        return false;
    }
}

bool callGlGetShaderSource(Stack* stack, bool pushReturn) {
    char* source = stack->pop<char*>();
    int32_t* string_length_written = stack->pop<int32_t*>();
    int32_t buffer_length = stack->pop<int32_t>();
    uint32_t shader = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetShaderSource(%" PRIu32 ", %" PRId32 ", %p, %p)\n", shader, buffer_length,
                   string_length_written, source);
        if (glGetShaderSource != nullptr) {
            glGetShaderSource(shader, buffer_length, string_length_written, source);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetShaderSource returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetShaderSource\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetShaderSource\n");
        return false;
    }
}

bool callGlReleaseShaderCompiler(Stack* stack, bool pushReturn) {
    if (stack->isValid()) {
        GAPID_INFO("glReleaseShaderCompiler()\n");
        if (glReleaseShaderCompiler != nullptr) {
            glReleaseShaderCompiler();
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glReleaseShaderCompiler returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glReleaseShaderCompiler\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glReleaseShaderCompiler\n");
        return false;
    }
}

bool callGlCompileShader(Stack* stack, bool pushReturn) {
    uint32_t shader = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glCompileShader(%" PRIu32 ")\n", shader);
        if (glCompileShader != nullptr) {
            glCompileShader(shader);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glCompileShader returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glCompileShader\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glCompileShader\n");
        return false;
    }
}

bool callGlIsShader(Stack* stack, bool pushReturn) {
    uint32_t shader = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glIsShader(%" PRIu32 ")\n", shader);
        if (glIsShader != nullptr) {
            bool return_value = glIsShader(shader);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<bool>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glIsShader returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glIsShader\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glIsShader\n");
        return false;
    }
}

bool callGlCreateProgram(Stack* stack, bool pushReturn) {
    if (stack->isValid()) {
        GAPID_INFO("glCreateProgram()\n");
        if (glCreateProgram != nullptr) {
            uint32_t return_value = glCreateProgram();
            GAPID_INFO("Returned: %" PRIu32 "\n", return_value);
            if (pushReturn) {
                stack->push<uint32_t>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glCreateProgram returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glCreateProgram\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glCreateProgram\n");
        return false;
    }
}

bool callGlDeleteProgram(Stack* stack, bool pushReturn) {
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glDeleteProgram(%" PRIu32 ")\n", program);
        if (glDeleteProgram != nullptr) {
            glDeleteProgram(program);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDeleteProgram returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDeleteProgram\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDeleteProgram\n");
        return false;
    }
}

bool callGlAttachShader(Stack* stack, bool pushReturn) {
    uint32_t shader = stack->pop<uint32_t>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glAttachShader(%" PRIu32 ", %" PRIu32 ")\n", program, shader);
        if (glAttachShader != nullptr) {
            glAttachShader(program, shader);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glAttachShader returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glAttachShader\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glAttachShader\n");
        return false;
    }
}

bool callGlDetachShader(Stack* stack, bool pushReturn) {
    uint32_t shader = stack->pop<uint32_t>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glDetachShader(%" PRIu32 ", %" PRIu32 ")\n", program, shader);
        if (glDetachShader != nullptr) {
            glDetachShader(program, shader);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDetachShader returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDetachShader\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDetachShader\n");
        return false;
    }
}

bool callGlGetAttachedShaders(Stack* stack, bool pushReturn) {
    uint32_t* shaders = stack->pop<uint32_t*>();
    int32_t* shaders_length_written = stack->pop<int32_t*>();
    int32_t buffer_length = stack->pop<int32_t>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetAttachedShaders(%" PRIu32 ", %" PRId32 ", %p, %p)\n", program,
                   buffer_length, shaders_length_written, shaders);
        if (glGetAttachedShaders != nullptr) {
            glGetAttachedShaders(program, buffer_length, shaders_length_written, shaders);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetAttachedShaders returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetAttachedShaders\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetAttachedShaders\n");
        return false;
    }
}

bool callGlLinkProgram(Stack* stack, bool pushReturn) {
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glLinkProgram(%" PRIu32 ")\n", program);
        if (glLinkProgram != nullptr) {
            glLinkProgram(program);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glLinkProgram returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glLinkProgram\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glLinkProgram\n");
        return false;
    }
}

bool callGlGetProgramInfoLog(Stack* stack, bool pushReturn) {
    char* info = stack->pop<char*>();
    int32_t* string_length_written = stack->pop<int32_t*>();
    int32_t buffer_length = stack->pop<int32_t>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetProgramInfoLog(%" PRIu32 ", %" PRId32 ", %p, %p)\n", program,
                   buffer_length, string_length_written, info);
        if (glGetProgramInfoLog != nullptr) {
            glGetProgramInfoLog(program, buffer_length, string_length_written, info);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetProgramInfoLog returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetProgramInfoLog\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetProgramInfoLog\n");
        return false;
    }
}

bool callGlUseProgram(Stack* stack, bool pushReturn) {
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUseProgram(%" PRIu32 ")\n", program);
        if (glUseProgram != nullptr) {
            glUseProgram(program);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUseProgram returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUseProgram\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUseProgram\n");
        return false;
    }
}

bool callGlIsProgram(Stack* stack, bool pushReturn) {
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glIsProgram(%" PRIu32 ")\n", program);
        if (glIsProgram != nullptr) {
            bool return_value = glIsProgram(program);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<bool>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glIsProgram returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glIsProgram\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glIsProgram\n");
        return false;
    }
}

bool callGlValidateProgram(Stack* stack, bool pushReturn) {
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glValidateProgram(%" PRIu32 ")\n", program);
        if (glValidateProgram != nullptr) {
            glValidateProgram(program);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glValidateProgram returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glValidateProgram\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glValidateProgram\n");
        return false;
    }
}

bool callGlClearColor(Stack* stack, bool pushReturn) {
    float a = stack->pop<float>();
    float b = stack->pop<float>();
    float g = stack->pop<float>();
    float r = stack->pop<float>();
    if (stack->isValid()) {
        GAPID_INFO("glClearColor(%f, %f, %f, %f)\n", r, g, b, a);
        if (glClearColor != nullptr) {
            glClearColor(r, g, b, a);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glClearColor returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glClearColor\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glClearColor\n");
        return false;
    }
}

bool callGlClearDepthf(Stack* stack, bool pushReturn) {
    float depth = stack->pop<float>();
    if (stack->isValid()) {
        GAPID_INFO("glClearDepthf(%f)\n", depth);
        if (glClearDepthf != nullptr) {
            glClearDepthf(depth);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glClearDepthf returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glClearDepthf\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glClearDepthf\n");
        return false;
    }
}

bool callGlClearStencil(Stack* stack, bool pushReturn) {
    int32_t stencil = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glClearStencil(%" PRId32 ")\n", stencil);
        if (glClearStencil != nullptr) {
            glClearStencil(stencil);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glClearStencil returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glClearStencil\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glClearStencil\n");
        return false;
    }
}

bool callGlClear(Stack* stack, bool pushReturn) {
    GLbitfield mask = stack->pop<GLbitfield>();
    if (stack->isValid()) {
        GAPID_INFO("glClear(%u)\n", mask);
        if (glClear != nullptr) {
            glClear(mask);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glClear returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glClear\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glClear\n");
        return false;
    }
}

bool callGlCullFace(Stack* stack, bool pushReturn) {
    GLenum mode = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glCullFace(%u)\n", mode);
        if (glCullFace != nullptr) {
            glCullFace(mode);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glCullFace returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glCullFace\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glCullFace\n");
        return false;
    }
}

bool callGlPolygonOffset(Stack* stack, bool pushReturn) {
    float units = stack->pop<float>();
    float scale_factor = stack->pop<float>();
    if (stack->isValid()) {
        GAPID_INFO("glPolygonOffset(%f, %f)\n", scale_factor, units);
        if (glPolygonOffset != nullptr) {
            glPolygonOffset(scale_factor, units);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glPolygonOffset returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glPolygonOffset\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glPolygonOffset\n");
        return false;
    }
}

bool callGlLineWidth(Stack* stack, bool pushReturn) {
    float width = stack->pop<float>();
    if (stack->isValid()) {
        GAPID_INFO("glLineWidth(%f)\n", width);
        if (glLineWidth != nullptr) {
            glLineWidth(width);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glLineWidth returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glLineWidth\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glLineWidth\n");
        return false;
    }
}

bool callGlSampleCoverage(Stack* stack, bool pushReturn) {
    bool invert = stack->pop<bool>();
    float value = stack->pop<float>();
    if (stack->isValid()) {
        GAPID_INFO("glSampleCoverage(%f, %d)\n", value, invert);
        if (glSampleCoverage != nullptr) {
            glSampleCoverage(value, invert);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glSampleCoverage returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glSampleCoverage\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glSampleCoverage\n");
        return false;
    }
}

bool callGlHint(Stack* stack, bool pushReturn) {
    GLenum mode = stack->pop<GLenum>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glHint(%u, %u)\n", target, mode);
        if (glHint != nullptr) {
            glHint(target, mode);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glHint returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glHint\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glHint\n");
        return false;
    }
}

bool callGlFramebufferRenderbuffer(Stack* stack, bool pushReturn) {
    uint32_t renderbuffer = stack->pop<uint32_t>();
    GLenum renderbuffer_target = stack->pop<GLenum>();
    GLenum framebuffer_attachment = stack->pop<GLenum>();
    GLenum framebuffer_target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glFramebufferRenderbuffer(%u, %u, %u, %" PRIu32 ")\n", framebuffer_target,
                   framebuffer_attachment, renderbuffer_target, renderbuffer);
        if (glFramebufferRenderbuffer != nullptr) {
            glFramebufferRenderbuffer(framebuffer_target, framebuffer_attachment,
                                      renderbuffer_target, renderbuffer);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glFramebufferRenderbuffer returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glFramebufferRenderbuffer\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glFramebufferRenderbuffer\n");
        return false;
    }
}

bool callGlFramebufferTexture2D(Stack* stack, bool pushReturn) {
    int32_t level = stack->pop<int32_t>();
    uint32_t texture = stack->pop<uint32_t>();
    GLenum texture_target = stack->pop<GLenum>();
    GLenum framebuffer_attachment = stack->pop<GLenum>();
    GLenum framebuffer_target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glFramebufferTexture2D(%u, %u, %u, %" PRIu32 ", %" PRId32 ")\n",
                   framebuffer_target, framebuffer_attachment, texture_target, texture, level);
        if (glFramebufferTexture2D != nullptr) {
            glFramebufferTexture2D(framebuffer_target, framebuffer_attachment, texture_target,
                                   texture, level);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glFramebufferTexture2D returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glFramebufferTexture2D\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glFramebufferTexture2D\n");
        return false;
    }
}

bool callGlGetFramebufferAttachmentParameteriv(Stack* stack, bool pushReturn) {
    int32_t* value = stack->pop<int32_t*>();
    GLenum parameter = stack->pop<GLenum>();
    GLenum attachment = stack->pop<GLenum>();
    GLenum framebuffer_target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glGetFramebufferAttachmentParameteriv(%u, %u, %u, %p)\n", framebuffer_target,
                   attachment, parameter, value);
        if (glGetFramebufferAttachmentParameteriv != nullptr) {
            glGetFramebufferAttachmentParameteriv(framebuffer_target, attachment, parameter, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetFramebufferAttachmentParameteriv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING(
                    "Attempted to call unsupported function "
                    "glGetFramebufferAttachmentParameteriv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetFramebufferAttachmentParameteriv\n");
        return false;
    }
}

bool callGlDrawElements(Stack* stack, bool pushReturn) {
    void* indices = stack->pop<void*>();
    GLenum indices_type = stack->pop<GLenum>();
    int32_t element_count = stack->pop<int32_t>();
    GLenum draw_mode = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glDrawElements(%u, %" PRId32 ", %u, %p)\n", draw_mode, element_count,
                   indices_type, indices);
        if (glDrawElements != nullptr) {
            glDrawElements(draw_mode, element_count, indices_type, indices);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDrawElements returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDrawElements\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDrawElements\n");
        return false;
    }
}

bool callGlDrawArrays(Stack* stack, bool pushReturn) {
    int32_t index_count = stack->pop<int32_t>();
    int32_t first_index = stack->pop<int32_t>();
    GLenum draw_mode = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glDrawArrays(%u, %" PRId32 ", %" PRId32 ")\n", draw_mode, first_index,
                   index_count);
        if (glDrawArrays != nullptr) {
            glDrawArrays(draw_mode, first_index, index_count);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDrawArrays returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDrawArrays\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDrawArrays\n");
        return false;
    }
}

bool callGlFlush(Stack* stack, bool pushReturn) {
    if (stack->isValid()) {
        GAPID_INFO("glFlush()\n");
        if (glFlush != nullptr) {
            glFlush();
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glFlush returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glFlush\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glFlush\n");
        return false;
    }
}

bool callGlFinish(Stack* stack, bool pushReturn) {
    if (stack->isValid()) {
        GAPID_INFO("glFinish()\n");
        if (glFinish != nullptr) {
            glFinish();
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glFinish returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glFinish\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glFinish\n");
        return false;
    }
}

bool callGlGetBooleanv(Stack* stack, bool pushReturn) {
    bool* values = stack->pop<bool*>();
    GLenum param = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glGetBooleanv(%u, %p)\n", param, values);
        if (glGetBooleanv != nullptr) {
            glGetBooleanv(param, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetBooleanv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetBooleanv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetBooleanv\n");
        return false;
    }
}

bool callGlGetFloatv(Stack* stack, bool pushReturn) {
    float* values = stack->pop<float*>();
    GLenum param = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glGetFloatv(%u, %p)\n", param, values);
        if (glGetFloatv != nullptr) {
            glGetFloatv(param, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetFloatv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetFloatv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetFloatv\n");
        return false;
    }
}

bool callGlGetIntegerv(Stack* stack, bool pushReturn) {
    int32_t* values = stack->pop<int32_t*>();
    GLenum param = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glGetIntegerv(%u, %p)\n", param, values);
        if (glGetIntegerv != nullptr) {
            glGetIntegerv(param, values);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetIntegerv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetIntegerv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetIntegerv\n");
        return false;
    }
}

bool callGlGetString(Stack* stack, bool pushReturn) {
    GLenum param = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glGetString(%u)\n", param);
        if (glGetString != nullptr) {
            char* return_value = glGetString(param);
            GAPID_INFO("Returned: %p\n", return_value);
            if (pushReturn) {
                stack->push<char*>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetString returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetString\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetString\n");
        return false;
    }
}

bool callGlEnable(Stack* stack, bool pushReturn) {
    GLenum capability = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glEnable(%u)\n", capability);
        if (glEnable != nullptr) {
            glEnable(capability);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glEnable returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glEnable\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glEnable\n");
        return false;
    }
}

bool callGlDisable(Stack* stack, bool pushReturn) {
    GLenum capability = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glDisable(%u)\n", capability);
        if (glDisable != nullptr) {
            glDisable(capability);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDisable returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDisable\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDisable\n");
        return false;
    }
}

bool callGlIsEnabled(Stack* stack, bool pushReturn) {
    GLenum capability = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glIsEnabled(%u)\n", capability);
        if (glIsEnabled != nullptr) {
            bool return_value = glIsEnabled(capability);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<bool>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glIsEnabled returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glIsEnabled\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glIsEnabled\n");
        return false;
    }
}

bool callGlFenceSync(Stack* stack, bool pushReturn) {
    SyncFlags syncFlags = stack->pop<SyncFlags>();
    SyncCondition condition = stack->pop<SyncCondition>();
    if (stack->isValid()) {
        GAPID_INFO("glFenceSync(%u, %u)\n", condition, syncFlags);
        if (glFenceSync != nullptr) {
            uint64_t return_value = glFenceSync(condition, syncFlags);
            GAPID_INFO("Returned: %" PRIu64 "\n", return_value);
            if (pushReturn) {
                stack->push<uint64_t>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glFenceSync returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glFenceSync\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glFenceSync\n");
        return false;
    }
}

bool callGlDeleteSync(Stack* stack, bool pushReturn) {
    uint64_t sync = stack->pop<uint64_t>();
    if (stack->isValid()) {
        GAPID_INFO("glDeleteSync(%" PRIu64 ")\n", sync);
        if (glDeleteSync != nullptr) {
            glDeleteSync(sync);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDeleteSync returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDeleteSync\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDeleteSync\n");
        return false;
    }
}

bool callGlWaitSync(Stack* stack, bool pushReturn) {
    uint64_t timeout = stack->pop<uint64_t>();
    SyncFlags syncFlags = stack->pop<SyncFlags>();
    uint64_t sync = stack->pop<uint64_t>();
    if (stack->isValid()) {
        GAPID_INFO("glWaitSync(%" PRIu64 ", %u, %" PRIu64 ")\n", sync, syncFlags, timeout);
        if (glWaitSync != nullptr) {
            glWaitSync(sync, syncFlags, timeout);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glWaitSync returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glWaitSync\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glWaitSync\n");
        return false;
    }
}

bool callGlClientWaitSync(Stack* stack, bool pushReturn) {
    uint64_t timeout = stack->pop<uint64_t>();
    SyncFlags syncFlags = stack->pop<SyncFlags>();
    uint64_t sync = stack->pop<uint64_t>();
    if (stack->isValid()) {
        GAPID_INFO("glClientWaitSync(%" PRIu64 ", %u, %" PRIu64 ")\n", sync, syncFlags, timeout);
        if (glClientWaitSync != nullptr) {
            ClientWaitSyncSignal return_value = glClientWaitSync(sync, syncFlags, timeout);
            GAPID_INFO("Returned: %u\n", return_value);
            if (pushReturn) {
                stack->push<ClientWaitSyncSignal>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glClientWaitSync returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glClientWaitSync\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glClientWaitSync\n");
        return false;
    }
}

bool callGlMapBufferRange(Stack* stack, bool pushReturn) {
    GLbitfield access = stack->pop<GLbitfield>();
    int32_t length = stack->pop<int32_t>();
    int32_t offset = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glMapBufferRange(%u, %" PRId32 ", %" PRId32 ", %u)\n", target, offset, length,
                   access);
        if (glMapBufferRange != nullptr) {
            void* return_value = glMapBufferRange(target, offset, length, access);
            GAPID_INFO("Returned: %p\n", return_value);
            if (pushReturn) {
                stack->push<void*>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glMapBufferRange returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glMapBufferRange\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glMapBufferRange\n");
        return false;
    }
}

bool callGlUnmapBuffer(Stack* stack, bool pushReturn) {
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glUnmapBuffer(%u)\n", target);
        if (glUnmapBuffer != nullptr) {
            glUnmapBuffer(target);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUnmapBuffer returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUnmapBuffer\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUnmapBuffer\n");
        return false;
    }
}

bool callGlInvalidateFramebuffer(Stack* stack, bool pushReturn) {
    GLenum* attachments = stack->pop<GLenum*>();
    int32_t count = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glInvalidateFramebuffer(%u, %" PRId32 ", %p)\n", target, count, attachments);
        if (glInvalidateFramebuffer != nullptr) {
            glInvalidateFramebuffer(target, count, attachments);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glInvalidateFramebuffer returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glInvalidateFramebuffer\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glInvalidateFramebuffer\n");
        return false;
    }
}

bool callGlRenderbufferStorageMultisample(Stack* stack, bool pushReturn) {
    int32_t height = stack->pop<int32_t>();
    int32_t width = stack->pop<int32_t>();
    GLenum format = stack->pop<GLenum>();
    int32_t samples = stack->pop<int32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glRenderbufferStorageMultisample(%u, %" PRId32 ", %u, %" PRId32 ", %" PRId32
                   ")\n",
                   target, samples, format, width, height);
        if (glRenderbufferStorageMultisample != nullptr) {
            glRenderbufferStorageMultisample(target, samples, format, width, height);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glRenderbufferStorageMultisample returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING(
                    "Attempted to call unsupported function glRenderbufferStorageMultisample\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glRenderbufferStorageMultisample\n");
        return false;
    }
}

bool callGlBlitFramebuffer(Stack* stack, bool pushReturn) {
    GLenum filter = stack->pop<GLenum>();
    GLbitfield mask = stack->pop<GLbitfield>();
    int32_t dstY1 = stack->pop<int32_t>();
    int32_t dstX1 = stack->pop<int32_t>();
    int32_t dstY0 = stack->pop<int32_t>();
    int32_t dstX0 = stack->pop<int32_t>();
    int32_t srcY1 = stack->pop<int32_t>();
    int32_t srcX1 = stack->pop<int32_t>();
    int32_t srcY0 = stack->pop<int32_t>();
    int32_t srcX0 = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glBlitFramebuffer(%" PRId32 ", %" PRId32 ", %" PRId32 ", %" PRId32 ", %" PRId32
                   ", %" PRId32 ", %" PRId32 ", %" PRId32 ", %u, %u)\n",
                   srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter);
        if (glBlitFramebuffer != nullptr) {
            glBlitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBlitFramebuffer returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBlitFramebuffer\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBlitFramebuffer\n");
        return false;
    }
}

bool callGlGenQueries(Stack* stack, bool pushReturn) {
    uint32_t* queries = stack->pop<uint32_t*>();
    int32_t count = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGenQueries(%" PRId32 ", %p)\n", count, queries);
        if (glGenQueries != nullptr) {
            glGenQueries(count, queries);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGenQueries returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGenQueries\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGenQueries\n");
        return false;
    }
}

bool callGlBeginQuery(Stack* stack, bool pushReturn) {
    uint32_t query = stack->pop<uint32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glBeginQuery(%u, %" PRIu32 ")\n", target, query);
        if (glBeginQuery != nullptr) {
            glBeginQuery(target, query);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBeginQuery returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBeginQuery\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBeginQuery\n");
        return false;
    }
}

bool callGlEndQuery(Stack* stack, bool pushReturn) {
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glEndQuery(%u)\n", target);
        if (glEndQuery != nullptr) {
            glEndQuery(target);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glEndQuery returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glEndQuery\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glEndQuery\n");
        return false;
    }
}

bool callGlDeleteQueries(Stack* stack, bool pushReturn) {
    uint32_t* queries = stack->pop<uint32_t*>();
    int32_t count = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glDeleteQueries(%" PRId32 ", %p)\n", count, queries);
        if (glDeleteQueries != nullptr) {
            glDeleteQueries(count, queries);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDeleteQueries returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDeleteQueries\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDeleteQueries\n");
        return false;
    }
}

bool callGlIsQuery(Stack* stack, bool pushReturn) {
    uint32_t query = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glIsQuery(%" PRIu32 ")\n", query);
        if (glIsQuery != nullptr) {
            bool return_value = glIsQuery(query);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<bool>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glIsQuery returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glIsQuery\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glIsQuery\n");
        return false;
    }
}

bool callGlGetQueryiv(Stack* stack, bool pushReturn) {
    int32_t* value = stack->pop<int32_t*>();
    GLenum parameter = stack->pop<GLenum>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glGetQueryiv(%u, %u, %p)\n", target, parameter, value);
        if (glGetQueryiv != nullptr) {
            glGetQueryiv(target, parameter, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetQueryiv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetQueryiv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetQueryiv\n");
        return false;
    }
}

bool callGlGetQueryObjectuiv(Stack* stack, bool pushReturn) {
    uint32_t* value = stack->pop<uint32_t*>();
    GLenum parameter = stack->pop<GLenum>();
    uint32_t query = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetQueryObjectuiv(%" PRIu32 ", %u, %p)\n", query, parameter, value);
        if (glGetQueryObjectuiv != nullptr) {
            glGetQueryObjectuiv(query, parameter, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetQueryObjectuiv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetQueryObjectuiv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetQueryObjectuiv\n");
        return false;
    }
}

bool callGlGetActiveUniformBlockName(Stack* stack, bool pushReturn) {
    char* name = stack->pop<char*>();
    int32_t* buffer_bytes_written = stack->pop<int32_t*>();
    int32_t buffer_size = stack->pop<int32_t>();
    uint32_t uniform_block_index = stack->pop<uint32_t>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetActiveUniformBlockName(%" PRIu32 ", %" PRIu32 ", %" PRId32 ", %p, %p)\n",
                   program, uniform_block_index, buffer_size, buffer_bytes_written, name);
        if (glGetActiveUniformBlockName != nullptr) {
            glGetActiveUniformBlockName(program, uniform_block_index, buffer_size,
                                        buffer_bytes_written, name);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetActiveUniformBlockName returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetActiveUniformBlockName\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetActiveUniformBlockName\n");
        return false;
    }
}

bool callGlGetActiveUniformBlockiv(Stack* stack, bool pushReturn) {
    int32_t* parameters = stack->pop<int32_t*>();
    GLenum parameter_name = stack->pop<GLenum>();
    uint32_t uniform_block_index = stack->pop<uint32_t>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetActiveUniformBlockiv(%" PRIu32 ", %" PRIu32 ", %u, %p)\n", program,
                   uniform_block_index, parameter_name, parameters);
        if (glGetActiveUniformBlockiv != nullptr) {
            glGetActiveUniformBlockiv(program, uniform_block_index, parameter_name, parameters);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetActiveUniformBlockiv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetActiveUniformBlockiv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetActiveUniformBlockiv\n");
        return false;
    }
}

bool callGlUniformBlockBinding(Stack* stack, bool pushReturn) {
    uint32_t uniform_block_binding = stack->pop<uint32_t>();
    uint32_t uniform_block_index = stack->pop<uint32_t>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glUniformBlockBinding(%" PRIu32 ", %" PRIu32 ", %" PRIu32 ")\n", program,
                   uniform_block_index, uniform_block_binding);
        if (glUniformBlockBinding != nullptr) {
            glUniformBlockBinding(program, uniform_block_index, uniform_block_binding);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glUniformBlockBinding returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glUniformBlockBinding\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glUniformBlockBinding\n");
        return false;
    }
}

bool callGlGetActiveUniformsiv(Stack* stack, bool pushReturn) {
    int32_t* parameters = stack->pop<int32_t*>();
    GLenum parameter_name = stack->pop<GLenum>();
    uint32_t* uniform_indices = stack->pop<uint32_t*>();
    uint32_t uniform_count = stack->pop<uint32_t>();
    uint32_t program = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetActiveUniformsiv(%" PRIu32 ", %" PRIu32 ", %p, %u, %p)\n", program,
                   uniform_count, uniform_indices, parameter_name, parameters);
        if (glGetActiveUniformsiv != nullptr) {
            glGetActiveUniformsiv(program, uniform_count, uniform_indices, parameter_name,
                                  parameters);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetActiveUniformsiv returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetActiveUniformsiv\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetActiveUniformsiv\n");
        return false;
    }
}

bool callGlBindBufferBase(Stack* stack, bool pushReturn) {
    uint32_t buffer = stack->pop<uint32_t>();
    uint32_t index = stack->pop<uint32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glBindBufferBase(%u, %" PRIu32 ", %" PRIu32 ")\n", target, index, buffer);
        if (glBindBufferBase != nullptr) {
            glBindBufferBase(target, index, buffer);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBindBufferBase returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBindBufferBase\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBindBufferBase\n");
        return false;
    }
}

bool callGlGenVertexArrays(Stack* stack, bool pushReturn) {
    uint32_t* arrays = stack->pop<uint32_t*>();
    int32_t count = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGenVertexArrays(%" PRId32 ", %p)\n", count, arrays);
        if (glGenVertexArrays != nullptr) {
            glGenVertexArrays(count, arrays);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGenVertexArrays returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGenVertexArrays\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGenVertexArrays\n");
        return false;
    }
}

bool callGlBindVertexArray(Stack* stack, bool pushReturn) {
    uint32_t array = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glBindVertexArray(%" PRIu32 ")\n", array);
        if (glBindVertexArray != nullptr) {
            glBindVertexArray(array);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBindVertexArray returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBindVertexArray\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBindVertexArray\n");
        return false;
    }
}

bool callGlDeleteVertexArrays(Stack* stack, bool pushReturn) {
    uint32_t* arrays = stack->pop<uint32_t*>();
    uint32_t count = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glDeleteVertexArrays(%" PRIu32 ", %p)\n", count, arrays);
        if (glDeleteVertexArrays != nullptr) {
            glDeleteVertexArrays(count, arrays);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDeleteVertexArrays returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDeleteVertexArrays\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDeleteVertexArrays\n");
        return false;
    }
}

bool callGlGetQueryObjecti64v(Stack* stack, bool pushReturn) {
    int64_t* value = stack->pop<int64_t*>();
    GLenum parameter = stack->pop<GLenum>();
    uint32_t query = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetQueryObjecti64v(%" PRIu32 ", %u, %p)\n", query, parameter, value);
        if (glGetQueryObjecti64v != nullptr) {
            glGetQueryObjecti64v(query, parameter, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetQueryObjecti64v returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetQueryObjecti64v\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetQueryObjecti64v\n");
        return false;
    }
}

bool callGlGetQueryObjectui64v(Stack* stack, bool pushReturn) {
    uint64_t* value = stack->pop<uint64_t*>();
    GLenum parameter = stack->pop<GLenum>();
    uint32_t query = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetQueryObjectui64v(%" PRIu32 ", %u, %p)\n", query, parameter, value);
        if (glGetQueryObjectui64v != nullptr) {
            glGetQueryObjectui64v(query, parameter, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetQueryObjectui64v returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetQueryObjectui64v\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetQueryObjectui64v\n");
        return false;
    }
}

bool callGlGenQueriesEXT(Stack* stack, bool pushReturn) {
    uint32_t* queries = stack->pop<uint32_t*>();
    int32_t count = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGenQueriesEXT(%" PRId32 ", %p)\n", count, queries);
        if (glGenQueriesEXT != nullptr) {
            glGenQueriesEXT(count, queries);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGenQueriesEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGenQueriesEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGenQueriesEXT\n");
        return false;
    }
}

bool callGlBeginQueryEXT(Stack* stack, bool pushReturn) {
    uint32_t query = stack->pop<uint32_t>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glBeginQueryEXT(%u, %" PRIu32 ")\n", target, query);
        if (glBeginQueryEXT != nullptr) {
            glBeginQueryEXT(target, query);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glBeginQueryEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glBeginQueryEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glBeginQueryEXT\n");
        return false;
    }
}

bool callGlEndQueryEXT(Stack* stack, bool pushReturn) {
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glEndQueryEXT(%u)\n", target);
        if (glEndQueryEXT != nullptr) {
            glEndQueryEXT(target);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glEndQueryEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glEndQueryEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glEndQueryEXT\n");
        return false;
    }
}

bool callGlDeleteQueriesEXT(Stack* stack, bool pushReturn) {
    uint32_t* queries = stack->pop<uint32_t*>();
    int32_t count = stack->pop<int32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glDeleteQueriesEXT(%" PRId32 ", %p)\n", count, queries);
        if (glDeleteQueriesEXT != nullptr) {
            glDeleteQueriesEXT(count, queries);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glDeleteQueriesEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glDeleteQueriesEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glDeleteQueriesEXT\n");
        return false;
    }
}

bool callGlIsQueryEXT(Stack* stack, bool pushReturn) {
    uint32_t query = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glIsQueryEXT(%" PRIu32 ")\n", query);
        if (glIsQueryEXT != nullptr) {
            bool return_value = glIsQueryEXT(query);
            GAPID_INFO("Returned: %d\n", return_value);
            if (pushReturn) {
                stack->push<bool>(return_value);
            }
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glIsQueryEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glIsQueryEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glIsQueryEXT\n");
        return false;
    }
}

bool callGlQueryCounterEXT(Stack* stack, bool pushReturn) {
    GLenum target = stack->pop<GLenum>();
    uint32_t query = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glQueryCounterEXT(%" PRIu32 ", %u)\n", query, target);
        if (glQueryCounterEXT != nullptr) {
            glQueryCounterEXT(query, target);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glQueryCounterEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glQueryCounterEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glQueryCounterEXT\n");
        return false;
    }
}

bool callGlGetQueryivEXT(Stack* stack, bool pushReturn) {
    int32_t* value = stack->pop<int32_t*>();
    GLenum parameter = stack->pop<GLenum>();
    GLenum target = stack->pop<GLenum>();
    if (stack->isValid()) {
        GAPID_INFO("glGetQueryivEXT(%u, %u, %p)\n", target, parameter, value);
        if (glGetQueryivEXT != nullptr) {
            glGetQueryivEXT(target, parameter, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetQueryivEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetQueryivEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetQueryivEXT\n");
        return false;
    }
}

bool callGlGetQueryObjectivEXT(Stack* stack, bool pushReturn) {
    int32_t* value = stack->pop<int32_t*>();
    GLenum parameter = stack->pop<GLenum>();
    uint32_t query = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetQueryObjectivEXT(%" PRIu32 ", %u, %p)\n", query, parameter, value);
        if (glGetQueryObjectivEXT != nullptr) {
            glGetQueryObjectivEXT(query, parameter, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetQueryObjectivEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetQueryObjectivEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetQueryObjectivEXT\n");
        return false;
    }
}

bool callGlGetQueryObjectuivEXT(Stack* stack, bool pushReturn) {
    uint32_t* value = stack->pop<uint32_t*>();
    GLenum parameter = stack->pop<GLenum>();
    uint32_t query = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetQueryObjectuivEXT(%" PRIu32 ", %u, %p)\n", query, parameter, value);
        if (glGetQueryObjectuivEXT != nullptr) {
            glGetQueryObjectuivEXT(query, parameter, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetQueryObjectuivEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetQueryObjectuivEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetQueryObjectuivEXT\n");
        return false;
    }
}

bool callGlGetQueryObjecti64vEXT(Stack* stack, bool pushReturn) {
    int64_t* value = stack->pop<int64_t*>();
    GLenum parameter = stack->pop<GLenum>();
    uint32_t query = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetQueryObjecti64vEXT(%" PRIu32 ", %u, %p)\n", query, parameter, value);
        if (glGetQueryObjecti64vEXT != nullptr) {
            glGetQueryObjecti64vEXT(query, parameter, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetQueryObjecti64vEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetQueryObjecti64vEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetQueryObjecti64vEXT\n");
        return false;
    }
}

bool callGlGetQueryObjectui64vEXT(Stack* stack, bool pushReturn) {
    uint64_t* value = stack->pop<uint64_t*>();
    GLenum parameter = stack->pop<GLenum>();
    uint32_t query = stack->pop<uint32_t>();
    if (stack->isValid()) {
        GAPID_INFO("glGetQueryObjectui64vEXT(%" PRIu32 ", %u, %p)\n", query, parameter, value);
        if (glGetQueryObjectui64vEXT != nullptr) {
            glGetQueryObjectui64vEXT(query, parameter, value);
            const GLenum err = glGetError();
            if (err != GLenum::GL_NO_ERROR) {
                GAPID_WARNING("glGetQueryObjectui64vEXT returned error: 0x%x\n", err);
            }
        } else {
            GAPID_WARNING("Attempted to call unsupported function glGetQueryObjectui64vEXT\n");
        }
        return true;
    } else {
        GAPID_WARNING("Error during calling function glGetQueryObjectui64vEXT\n");
        return false;
    }
}

}  // end of anonymous namespace

PFNEGLINITIALIZE eglInitialize = nullptr;
PFNEGLCREATECONTEXT eglCreateContext = nullptr;
PFNEGLMAKECURRENT eglMakeCurrent = nullptr;
PFNEGLSWAPBUFFERS eglSwapBuffers = nullptr;
PFNEGLQUERYSURFACE eglQuerySurface = nullptr;
PFNGLXCREATECONTEXT glXCreateContext = nullptr;
PFNGLXCREATENEWCONTEXT glXCreateNewContext = nullptr;
PFNGLXMAKECONTEXTCURRENT glXMakeContextCurrent = nullptr;
PFNGLXMAKECURRENT glXMakeCurrent = nullptr;
PFNGLXSWAPBUFFERS glXSwapBuffers = nullptr;
PFNGLXQUERYDRAWABLE glXQueryDrawable = nullptr;
PFNWGLCREATECONTEXT wglCreateContext = nullptr;
PFNWGLCREATECONTEXTATTRIBSARB wglCreateContextAttribsARB = nullptr;
PFNWGLMAKECURRENT wglMakeCurrent = nullptr;
PFNWGLSWAPBUFFERS wglSwapBuffers = nullptr;
PFNCGLCREATECONTEXT CGLCreateContext = nullptr;
PFNCGLSETCURRENTCONTEXT CGLSetCurrentContext = nullptr;
PFNCGLGETSURFACE CGLGetSurface = nullptr;
PFNCGSGETSURFACEBOUNDS CGSGetSurfaceBounds = nullptr;
PFNCGLFLUSHDRAWABLE CGLFlushDrawable = nullptr;
PFNGLENABLECLIENTSTATE glEnableClientState = nullptr;
PFNGLDISABLECLIENTSTATE glDisableClientState = nullptr;
PFNGLGETPROGRAMBINARYOES glGetProgramBinaryOES = nullptr;
PFNGLPROGRAMBINARYOES glProgramBinaryOES = nullptr;
PFNGLSTARTTILINGQCOM glStartTilingQCOM = nullptr;
PFNGLENDTILINGQCOM glEndTilingQCOM = nullptr;
PFNGLDISCARDFRAMEBUFFEREXT glDiscardFramebufferEXT = nullptr;
PFNGLINSERTEVENTMARKEREXT glInsertEventMarkerEXT = nullptr;
PFNGLPUSHGROUPMARKEREXT glPushGroupMarkerEXT = nullptr;
PFNGLPOPGROUPMARKEREXT glPopGroupMarkerEXT = nullptr;
PFNGLTEXSTORAGE1DEXT glTexStorage1DEXT = nullptr;
PFNGLTEXSTORAGE2DEXT glTexStorage2DEXT = nullptr;
PFNGLTEXSTORAGE3DEXT glTexStorage3DEXT = nullptr;
PFNGLTEXTURESTORAGE1DEXT glTextureStorage1DEXT = nullptr;
PFNGLTEXTURESTORAGE2DEXT glTextureStorage2DEXT = nullptr;
PFNGLTEXTURESTORAGE3DEXT glTextureStorage3DEXT = nullptr;
PFNGLGENVERTEXARRAYSOES glGenVertexArraysOES = nullptr;
PFNGLBINDVERTEXARRAYOES glBindVertexArrayOES = nullptr;
PFNGLDELETEVERTEXARRAYSOES glDeleteVertexArraysOES = nullptr;
PFNGLISVERTEXARRAYOES glIsVertexArrayOES = nullptr;
PFNGLEGLIMAGETARGETTEXTURE2DOES glEGLImageTargetTexture2DOES = nullptr;
PFNGLEGLIMAGETARGETRENDERBUFFERSTORAGEOES glEGLImageTargetRenderbufferStorageOES = nullptr;
PFNGLGETGRAPHICSRESETSTATUSEXT glGetGraphicsResetStatusEXT = nullptr;
PFNGLBINDATTRIBLOCATION glBindAttribLocation = nullptr;
PFNGLBLENDFUNC glBlendFunc = nullptr;
PFNGLBLENDFUNCSEPARATE glBlendFuncSeparate = nullptr;
PFNGLBLENDEQUATION glBlendEquation = nullptr;
PFNGLBLENDEQUATIONSEPARATE glBlendEquationSeparate = nullptr;
PFNGLBLENDCOLOR glBlendColor = nullptr;
PFNGLENABLEVERTEXATTRIBARRAY glEnableVertexAttribArray = nullptr;
PFNGLDISABLEVERTEXATTRIBARRAY glDisableVertexAttribArray = nullptr;
PFNGLVERTEXATTRIBPOINTER glVertexAttribPointer = nullptr;
PFNGLGETACTIVEATTRIB glGetActiveAttrib = nullptr;
PFNGLGETACTIVEUNIFORM glGetActiveUniform = nullptr;
PFNGLGETERROR glGetError = nullptr;
PFNGLGETPROGRAMIV glGetProgramiv = nullptr;
PFNGLGETSHADERIV glGetShaderiv = nullptr;
PFNGLGETUNIFORMLOCATION glGetUniformLocation = nullptr;
PFNGLGETATTRIBLOCATION glGetAttribLocation = nullptr;
PFNGLPIXELSTOREI glPixelStorei = nullptr;
PFNGLTEXPARAMETERI glTexParameteri = nullptr;
PFNGLTEXPARAMETERF glTexParameterf = nullptr;
PFNGLGETTEXPARAMETERIV glGetTexParameteriv = nullptr;
PFNGLGETTEXPARAMETERFV glGetTexParameterfv = nullptr;
PFNGLUNIFORM1I glUniform1i = nullptr;
PFNGLUNIFORM2I glUniform2i = nullptr;
PFNGLUNIFORM3I glUniform3i = nullptr;
PFNGLUNIFORM4I glUniform4i = nullptr;
PFNGLUNIFORM1IV glUniform1iv = nullptr;
PFNGLUNIFORM2IV glUniform2iv = nullptr;
PFNGLUNIFORM3IV glUniform3iv = nullptr;
PFNGLUNIFORM4IV glUniform4iv = nullptr;
PFNGLUNIFORM1F glUniform1f = nullptr;
PFNGLUNIFORM2F glUniform2f = nullptr;
PFNGLUNIFORM3F glUniform3f = nullptr;
PFNGLUNIFORM4F glUniform4f = nullptr;
PFNGLUNIFORM1FV glUniform1fv = nullptr;
PFNGLUNIFORM2FV glUniform2fv = nullptr;
PFNGLUNIFORM3FV glUniform3fv = nullptr;
PFNGLUNIFORM4FV glUniform4fv = nullptr;
PFNGLUNIFORMMATRIX2FV glUniformMatrix2fv = nullptr;
PFNGLUNIFORMMATRIX3FV glUniformMatrix3fv = nullptr;
PFNGLUNIFORMMATRIX4FV glUniformMatrix4fv = nullptr;
PFNGLGETUNIFORMFV glGetUniformfv = nullptr;
PFNGLGETUNIFORMIV glGetUniformiv = nullptr;
PFNGLVERTEXATTRIB1F glVertexAttrib1f = nullptr;
PFNGLVERTEXATTRIB2F glVertexAttrib2f = nullptr;
PFNGLVERTEXATTRIB3F glVertexAttrib3f = nullptr;
PFNGLVERTEXATTRIB4F glVertexAttrib4f = nullptr;
PFNGLVERTEXATTRIB1FV glVertexAttrib1fv = nullptr;
PFNGLVERTEXATTRIB2FV glVertexAttrib2fv = nullptr;
PFNGLVERTEXATTRIB3FV glVertexAttrib3fv = nullptr;
PFNGLVERTEXATTRIB4FV glVertexAttrib4fv = nullptr;
PFNGLGETSHADERPRECISIONFORMAT glGetShaderPrecisionFormat = nullptr;
PFNGLDEPTHMASK glDepthMask = nullptr;
PFNGLDEPTHFUNC glDepthFunc = nullptr;
PFNGLDEPTHRANGEF glDepthRangef = nullptr;
PFNGLCOLORMASK glColorMask = nullptr;
PFNGLSTENCILMASK glStencilMask = nullptr;
PFNGLSTENCILMASKSEPARATE glStencilMaskSeparate = nullptr;
PFNGLSTENCILFUNCSEPARATE glStencilFuncSeparate = nullptr;
PFNGLSTENCILOPSEPARATE glStencilOpSeparate = nullptr;
PFNGLFRONTFACE glFrontFace = nullptr;
PFNGLVIEWPORT glViewport = nullptr;
PFNGLSCISSOR glScissor = nullptr;
PFNGLACTIVETEXTURE glActiveTexture = nullptr;
PFNGLGENTEXTURES glGenTextures = nullptr;
PFNGLDELETETEXTURES glDeleteTextures = nullptr;
PFNGLISTEXTURE glIsTexture = nullptr;
PFNGLBINDTEXTURE glBindTexture = nullptr;
PFNGLTEXIMAGE2D glTexImage2D = nullptr;
PFNGLTEXSUBIMAGE2D glTexSubImage2D = nullptr;
PFNGLCOPYTEXIMAGE2D glCopyTexImage2D = nullptr;
PFNGLCOPYTEXSUBIMAGE2D glCopyTexSubImage2D = nullptr;
PFNGLCOMPRESSEDTEXIMAGE2D glCompressedTexImage2D = nullptr;
PFNGLCOMPRESSEDTEXSUBIMAGE2D glCompressedTexSubImage2D = nullptr;
PFNGLGENERATEMIPMAP glGenerateMipmap = nullptr;
PFNGLREADPIXELS glReadPixels = nullptr;
PFNGLGENFRAMEBUFFERS glGenFramebuffers = nullptr;
PFNGLBINDFRAMEBUFFER glBindFramebuffer = nullptr;
PFNGLCHECKFRAMEBUFFERSTATUS glCheckFramebufferStatus = nullptr;
PFNGLDELETEFRAMEBUFFERS glDeleteFramebuffers = nullptr;
PFNGLISFRAMEBUFFER glIsFramebuffer = nullptr;
PFNGLGENRENDERBUFFERS glGenRenderbuffers = nullptr;
PFNGLBINDRENDERBUFFER glBindRenderbuffer = nullptr;
PFNGLRENDERBUFFERSTORAGE glRenderbufferStorage = nullptr;
PFNGLDELETERENDERBUFFERS glDeleteRenderbuffers = nullptr;
PFNGLISRENDERBUFFER glIsRenderbuffer = nullptr;
PFNGLGETRENDERBUFFERPARAMETERIV glGetRenderbufferParameteriv = nullptr;
PFNGLGENBUFFERS glGenBuffers = nullptr;
PFNGLBINDBUFFER glBindBuffer = nullptr;
PFNGLBUFFERDATA glBufferData = nullptr;
PFNGLBUFFERSUBDATA glBufferSubData = nullptr;
PFNGLDELETEBUFFERS glDeleteBuffers = nullptr;
PFNGLISBUFFER glIsBuffer = nullptr;
PFNGLGETBUFFERPARAMETERIV glGetBufferParameteriv = nullptr;
PFNGLCREATESHADER glCreateShader = nullptr;
PFNGLDELETESHADER glDeleteShader = nullptr;
PFNGLSHADERSOURCE glShaderSource = nullptr;
PFNGLSHADERBINARY glShaderBinary = nullptr;
PFNGLGETSHADERINFOLOG glGetShaderInfoLog = nullptr;
PFNGLGETSHADERSOURCE glGetShaderSource = nullptr;
PFNGLRELEASESHADERCOMPILER glReleaseShaderCompiler = nullptr;
PFNGLCOMPILESHADER glCompileShader = nullptr;
PFNGLISSHADER glIsShader = nullptr;
PFNGLCREATEPROGRAM glCreateProgram = nullptr;
PFNGLDELETEPROGRAM glDeleteProgram = nullptr;
PFNGLATTACHSHADER glAttachShader = nullptr;
PFNGLDETACHSHADER glDetachShader = nullptr;
PFNGLGETATTACHEDSHADERS glGetAttachedShaders = nullptr;
PFNGLLINKPROGRAM glLinkProgram = nullptr;
PFNGLGETPROGRAMINFOLOG glGetProgramInfoLog = nullptr;
PFNGLUSEPROGRAM glUseProgram = nullptr;
PFNGLISPROGRAM glIsProgram = nullptr;
PFNGLVALIDATEPROGRAM glValidateProgram = nullptr;
PFNGLCLEARCOLOR glClearColor = nullptr;
PFNGLCLEARDEPTHF glClearDepthf = nullptr;
PFNGLCLEARSTENCIL glClearStencil = nullptr;
PFNGLCLEAR glClear = nullptr;
PFNGLCULLFACE glCullFace = nullptr;
PFNGLPOLYGONOFFSET glPolygonOffset = nullptr;
PFNGLLINEWIDTH glLineWidth = nullptr;
PFNGLSAMPLECOVERAGE glSampleCoverage = nullptr;
PFNGLHINT glHint = nullptr;
PFNGLFRAMEBUFFERRENDERBUFFER glFramebufferRenderbuffer = nullptr;
PFNGLFRAMEBUFFERTEXTURE2D glFramebufferTexture2D = nullptr;
PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIV glGetFramebufferAttachmentParameteriv = nullptr;
PFNGLDRAWELEMENTS glDrawElements = nullptr;
PFNGLDRAWARRAYS glDrawArrays = nullptr;
PFNGLFLUSH glFlush = nullptr;
PFNGLFINISH glFinish = nullptr;
PFNGLGETBOOLEANV glGetBooleanv = nullptr;
PFNGLGETFLOATV glGetFloatv = nullptr;
PFNGLGETINTEGERV glGetIntegerv = nullptr;
PFNGLGETSTRING glGetString = nullptr;
PFNGLENABLE glEnable = nullptr;
PFNGLDISABLE glDisable = nullptr;
PFNGLISENABLED glIsEnabled = nullptr;
PFNGLFENCESYNC glFenceSync = nullptr;
PFNGLDELETESYNC glDeleteSync = nullptr;
PFNGLWAITSYNC glWaitSync = nullptr;
PFNGLCLIENTWAITSYNC glClientWaitSync = nullptr;
PFNGLMAPBUFFERRANGE glMapBufferRange = nullptr;
PFNGLUNMAPBUFFER glUnmapBuffer = nullptr;
PFNGLINVALIDATEFRAMEBUFFER glInvalidateFramebuffer = nullptr;
PFNGLRENDERBUFFERSTORAGEMULTISAMPLE glRenderbufferStorageMultisample = nullptr;
PFNGLBLITFRAMEBUFFER glBlitFramebuffer = nullptr;
PFNGLGENQUERIES glGenQueries = nullptr;
PFNGLBEGINQUERY glBeginQuery = nullptr;
PFNGLENDQUERY glEndQuery = nullptr;
PFNGLDELETEQUERIES glDeleteQueries = nullptr;
PFNGLISQUERY glIsQuery = nullptr;
PFNGLGETQUERYIV glGetQueryiv = nullptr;
PFNGLGETQUERYOBJECTUIV glGetQueryObjectuiv = nullptr;
PFNGLGETACTIVEUNIFORMBLOCKNAME glGetActiveUniformBlockName = nullptr;
PFNGLGETACTIVEUNIFORMBLOCKIV glGetActiveUniformBlockiv = nullptr;
PFNGLUNIFORMBLOCKBINDING glUniformBlockBinding = nullptr;
PFNGLGETACTIVEUNIFORMSIV glGetActiveUniformsiv = nullptr;
PFNGLBINDBUFFERBASE glBindBufferBase = nullptr;
PFNGLGENVERTEXARRAYS glGenVertexArrays = nullptr;
PFNGLBINDVERTEXARRAY glBindVertexArray = nullptr;
PFNGLDELETEVERTEXARRAYS glDeleteVertexArrays = nullptr;
PFNGLGETQUERYOBJECTI64V glGetQueryObjecti64v = nullptr;
PFNGLGETQUERYOBJECTUI64V glGetQueryObjectui64v = nullptr;
PFNGLGENQUERIESEXT glGenQueriesEXT = nullptr;
PFNGLBEGINQUERYEXT glBeginQueryEXT = nullptr;
PFNGLENDQUERYEXT glEndQueryEXT = nullptr;
PFNGLDELETEQUERIESEXT glDeleteQueriesEXT = nullptr;
PFNGLISQUERYEXT glIsQueryEXT = nullptr;
PFNGLQUERYCOUNTEREXT glQueryCounterEXT = nullptr;
PFNGLGETQUERYIVEXT glGetQueryivEXT = nullptr;
PFNGLGETQUERYOBJECTIVEXT glGetQueryObjectivEXT = nullptr;
PFNGLGETQUERYOBJECTUIVEXT glGetQueryObjectuivEXT = nullptr;
PFNGLGETQUERYOBJECTI64VEXT glGetQueryObjecti64vEXT = nullptr;
PFNGLGETQUERYOBJECTUI64VEXT glGetQueryObjectui64vEXT = nullptr;

void Register(Interpreter* interpreter) {
    interpreter->registerFunction(Ids::EglInitialize, callEglInitialize);
    interpreter->registerFunction(Ids::EglCreateContext, callEglCreateContext);
    interpreter->registerFunction(Ids::EglMakeCurrent, callEglMakeCurrent);
    interpreter->registerFunction(Ids::EglSwapBuffers, callEglSwapBuffers);
    interpreter->registerFunction(Ids::EglQuerySurface, callEglQuerySurface);
    interpreter->registerFunction(Ids::GlXCreateContext, callGlXCreateContext);
    interpreter->registerFunction(Ids::GlXCreateNewContext, callGlXCreateNewContext);
    interpreter->registerFunction(Ids::GlXMakeContextCurrent, callGlXMakeContextCurrent);
    interpreter->registerFunction(Ids::GlXMakeCurrent, callGlXMakeCurrent);
    interpreter->registerFunction(Ids::GlXSwapBuffers, callGlXSwapBuffers);
    interpreter->registerFunction(Ids::GlXQueryDrawable, callGlXQueryDrawable);
    interpreter->registerFunction(Ids::WglCreateContext, callWglCreateContext);
    interpreter->registerFunction(Ids::WglCreateContextAttribsARB, callWglCreateContextAttribsARB);
    interpreter->registerFunction(Ids::WglMakeCurrent, callWglMakeCurrent);
    interpreter->registerFunction(Ids::WglSwapBuffers, callWglSwapBuffers);
    interpreter->registerFunction(Ids::CGLCreateContext, callCGLCreateContext);
    interpreter->registerFunction(Ids::CGLSetCurrentContext, callCGLSetCurrentContext);
    interpreter->registerFunction(Ids::CGLGetSurface, callCGLGetSurface);
    interpreter->registerFunction(Ids::CGSGetSurfaceBounds, callCGSGetSurfaceBounds);
    interpreter->registerFunction(Ids::CGLFlushDrawable, callCGLFlushDrawable);
    interpreter->registerFunction(Ids::GlEnableClientState, callGlEnableClientState);
    interpreter->registerFunction(Ids::GlDisableClientState, callGlDisableClientState);
    interpreter->registerFunction(Ids::GlGetProgramBinaryOES, callGlGetProgramBinaryOES);
    interpreter->registerFunction(Ids::GlProgramBinaryOES, callGlProgramBinaryOES);
    interpreter->registerFunction(Ids::GlStartTilingQCOM, callGlStartTilingQCOM);
    interpreter->registerFunction(Ids::GlEndTilingQCOM, callGlEndTilingQCOM);
    interpreter->registerFunction(Ids::GlDiscardFramebufferEXT, callGlDiscardFramebufferEXT);
    interpreter->registerFunction(Ids::GlInsertEventMarkerEXT, callGlInsertEventMarkerEXT);
    interpreter->registerFunction(Ids::GlPushGroupMarkerEXT, callGlPushGroupMarkerEXT);
    interpreter->registerFunction(Ids::GlPopGroupMarkerEXT, callGlPopGroupMarkerEXT);
    interpreter->registerFunction(Ids::GlTexStorage1DEXT, callGlTexStorage1DEXT);
    interpreter->registerFunction(Ids::GlTexStorage2DEXT, callGlTexStorage2DEXT);
    interpreter->registerFunction(Ids::GlTexStorage3DEXT, callGlTexStorage3DEXT);
    interpreter->registerFunction(Ids::GlTextureStorage1DEXT, callGlTextureStorage1DEXT);
    interpreter->registerFunction(Ids::GlTextureStorage2DEXT, callGlTextureStorage2DEXT);
    interpreter->registerFunction(Ids::GlTextureStorage3DEXT, callGlTextureStorage3DEXT);
    interpreter->registerFunction(Ids::GlGenVertexArraysOES, callGlGenVertexArraysOES);
    interpreter->registerFunction(Ids::GlBindVertexArrayOES, callGlBindVertexArrayOES);
    interpreter->registerFunction(Ids::GlDeleteVertexArraysOES, callGlDeleteVertexArraysOES);
    interpreter->registerFunction(Ids::GlIsVertexArrayOES, callGlIsVertexArrayOES);
    interpreter->registerFunction(Ids::GlEGLImageTargetTexture2DOES,
                                  callGlEGLImageTargetTexture2DOES);
    interpreter->registerFunction(Ids::GlEGLImageTargetRenderbufferStorageOES,
                                  callGlEGLImageTargetRenderbufferStorageOES);
    interpreter->registerFunction(Ids::GlGetGraphicsResetStatusEXT,
                                  callGlGetGraphicsResetStatusEXT);
    interpreter->registerFunction(Ids::GlBindAttribLocation, callGlBindAttribLocation);
    interpreter->registerFunction(Ids::GlBlendFunc, callGlBlendFunc);
    interpreter->registerFunction(Ids::GlBlendFuncSeparate, callGlBlendFuncSeparate);
    interpreter->registerFunction(Ids::GlBlendEquation, callGlBlendEquation);
    interpreter->registerFunction(Ids::GlBlendEquationSeparate, callGlBlendEquationSeparate);
    interpreter->registerFunction(Ids::GlBlendColor, callGlBlendColor);
    interpreter->registerFunction(Ids::GlEnableVertexAttribArray, callGlEnableVertexAttribArray);
    interpreter->registerFunction(Ids::GlDisableVertexAttribArray, callGlDisableVertexAttribArray);
    interpreter->registerFunction(Ids::GlVertexAttribPointer, callGlVertexAttribPointer);
    interpreter->registerFunction(Ids::GlGetActiveAttrib, callGlGetActiveAttrib);
    interpreter->registerFunction(Ids::GlGetActiveUniform, callGlGetActiveUniform);
    interpreter->registerFunction(Ids::GlGetError, callGlGetError);
    interpreter->registerFunction(Ids::GlGetProgramiv, callGlGetProgramiv);
    interpreter->registerFunction(Ids::GlGetShaderiv, callGlGetShaderiv);
    interpreter->registerFunction(Ids::GlGetUniformLocation, callGlGetUniformLocation);
    interpreter->registerFunction(Ids::GlGetAttribLocation, callGlGetAttribLocation);
    interpreter->registerFunction(Ids::GlPixelStorei, callGlPixelStorei);
    interpreter->registerFunction(Ids::GlTexParameteri, callGlTexParameteri);
    interpreter->registerFunction(Ids::GlTexParameterf, callGlTexParameterf);
    interpreter->registerFunction(Ids::GlGetTexParameteriv, callGlGetTexParameteriv);
    interpreter->registerFunction(Ids::GlGetTexParameterfv, callGlGetTexParameterfv);
    interpreter->registerFunction(Ids::GlUniform1i, callGlUniform1i);
    interpreter->registerFunction(Ids::GlUniform2i, callGlUniform2i);
    interpreter->registerFunction(Ids::GlUniform3i, callGlUniform3i);
    interpreter->registerFunction(Ids::GlUniform4i, callGlUniform4i);
    interpreter->registerFunction(Ids::GlUniform1iv, callGlUniform1iv);
    interpreter->registerFunction(Ids::GlUniform2iv, callGlUniform2iv);
    interpreter->registerFunction(Ids::GlUniform3iv, callGlUniform3iv);
    interpreter->registerFunction(Ids::GlUniform4iv, callGlUniform4iv);
    interpreter->registerFunction(Ids::GlUniform1f, callGlUniform1f);
    interpreter->registerFunction(Ids::GlUniform2f, callGlUniform2f);
    interpreter->registerFunction(Ids::GlUniform3f, callGlUniform3f);
    interpreter->registerFunction(Ids::GlUniform4f, callGlUniform4f);
    interpreter->registerFunction(Ids::GlUniform1fv, callGlUniform1fv);
    interpreter->registerFunction(Ids::GlUniform2fv, callGlUniform2fv);
    interpreter->registerFunction(Ids::GlUniform3fv, callGlUniform3fv);
    interpreter->registerFunction(Ids::GlUniform4fv, callGlUniform4fv);
    interpreter->registerFunction(Ids::GlUniformMatrix2fv, callGlUniformMatrix2fv);
    interpreter->registerFunction(Ids::GlUniformMatrix3fv, callGlUniformMatrix3fv);
    interpreter->registerFunction(Ids::GlUniformMatrix4fv, callGlUniformMatrix4fv);
    interpreter->registerFunction(Ids::GlGetUniformfv, callGlGetUniformfv);
    interpreter->registerFunction(Ids::GlGetUniformiv, callGlGetUniformiv);
    interpreter->registerFunction(Ids::GlVertexAttrib1f, callGlVertexAttrib1f);
    interpreter->registerFunction(Ids::GlVertexAttrib2f, callGlVertexAttrib2f);
    interpreter->registerFunction(Ids::GlVertexAttrib3f, callGlVertexAttrib3f);
    interpreter->registerFunction(Ids::GlVertexAttrib4f, callGlVertexAttrib4f);
    interpreter->registerFunction(Ids::GlVertexAttrib1fv, callGlVertexAttrib1fv);
    interpreter->registerFunction(Ids::GlVertexAttrib2fv, callGlVertexAttrib2fv);
    interpreter->registerFunction(Ids::GlVertexAttrib3fv, callGlVertexAttrib3fv);
    interpreter->registerFunction(Ids::GlVertexAttrib4fv, callGlVertexAttrib4fv);
    interpreter->registerFunction(Ids::GlGetShaderPrecisionFormat, callGlGetShaderPrecisionFormat);
    interpreter->registerFunction(Ids::GlDepthMask, callGlDepthMask);
    interpreter->registerFunction(Ids::GlDepthFunc, callGlDepthFunc);
    interpreter->registerFunction(Ids::GlDepthRangef, callGlDepthRangef);
    interpreter->registerFunction(Ids::GlColorMask, callGlColorMask);
    interpreter->registerFunction(Ids::GlStencilMask, callGlStencilMask);
    interpreter->registerFunction(Ids::GlStencilMaskSeparate, callGlStencilMaskSeparate);
    interpreter->registerFunction(Ids::GlStencilFuncSeparate, callGlStencilFuncSeparate);
    interpreter->registerFunction(Ids::GlStencilOpSeparate, callGlStencilOpSeparate);
    interpreter->registerFunction(Ids::GlFrontFace, callGlFrontFace);
    interpreter->registerFunction(Ids::GlViewport, callGlViewport);
    interpreter->registerFunction(Ids::GlScissor, callGlScissor);
    interpreter->registerFunction(Ids::GlActiveTexture, callGlActiveTexture);
    interpreter->registerFunction(Ids::GlGenTextures, callGlGenTextures);
    interpreter->registerFunction(Ids::GlDeleteTextures, callGlDeleteTextures);
    interpreter->registerFunction(Ids::GlIsTexture, callGlIsTexture);
    interpreter->registerFunction(Ids::GlBindTexture, callGlBindTexture);
    interpreter->registerFunction(Ids::GlTexImage2D, callGlTexImage2D);
    interpreter->registerFunction(Ids::GlTexSubImage2D, callGlTexSubImage2D);
    interpreter->registerFunction(Ids::GlCopyTexImage2D, callGlCopyTexImage2D);
    interpreter->registerFunction(Ids::GlCopyTexSubImage2D, callGlCopyTexSubImage2D);
    interpreter->registerFunction(Ids::GlCompressedTexImage2D, callGlCompressedTexImage2D);
    interpreter->registerFunction(Ids::GlCompressedTexSubImage2D, callGlCompressedTexSubImage2D);
    interpreter->registerFunction(Ids::GlGenerateMipmap, callGlGenerateMipmap);
    interpreter->registerFunction(Ids::GlReadPixels, callGlReadPixels);
    interpreter->registerFunction(Ids::GlGenFramebuffers, callGlGenFramebuffers);
    interpreter->registerFunction(Ids::GlBindFramebuffer, callGlBindFramebuffer);
    interpreter->registerFunction(Ids::GlCheckFramebufferStatus, callGlCheckFramebufferStatus);
    interpreter->registerFunction(Ids::GlDeleteFramebuffers, callGlDeleteFramebuffers);
    interpreter->registerFunction(Ids::GlIsFramebuffer, callGlIsFramebuffer);
    interpreter->registerFunction(Ids::GlGenRenderbuffers, callGlGenRenderbuffers);
    interpreter->registerFunction(Ids::GlBindRenderbuffer, callGlBindRenderbuffer);
    interpreter->registerFunction(Ids::GlRenderbufferStorage, callGlRenderbufferStorage);
    interpreter->registerFunction(Ids::GlDeleteRenderbuffers, callGlDeleteRenderbuffers);
    interpreter->registerFunction(Ids::GlIsRenderbuffer, callGlIsRenderbuffer);
    interpreter->registerFunction(Ids::GlGetRenderbufferParameteriv,
                                  callGlGetRenderbufferParameteriv);
    interpreter->registerFunction(Ids::GlGenBuffers, callGlGenBuffers);
    interpreter->registerFunction(Ids::GlBindBuffer, callGlBindBuffer);
    interpreter->registerFunction(Ids::GlBufferData, callGlBufferData);
    interpreter->registerFunction(Ids::GlBufferSubData, callGlBufferSubData);
    interpreter->registerFunction(Ids::GlDeleteBuffers, callGlDeleteBuffers);
    interpreter->registerFunction(Ids::GlIsBuffer, callGlIsBuffer);
    interpreter->registerFunction(Ids::GlGetBufferParameteriv, callGlGetBufferParameteriv);
    interpreter->registerFunction(Ids::GlCreateShader, callGlCreateShader);
    interpreter->registerFunction(Ids::GlDeleteShader, callGlDeleteShader);
    interpreter->registerFunction(Ids::GlShaderSource, callGlShaderSource);
    interpreter->registerFunction(Ids::GlShaderBinary, callGlShaderBinary);
    interpreter->registerFunction(Ids::GlGetShaderInfoLog, callGlGetShaderInfoLog);
    interpreter->registerFunction(Ids::GlGetShaderSource, callGlGetShaderSource);
    interpreter->registerFunction(Ids::GlReleaseShaderCompiler, callGlReleaseShaderCompiler);
    interpreter->registerFunction(Ids::GlCompileShader, callGlCompileShader);
    interpreter->registerFunction(Ids::GlIsShader, callGlIsShader);
    interpreter->registerFunction(Ids::GlCreateProgram, callGlCreateProgram);
    interpreter->registerFunction(Ids::GlDeleteProgram, callGlDeleteProgram);
    interpreter->registerFunction(Ids::GlAttachShader, callGlAttachShader);
    interpreter->registerFunction(Ids::GlDetachShader, callGlDetachShader);
    interpreter->registerFunction(Ids::GlGetAttachedShaders, callGlGetAttachedShaders);
    interpreter->registerFunction(Ids::GlLinkProgram, callGlLinkProgram);
    interpreter->registerFunction(Ids::GlGetProgramInfoLog, callGlGetProgramInfoLog);
    interpreter->registerFunction(Ids::GlUseProgram, callGlUseProgram);
    interpreter->registerFunction(Ids::GlIsProgram, callGlIsProgram);
    interpreter->registerFunction(Ids::GlValidateProgram, callGlValidateProgram);
    interpreter->registerFunction(Ids::GlClearColor, callGlClearColor);
    interpreter->registerFunction(Ids::GlClearDepthf, callGlClearDepthf);
    interpreter->registerFunction(Ids::GlClearStencil, callGlClearStencil);
    interpreter->registerFunction(Ids::GlClear, callGlClear);
    interpreter->registerFunction(Ids::GlCullFace, callGlCullFace);
    interpreter->registerFunction(Ids::GlPolygonOffset, callGlPolygonOffset);
    interpreter->registerFunction(Ids::GlLineWidth, callGlLineWidth);
    interpreter->registerFunction(Ids::GlSampleCoverage, callGlSampleCoverage);
    interpreter->registerFunction(Ids::GlHint, callGlHint);
    interpreter->registerFunction(Ids::GlFramebufferRenderbuffer, callGlFramebufferRenderbuffer);
    interpreter->registerFunction(Ids::GlFramebufferTexture2D, callGlFramebufferTexture2D);
    interpreter->registerFunction(Ids::GlGetFramebufferAttachmentParameteriv,
                                  callGlGetFramebufferAttachmentParameteriv);
    interpreter->registerFunction(Ids::GlDrawElements, callGlDrawElements);
    interpreter->registerFunction(Ids::GlDrawArrays, callGlDrawArrays);
    interpreter->registerFunction(Ids::GlFlush, callGlFlush);
    interpreter->registerFunction(Ids::GlFinish, callGlFinish);
    interpreter->registerFunction(Ids::GlGetBooleanv, callGlGetBooleanv);
    interpreter->registerFunction(Ids::GlGetFloatv, callGlGetFloatv);
    interpreter->registerFunction(Ids::GlGetIntegerv, callGlGetIntegerv);
    interpreter->registerFunction(Ids::GlGetString, callGlGetString);
    interpreter->registerFunction(Ids::GlEnable, callGlEnable);
    interpreter->registerFunction(Ids::GlDisable, callGlDisable);
    interpreter->registerFunction(Ids::GlIsEnabled, callGlIsEnabled);
    interpreter->registerFunction(Ids::GlFenceSync, callGlFenceSync);
    interpreter->registerFunction(Ids::GlDeleteSync, callGlDeleteSync);
    interpreter->registerFunction(Ids::GlWaitSync, callGlWaitSync);
    interpreter->registerFunction(Ids::GlClientWaitSync, callGlClientWaitSync);
    interpreter->registerFunction(Ids::GlMapBufferRange, callGlMapBufferRange);
    interpreter->registerFunction(Ids::GlUnmapBuffer, callGlUnmapBuffer);
    interpreter->registerFunction(Ids::GlInvalidateFramebuffer, callGlInvalidateFramebuffer);
    interpreter->registerFunction(Ids::GlRenderbufferStorageMultisample,
                                  callGlRenderbufferStorageMultisample);
    interpreter->registerFunction(Ids::GlBlitFramebuffer, callGlBlitFramebuffer);
    interpreter->registerFunction(Ids::GlGenQueries, callGlGenQueries);
    interpreter->registerFunction(Ids::GlBeginQuery, callGlBeginQuery);
    interpreter->registerFunction(Ids::GlEndQuery, callGlEndQuery);
    interpreter->registerFunction(Ids::GlDeleteQueries, callGlDeleteQueries);
    interpreter->registerFunction(Ids::GlIsQuery, callGlIsQuery);
    interpreter->registerFunction(Ids::GlGetQueryiv, callGlGetQueryiv);
    interpreter->registerFunction(Ids::GlGetQueryObjectuiv, callGlGetQueryObjectuiv);
    interpreter->registerFunction(Ids::GlGetActiveUniformBlockName,
                                  callGlGetActiveUniformBlockName);
    interpreter->registerFunction(Ids::GlGetActiveUniformBlockiv, callGlGetActiveUniformBlockiv);
    interpreter->registerFunction(Ids::GlUniformBlockBinding, callGlUniformBlockBinding);
    interpreter->registerFunction(Ids::GlGetActiveUniformsiv, callGlGetActiveUniformsiv);
    interpreter->registerFunction(Ids::GlBindBufferBase, callGlBindBufferBase);
    interpreter->registerFunction(Ids::GlGenVertexArrays, callGlGenVertexArrays);
    interpreter->registerFunction(Ids::GlBindVertexArray, callGlBindVertexArray);
    interpreter->registerFunction(Ids::GlDeleteVertexArrays, callGlDeleteVertexArrays);
    interpreter->registerFunction(Ids::GlGetQueryObjecti64v, callGlGetQueryObjecti64v);
    interpreter->registerFunction(Ids::GlGetQueryObjectui64v, callGlGetQueryObjectui64v);
    interpreter->registerFunction(Ids::GlGenQueriesEXT, callGlGenQueriesEXT);
    interpreter->registerFunction(Ids::GlBeginQueryEXT, callGlBeginQueryEXT);
    interpreter->registerFunction(Ids::GlEndQueryEXT, callGlEndQueryEXT);
    interpreter->registerFunction(Ids::GlDeleteQueriesEXT, callGlDeleteQueriesEXT);
    interpreter->registerFunction(Ids::GlIsQueryEXT, callGlIsQueryEXT);
    interpreter->registerFunction(Ids::GlQueryCounterEXT, callGlQueryCounterEXT);
    interpreter->registerFunction(Ids::GlGetQueryivEXT, callGlGetQueryivEXT);
    interpreter->registerFunction(Ids::GlGetQueryObjectivEXT, callGlGetQueryObjectivEXT);
    interpreter->registerFunction(Ids::GlGetQueryObjectuivEXT, callGlGetQueryObjectuivEXT);
    interpreter->registerFunction(Ids::GlGetQueryObjecti64vEXT, callGlGetQueryObjecti64vEXT);
    interpreter->registerFunction(Ids::GlGetQueryObjectui64vEXT, callGlGetQueryObjectui64vEXT);
}
void Initialize() {
    eglInitialize =
            reinterpret_cast<PFNEGLINITIALIZE>(gapic::GetGfxProcAddress("eglInitialize", false));
    eglCreateContext = reinterpret_cast<PFNEGLCREATECONTEXT>(
            gapic::GetGfxProcAddress("eglCreateContext", false));
    eglMakeCurrent =
            reinterpret_cast<PFNEGLMAKECURRENT>(gapic::GetGfxProcAddress("eglMakeCurrent", false));
    eglSwapBuffers =
            reinterpret_cast<PFNEGLSWAPBUFFERS>(gapic::GetGfxProcAddress("eglSwapBuffers", false));
    eglQuerySurface = reinterpret_cast<PFNEGLQUERYSURFACE>(
            gapic::GetGfxProcAddress("eglQuerySurface", false));
    glXCreateContext = reinterpret_cast<PFNGLXCREATECONTEXT>(
            gapic::GetGfxProcAddress("glXCreateContext", false));
    glXCreateNewContext = reinterpret_cast<PFNGLXCREATENEWCONTEXT>(
            gapic::GetGfxProcAddress("glXCreateNewContext", false));
    glXMakeContextCurrent = reinterpret_cast<PFNGLXMAKECONTEXTCURRENT>(
            gapic::GetGfxProcAddress("glXMakeContextCurrent", false));
    glXMakeCurrent =
            reinterpret_cast<PFNGLXMAKECURRENT>(gapic::GetGfxProcAddress("glXMakeCurrent", false));
    glXSwapBuffers =
            reinterpret_cast<PFNGLXSWAPBUFFERS>(gapic::GetGfxProcAddress("glXSwapBuffers", false));
    glXQueryDrawable = reinterpret_cast<PFNGLXQUERYDRAWABLE>(
            gapic::GetGfxProcAddress("glXQueryDrawable", false));
    wglCreateContext = reinterpret_cast<PFNWGLCREATECONTEXT>(
            gapic::GetGfxProcAddress("wglCreateContext", false));
    wglCreateContextAttribsARB = reinterpret_cast<PFNWGLCREATECONTEXTATTRIBSARB>(
            gapic::GetGfxProcAddress("wglCreateContextAttribsARB", false));
    wglMakeCurrent =
            reinterpret_cast<PFNWGLMAKECURRENT>(gapic::GetGfxProcAddress("wglMakeCurrent", false));
    wglSwapBuffers =
            reinterpret_cast<PFNWGLSWAPBUFFERS>(gapic::GetGfxProcAddress("wglSwapBuffers", false));
    CGLCreateContext = reinterpret_cast<PFNCGLCREATECONTEXT>(
            gapic::GetGfxProcAddress("CGLCreateContext", false));
    CGLSetCurrentContext = reinterpret_cast<PFNCGLSETCURRENTCONTEXT>(
            gapic::GetGfxProcAddress("CGLSetCurrentContext", false));
    CGLGetSurface =
            reinterpret_cast<PFNCGLGETSURFACE>(gapic::GetGfxProcAddress("CGLGetSurface", false));
    CGSGetSurfaceBounds = reinterpret_cast<PFNCGSGETSURFACEBOUNDS>(
            gapic::GetGfxProcAddress("CGSGetSurfaceBounds", false));
    CGLFlushDrawable = reinterpret_cast<PFNCGLFLUSHDRAWABLE>(
            gapic::GetGfxProcAddress("CGLFlushDrawable", false));
    glEnableClientState = reinterpret_cast<PFNGLENABLECLIENTSTATE>(
            gapic::GetGfxProcAddress("glEnableClientState", false));
    glDisableClientState = reinterpret_cast<PFNGLDISABLECLIENTSTATE>(
            gapic::GetGfxProcAddress("glDisableClientState", false));
    glGetProgramBinaryOES = reinterpret_cast<PFNGLGETPROGRAMBINARYOES>(
            gapic::GetGfxProcAddress("glGetProgramBinaryOES", false));
    glProgramBinaryOES = reinterpret_cast<PFNGLPROGRAMBINARYOES>(
            gapic::GetGfxProcAddress("glProgramBinaryOES", false));
    glStartTilingQCOM = reinterpret_cast<PFNGLSTARTTILINGQCOM>(
            gapic::GetGfxProcAddress("glStartTilingQCOM", false));
    glEndTilingQCOM = reinterpret_cast<PFNGLENDTILINGQCOM>(
            gapic::GetGfxProcAddress("glEndTilingQCOM", false));
    glDiscardFramebufferEXT = reinterpret_cast<PFNGLDISCARDFRAMEBUFFEREXT>(
            gapic::GetGfxProcAddress("glDiscardFramebufferEXT", false));
    glInsertEventMarkerEXT = reinterpret_cast<PFNGLINSERTEVENTMARKEREXT>(
            gapic::GetGfxProcAddress("glInsertEventMarkerEXT", false));
    glPushGroupMarkerEXT = reinterpret_cast<PFNGLPUSHGROUPMARKEREXT>(
            gapic::GetGfxProcAddress("glPushGroupMarkerEXT", false));
    glPopGroupMarkerEXT = reinterpret_cast<PFNGLPOPGROUPMARKEREXT>(
            gapic::GetGfxProcAddress("glPopGroupMarkerEXT", false));
    glTexStorage1DEXT = reinterpret_cast<PFNGLTEXSTORAGE1DEXT>(
            gapic::GetGfxProcAddress("glTexStorage1DEXT", false));
    glTexStorage2DEXT = reinterpret_cast<PFNGLTEXSTORAGE2DEXT>(
            gapic::GetGfxProcAddress("glTexStorage2DEXT", false));
    glTexStorage3DEXT = reinterpret_cast<PFNGLTEXSTORAGE3DEXT>(
            gapic::GetGfxProcAddress("glTexStorage3DEXT", false));
    glTextureStorage1DEXT = reinterpret_cast<PFNGLTEXTURESTORAGE1DEXT>(
            gapic::GetGfxProcAddress("glTextureStorage1DEXT", false));
    glTextureStorage2DEXT = reinterpret_cast<PFNGLTEXTURESTORAGE2DEXT>(
            gapic::GetGfxProcAddress("glTextureStorage2DEXT", false));
    glTextureStorage3DEXT = reinterpret_cast<PFNGLTEXTURESTORAGE3DEXT>(
            gapic::GetGfxProcAddress("glTextureStorage3DEXT", false));
    glGenVertexArraysOES = reinterpret_cast<PFNGLGENVERTEXARRAYSOES>(
            gapic::GetGfxProcAddress("glGenVertexArraysOES", false));
    glBindVertexArrayOES = reinterpret_cast<PFNGLBINDVERTEXARRAYOES>(
            gapic::GetGfxProcAddress("glBindVertexArrayOES", false));
    glDeleteVertexArraysOES = reinterpret_cast<PFNGLDELETEVERTEXARRAYSOES>(
            gapic::GetGfxProcAddress("glDeleteVertexArraysOES", false));
    glIsVertexArrayOES = reinterpret_cast<PFNGLISVERTEXARRAYOES>(
            gapic::GetGfxProcAddress("glIsVertexArrayOES", false));
    glEGLImageTargetTexture2DOES = reinterpret_cast<PFNGLEGLIMAGETARGETTEXTURE2DOES>(
            gapic::GetGfxProcAddress("glEGLImageTargetTexture2DOES", false));
    glEGLImageTargetRenderbufferStorageOES =
            reinterpret_cast<PFNGLEGLIMAGETARGETRENDERBUFFERSTORAGEOES>(
                    gapic::GetGfxProcAddress("glEGLImageTargetRenderbufferStorageOES", false));
    glGetGraphicsResetStatusEXT = reinterpret_cast<PFNGLGETGRAPHICSRESETSTATUSEXT>(
            gapic::GetGfxProcAddress("glGetGraphicsResetStatusEXT", false));
    glBindAttribLocation = reinterpret_cast<PFNGLBINDATTRIBLOCATION>(
            gapic::GetGfxProcAddress("glBindAttribLocation", false));
    glBlendFunc = reinterpret_cast<PFNGLBLENDFUNC>(gapic::GetGfxProcAddress("glBlendFunc", false));
    glBlendFuncSeparate = reinterpret_cast<PFNGLBLENDFUNCSEPARATE>(
            gapic::GetGfxProcAddress("glBlendFuncSeparate", false));
    glBlendEquation = reinterpret_cast<PFNGLBLENDEQUATION>(
            gapic::GetGfxProcAddress("glBlendEquation", false));
    glBlendEquationSeparate = reinterpret_cast<PFNGLBLENDEQUATIONSEPARATE>(
            gapic::GetGfxProcAddress("glBlendEquationSeparate", false));
    glBlendColor =
            reinterpret_cast<PFNGLBLENDCOLOR>(gapic::GetGfxProcAddress("glBlendColor", false));
    glEnableVertexAttribArray = reinterpret_cast<PFNGLENABLEVERTEXATTRIBARRAY>(
            gapic::GetGfxProcAddress("glEnableVertexAttribArray", false));
    glDisableVertexAttribArray = reinterpret_cast<PFNGLDISABLEVERTEXATTRIBARRAY>(
            gapic::GetGfxProcAddress("glDisableVertexAttribArray", false));
    glVertexAttribPointer = reinterpret_cast<PFNGLVERTEXATTRIBPOINTER>(
            gapic::GetGfxProcAddress("glVertexAttribPointer", false));
    glGetActiveAttrib = reinterpret_cast<PFNGLGETACTIVEATTRIB>(
            gapic::GetGfxProcAddress("glGetActiveAttrib", false));
    glGetActiveUniform = reinterpret_cast<PFNGLGETACTIVEUNIFORM>(
            gapic::GetGfxProcAddress("glGetActiveUniform", false));
    glGetError = reinterpret_cast<PFNGLGETERROR>(gapic::GetGfxProcAddress("glGetError", false));
    glGetProgramiv =
            reinterpret_cast<PFNGLGETPROGRAMIV>(gapic::GetGfxProcAddress("glGetProgramiv", false));
    glGetShaderiv =
            reinterpret_cast<PFNGLGETSHADERIV>(gapic::GetGfxProcAddress("glGetShaderiv", false));
    glGetUniformLocation = reinterpret_cast<PFNGLGETUNIFORMLOCATION>(
            gapic::GetGfxProcAddress("glGetUniformLocation", false));
    glGetAttribLocation = reinterpret_cast<PFNGLGETATTRIBLOCATION>(
            gapic::GetGfxProcAddress("glGetAttribLocation", false));
    glPixelStorei =
            reinterpret_cast<PFNGLPIXELSTOREI>(gapic::GetGfxProcAddress("glPixelStorei", false));
    glTexParameteri = reinterpret_cast<PFNGLTEXPARAMETERI>(
            gapic::GetGfxProcAddress("glTexParameteri", false));
    glTexParameterf = reinterpret_cast<PFNGLTEXPARAMETERF>(
            gapic::GetGfxProcAddress("glTexParameterf", false));
    glGetTexParameteriv = reinterpret_cast<PFNGLGETTEXPARAMETERIV>(
            gapic::GetGfxProcAddress("glGetTexParameteriv", false));
    glGetTexParameterfv = reinterpret_cast<PFNGLGETTEXPARAMETERFV>(
            gapic::GetGfxProcAddress("glGetTexParameterfv", false));
    glUniform1i = reinterpret_cast<PFNGLUNIFORM1I>(gapic::GetGfxProcAddress("glUniform1i", false));
    glUniform2i = reinterpret_cast<PFNGLUNIFORM2I>(gapic::GetGfxProcAddress("glUniform2i", false));
    glUniform3i = reinterpret_cast<PFNGLUNIFORM3I>(gapic::GetGfxProcAddress("glUniform3i", false));
    glUniform4i = reinterpret_cast<PFNGLUNIFORM4I>(gapic::GetGfxProcAddress("glUniform4i", false));
    glUniform1iv =
            reinterpret_cast<PFNGLUNIFORM1IV>(gapic::GetGfxProcAddress("glUniform1iv", false));
    glUniform2iv =
            reinterpret_cast<PFNGLUNIFORM2IV>(gapic::GetGfxProcAddress("glUniform2iv", false));
    glUniform3iv =
            reinterpret_cast<PFNGLUNIFORM3IV>(gapic::GetGfxProcAddress("glUniform3iv", false));
    glUniform4iv =
            reinterpret_cast<PFNGLUNIFORM4IV>(gapic::GetGfxProcAddress("glUniform4iv", false));
    glUniform1f = reinterpret_cast<PFNGLUNIFORM1F>(gapic::GetGfxProcAddress("glUniform1f", false));
    glUniform2f = reinterpret_cast<PFNGLUNIFORM2F>(gapic::GetGfxProcAddress("glUniform2f", false));
    glUniform3f = reinterpret_cast<PFNGLUNIFORM3F>(gapic::GetGfxProcAddress("glUniform3f", false));
    glUniform4f = reinterpret_cast<PFNGLUNIFORM4F>(gapic::GetGfxProcAddress("glUniform4f", false));
    glUniform1fv =
            reinterpret_cast<PFNGLUNIFORM1FV>(gapic::GetGfxProcAddress("glUniform1fv", false));
    glUniform2fv =
            reinterpret_cast<PFNGLUNIFORM2FV>(gapic::GetGfxProcAddress("glUniform2fv", false));
    glUniform3fv =
            reinterpret_cast<PFNGLUNIFORM3FV>(gapic::GetGfxProcAddress("glUniform3fv", false));
    glUniform4fv =
            reinterpret_cast<PFNGLUNIFORM4FV>(gapic::GetGfxProcAddress("glUniform4fv", false));
    glUniformMatrix2fv = reinterpret_cast<PFNGLUNIFORMMATRIX2FV>(
            gapic::GetGfxProcAddress("glUniformMatrix2fv", false));
    glUniformMatrix3fv = reinterpret_cast<PFNGLUNIFORMMATRIX3FV>(
            gapic::GetGfxProcAddress("glUniformMatrix3fv", false));
    glUniformMatrix4fv = reinterpret_cast<PFNGLUNIFORMMATRIX4FV>(
            gapic::GetGfxProcAddress("glUniformMatrix4fv", false));
    glGetUniformfv =
            reinterpret_cast<PFNGLGETUNIFORMFV>(gapic::GetGfxProcAddress("glGetUniformfv", false));
    glGetUniformiv =
            reinterpret_cast<PFNGLGETUNIFORMIV>(gapic::GetGfxProcAddress("glGetUniformiv", false));
    glVertexAttrib1f = reinterpret_cast<PFNGLVERTEXATTRIB1F>(
            gapic::GetGfxProcAddress("glVertexAttrib1f", false));
    glVertexAttrib2f = reinterpret_cast<PFNGLVERTEXATTRIB2F>(
            gapic::GetGfxProcAddress("glVertexAttrib2f", false));
    glVertexAttrib3f = reinterpret_cast<PFNGLVERTEXATTRIB3F>(
            gapic::GetGfxProcAddress("glVertexAttrib3f", false));
    glVertexAttrib4f = reinterpret_cast<PFNGLVERTEXATTRIB4F>(
            gapic::GetGfxProcAddress("glVertexAttrib4f", false));
    glVertexAttrib1fv = reinterpret_cast<PFNGLVERTEXATTRIB1FV>(
            gapic::GetGfxProcAddress("glVertexAttrib1fv", false));
    glVertexAttrib2fv = reinterpret_cast<PFNGLVERTEXATTRIB2FV>(
            gapic::GetGfxProcAddress("glVertexAttrib2fv", false));
    glVertexAttrib3fv = reinterpret_cast<PFNGLVERTEXATTRIB3FV>(
            gapic::GetGfxProcAddress("glVertexAttrib3fv", false));
    glVertexAttrib4fv = reinterpret_cast<PFNGLVERTEXATTRIB4FV>(
            gapic::GetGfxProcAddress("glVertexAttrib4fv", false));
    glGetShaderPrecisionFormat = reinterpret_cast<PFNGLGETSHADERPRECISIONFORMAT>(
            gapic::GetGfxProcAddress("glGetShaderPrecisionFormat", false));
    glDepthMask = reinterpret_cast<PFNGLDEPTHMASK>(gapic::GetGfxProcAddress("glDepthMask", false));
    glDepthFunc = reinterpret_cast<PFNGLDEPTHFUNC>(gapic::GetGfxProcAddress("glDepthFunc", false));
    glDepthRangef =
            reinterpret_cast<PFNGLDEPTHRANGEF>(gapic::GetGfxProcAddress("glDepthRangef", false));
    glColorMask = reinterpret_cast<PFNGLCOLORMASK>(gapic::GetGfxProcAddress("glColorMask", false));
    glStencilMask =
            reinterpret_cast<PFNGLSTENCILMASK>(gapic::GetGfxProcAddress("glStencilMask", false));
    glStencilMaskSeparate = reinterpret_cast<PFNGLSTENCILMASKSEPARATE>(
            gapic::GetGfxProcAddress("glStencilMaskSeparate", false));
    glStencilFuncSeparate = reinterpret_cast<PFNGLSTENCILFUNCSEPARATE>(
            gapic::GetGfxProcAddress("glStencilFuncSeparate", false));
    glStencilOpSeparate = reinterpret_cast<PFNGLSTENCILOPSEPARATE>(
            gapic::GetGfxProcAddress("glStencilOpSeparate", false));
    glFrontFace = reinterpret_cast<PFNGLFRONTFACE>(gapic::GetGfxProcAddress("glFrontFace", false));
    glViewport = reinterpret_cast<PFNGLVIEWPORT>(gapic::GetGfxProcAddress("glViewport", false));
    glScissor = reinterpret_cast<PFNGLSCISSOR>(gapic::GetGfxProcAddress("glScissor", false));
    glActiveTexture = reinterpret_cast<PFNGLACTIVETEXTURE>(
            gapic::GetGfxProcAddress("glActiveTexture", false));
    glGenTextures =
            reinterpret_cast<PFNGLGENTEXTURES>(gapic::GetGfxProcAddress("glGenTextures", false));
    glDeleteTextures = reinterpret_cast<PFNGLDELETETEXTURES>(
            gapic::GetGfxProcAddress("glDeleteTextures", false));
    glIsTexture = reinterpret_cast<PFNGLISTEXTURE>(gapic::GetGfxProcAddress("glIsTexture", false));
    glBindTexture =
            reinterpret_cast<PFNGLBINDTEXTURE>(gapic::GetGfxProcAddress("glBindTexture", false));
    glTexImage2D =
            reinterpret_cast<PFNGLTEXIMAGE2D>(gapic::GetGfxProcAddress("glTexImage2D", false));
    glTexSubImage2D = reinterpret_cast<PFNGLTEXSUBIMAGE2D>(
            gapic::GetGfxProcAddress("glTexSubImage2D", false));
    glCopyTexImage2D = reinterpret_cast<PFNGLCOPYTEXIMAGE2D>(
            gapic::GetGfxProcAddress("glCopyTexImage2D", false));
    glCopyTexSubImage2D = reinterpret_cast<PFNGLCOPYTEXSUBIMAGE2D>(
            gapic::GetGfxProcAddress("glCopyTexSubImage2D", false));
    glCompressedTexImage2D = reinterpret_cast<PFNGLCOMPRESSEDTEXIMAGE2D>(
            gapic::GetGfxProcAddress("glCompressedTexImage2D", false));
    glCompressedTexSubImage2D = reinterpret_cast<PFNGLCOMPRESSEDTEXSUBIMAGE2D>(
            gapic::GetGfxProcAddress("glCompressedTexSubImage2D", false));
    glGenerateMipmap = reinterpret_cast<PFNGLGENERATEMIPMAP>(
            gapic::GetGfxProcAddress("glGenerateMipmap", false));
    glReadPixels =
            reinterpret_cast<PFNGLREADPIXELS>(gapic::GetGfxProcAddress("glReadPixels", false));
    glGenFramebuffers = reinterpret_cast<PFNGLGENFRAMEBUFFERS>(
            gapic::GetGfxProcAddress("glGenFramebuffers", false));
    glBindFramebuffer = reinterpret_cast<PFNGLBINDFRAMEBUFFER>(
            gapic::GetGfxProcAddress("glBindFramebuffer", false));
    glCheckFramebufferStatus = reinterpret_cast<PFNGLCHECKFRAMEBUFFERSTATUS>(
            gapic::GetGfxProcAddress("glCheckFramebufferStatus", false));
    glDeleteFramebuffers = reinterpret_cast<PFNGLDELETEFRAMEBUFFERS>(
            gapic::GetGfxProcAddress("glDeleteFramebuffers", false));
    glIsFramebuffer = reinterpret_cast<PFNGLISFRAMEBUFFER>(
            gapic::GetGfxProcAddress("glIsFramebuffer", false));
    glGenRenderbuffers = reinterpret_cast<PFNGLGENRENDERBUFFERS>(
            gapic::GetGfxProcAddress("glGenRenderbuffers", false));
    glBindRenderbuffer = reinterpret_cast<PFNGLBINDRENDERBUFFER>(
            gapic::GetGfxProcAddress("glBindRenderbuffer", false));
    glRenderbufferStorage = reinterpret_cast<PFNGLRENDERBUFFERSTORAGE>(
            gapic::GetGfxProcAddress("glRenderbufferStorage", false));
    glDeleteRenderbuffers = reinterpret_cast<PFNGLDELETERENDERBUFFERS>(
            gapic::GetGfxProcAddress("glDeleteRenderbuffers", false));
    glIsRenderbuffer = reinterpret_cast<PFNGLISRENDERBUFFER>(
            gapic::GetGfxProcAddress("glIsRenderbuffer", false));
    glGetRenderbufferParameteriv = reinterpret_cast<PFNGLGETRENDERBUFFERPARAMETERIV>(
            gapic::GetGfxProcAddress("glGetRenderbufferParameteriv", false));
    glGenBuffers =
            reinterpret_cast<PFNGLGENBUFFERS>(gapic::GetGfxProcAddress("glGenBuffers", false));
    glBindBuffer =
            reinterpret_cast<PFNGLBINDBUFFER>(gapic::GetGfxProcAddress("glBindBuffer", false));
    glBufferData =
            reinterpret_cast<PFNGLBUFFERDATA>(gapic::GetGfxProcAddress("glBufferData", false));
    glBufferSubData = reinterpret_cast<PFNGLBUFFERSUBDATA>(
            gapic::GetGfxProcAddress("glBufferSubData", false));
    glDeleteBuffers = reinterpret_cast<PFNGLDELETEBUFFERS>(
            gapic::GetGfxProcAddress("glDeleteBuffers", false));
    glIsBuffer = reinterpret_cast<PFNGLISBUFFER>(gapic::GetGfxProcAddress("glIsBuffer", false));
    glGetBufferParameteriv = reinterpret_cast<PFNGLGETBUFFERPARAMETERIV>(
            gapic::GetGfxProcAddress("glGetBufferParameteriv", false));
    glCreateShader =
            reinterpret_cast<PFNGLCREATESHADER>(gapic::GetGfxProcAddress("glCreateShader", false));
    glDeleteShader =
            reinterpret_cast<PFNGLDELETESHADER>(gapic::GetGfxProcAddress("glDeleteShader", false));
    glShaderSource =
            reinterpret_cast<PFNGLSHADERSOURCE>(gapic::GetGfxProcAddress("glShaderSource", false));
    glShaderBinary =
            reinterpret_cast<PFNGLSHADERBINARY>(gapic::GetGfxProcAddress("glShaderBinary", false));
    glGetShaderInfoLog = reinterpret_cast<PFNGLGETSHADERINFOLOG>(
            gapic::GetGfxProcAddress("glGetShaderInfoLog", false));
    glGetShaderSource = reinterpret_cast<PFNGLGETSHADERSOURCE>(
            gapic::GetGfxProcAddress("glGetShaderSource", false));
    glReleaseShaderCompiler = reinterpret_cast<PFNGLRELEASESHADERCOMPILER>(
            gapic::GetGfxProcAddress("glReleaseShaderCompiler", false));
    glCompileShader = reinterpret_cast<PFNGLCOMPILESHADER>(
            gapic::GetGfxProcAddress("glCompileShader", false));
    glIsShader = reinterpret_cast<PFNGLISSHADER>(gapic::GetGfxProcAddress("glIsShader", false));
    glCreateProgram = reinterpret_cast<PFNGLCREATEPROGRAM>(
            gapic::GetGfxProcAddress("glCreateProgram", false));
    glDeleteProgram = reinterpret_cast<PFNGLDELETEPROGRAM>(
            gapic::GetGfxProcAddress("glDeleteProgram", false));
    glAttachShader =
            reinterpret_cast<PFNGLATTACHSHADER>(gapic::GetGfxProcAddress("glAttachShader", false));
    glDetachShader =
            reinterpret_cast<PFNGLDETACHSHADER>(gapic::GetGfxProcAddress("glDetachShader", false));
    glGetAttachedShaders = reinterpret_cast<PFNGLGETATTACHEDSHADERS>(
            gapic::GetGfxProcAddress("glGetAttachedShaders", false));
    glLinkProgram =
            reinterpret_cast<PFNGLLINKPROGRAM>(gapic::GetGfxProcAddress("glLinkProgram", false));
    glGetProgramInfoLog = reinterpret_cast<PFNGLGETPROGRAMINFOLOG>(
            gapic::GetGfxProcAddress("glGetProgramInfoLog", false));
    glUseProgram =
            reinterpret_cast<PFNGLUSEPROGRAM>(gapic::GetGfxProcAddress("glUseProgram", false));
    glIsProgram = reinterpret_cast<PFNGLISPROGRAM>(gapic::GetGfxProcAddress("glIsProgram", false));
    glValidateProgram = reinterpret_cast<PFNGLVALIDATEPROGRAM>(
            gapic::GetGfxProcAddress("glValidateProgram", false));
    glClearColor =
            reinterpret_cast<PFNGLCLEARCOLOR>(gapic::GetGfxProcAddress("glClearColor", false));
    glClearDepthf =
            reinterpret_cast<PFNGLCLEARDEPTHF>(gapic::GetGfxProcAddress("glClearDepthf", false));
    glClearStencil =
            reinterpret_cast<PFNGLCLEARSTENCIL>(gapic::GetGfxProcAddress("glClearStencil", false));
    glClear = reinterpret_cast<PFNGLCLEAR>(gapic::GetGfxProcAddress("glClear", false));
    glCullFace = reinterpret_cast<PFNGLCULLFACE>(gapic::GetGfxProcAddress("glCullFace", false));
    glPolygonOffset = reinterpret_cast<PFNGLPOLYGONOFFSET>(
            gapic::GetGfxProcAddress("glPolygonOffset", false));
    glLineWidth = reinterpret_cast<PFNGLLINEWIDTH>(gapic::GetGfxProcAddress("glLineWidth", false));
    glSampleCoverage = reinterpret_cast<PFNGLSAMPLECOVERAGE>(
            gapic::GetGfxProcAddress("glSampleCoverage", false));
    glHint = reinterpret_cast<PFNGLHINT>(gapic::GetGfxProcAddress("glHint", false));
    glFramebufferRenderbuffer = reinterpret_cast<PFNGLFRAMEBUFFERRENDERBUFFER>(
            gapic::GetGfxProcAddress("glFramebufferRenderbuffer", false));
    glFramebufferTexture2D = reinterpret_cast<PFNGLFRAMEBUFFERTEXTURE2D>(
            gapic::GetGfxProcAddress("glFramebufferTexture2D", false));
    glGetFramebufferAttachmentParameteriv =
            reinterpret_cast<PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIV>(
                    gapic::GetGfxProcAddress("glGetFramebufferAttachmentParameteriv", false));
    glDrawElements =
            reinterpret_cast<PFNGLDRAWELEMENTS>(gapic::GetGfxProcAddress("glDrawElements", false));
    glDrawArrays =
            reinterpret_cast<PFNGLDRAWARRAYS>(gapic::GetGfxProcAddress("glDrawArrays", false));
    glFlush = reinterpret_cast<PFNGLFLUSH>(gapic::GetGfxProcAddress("glFlush", false));
    glFinish = reinterpret_cast<PFNGLFINISH>(gapic::GetGfxProcAddress("glFinish", false));
    glGetBooleanv =
            reinterpret_cast<PFNGLGETBOOLEANV>(gapic::GetGfxProcAddress("glGetBooleanv", false));
    glGetFloatv = reinterpret_cast<PFNGLGETFLOATV>(gapic::GetGfxProcAddress("glGetFloatv", false));
    glGetIntegerv =
            reinterpret_cast<PFNGLGETINTEGERV>(gapic::GetGfxProcAddress("glGetIntegerv", false));
    glGetString = reinterpret_cast<PFNGLGETSTRING>(gapic::GetGfxProcAddress("glGetString", false));
    glEnable = reinterpret_cast<PFNGLENABLE>(gapic::GetGfxProcAddress("glEnable", false));
    glDisable = reinterpret_cast<PFNGLDISABLE>(gapic::GetGfxProcAddress("glDisable", false));
    glIsEnabled = reinterpret_cast<PFNGLISENABLED>(gapic::GetGfxProcAddress("glIsEnabled", false));
    glFenceSync = reinterpret_cast<PFNGLFENCESYNC>(gapic::GetGfxProcAddress("glFenceSync", false));
    glDeleteSync =
            reinterpret_cast<PFNGLDELETESYNC>(gapic::GetGfxProcAddress("glDeleteSync", false));
    glWaitSync = reinterpret_cast<PFNGLWAITSYNC>(gapic::GetGfxProcAddress("glWaitSync", false));
    glClientWaitSync = reinterpret_cast<PFNGLCLIENTWAITSYNC>(
            gapic::GetGfxProcAddress("glClientWaitSync", false));
    glMapBufferRange = reinterpret_cast<PFNGLMAPBUFFERRANGE>(
            gapic::GetGfxProcAddress("glMapBufferRange", false));
    glUnmapBuffer =
            reinterpret_cast<PFNGLUNMAPBUFFER>(gapic::GetGfxProcAddress("glUnmapBuffer", false));
    glInvalidateFramebuffer = reinterpret_cast<PFNGLINVALIDATEFRAMEBUFFER>(
            gapic::GetGfxProcAddress("glInvalidateFramebuffer", false));
    glRenderbufferStorageMultisample = reinterpret_cast<PFNGLRENDERBUFFERSTORAGEMULTISAMPLE>(
            gapic::GetGfxProcAddress("glRenderbufferStorageMultisample", false));
    glBlitFramebuffer = reinterpret_cast<PFNGLBLITFRAMEBUFFER>(
            gapic::GetGfxProcAddress("glBlitFramebuffer", false));
    glGenQueries =
            reinterpret_cast<PFNGLGENQUERIES>(gapic::GetGfxProcAddress("glGenQueries", false));
    glBeginQuery =
            reinterpret_cast<PFNGLBEGINQUERY>(gapic::GetGfxProcAddress("glBeginQuery", false));
    glEndQuery = reinterpret_cast<PFNGLENDQUERY>(gapic::GetGfxProcAddress("glEndQuery", false));
    glDeleteQueries = reinterpret_cast<PFNGLDELETEQUERIES>(
            gapic::GetGfxProcAddress("glDeleteQueries", false));
    glIsQuery = reinterpret_cast<PFNGLISQUERY>(gapic::GetGfxProcAddress("glIsQuery", false));
    glGetQueryiv =
            reinterpret_cast<PFNGLGETQUERYIV>(gapic::GetGfxProcAddress("glGetQueryiv", false));
    glGetQueryObjectuiv = reinterpret_cast<PFNGLGETQUERYOBJECTUIV>(
            gapic::GetGfxProcAddress("glGetQueryObjectuiv", false));
    glGetActiveUniformBlockName = reinterpret_cast<PFNGLGETACTIVEUNIFORMBLOCKNAME>(
            gapic::GetGfxProcAddress("glGetActiveUniformBlockName", false));
    glGetActiveUniformBlockiv = reinterpret_cast<PFNGLGETACTIVEUNIFORMBLOCKIV>(
            gapic::GetGfxProcAddress("glGetActiveUniformBlockiv", false));
    glUniformBlockBinding = reinterpret_cast<PFNGLUNIFORMBLOCKBINDING>(
            gapic::GetGfxProcAddress("glUniformBlockBinding", false));
    glGetActiveUniformsiv = reinterpret_cast<PFNGLGETACTIVEUNIFORMSIV>(
            gapic::GetGfxProcAddress("glGetActiveUniformsiv", false));
    glBindBufferBase = reinterpret_cast<PFNGLBINDBUFFERBASE>(
            gapic::GetGfxProcAddress("glBindBufferBase", false));
    glGenVertexArrays = reinterpret_cast<PFNGLGENVERTEXARRAYS>(
            gapic::GetGfxProcAddress("glGenVertexArrays", false));
    glBindVertexArray = reinterpret_cast<PFNGLBINDVERTEXARRAY>(
            gapic::GetGfxProcAddress("glBindVertexArray", false));
    glDeleteVertexArrays = reinterpret_cast<PFNGLDELETEVERTEXARRAYS>(
            gapic::GetGfxProcAddress("glDeleteVertexArrays", false));
    glGetQueryObjecti64v = reinterpret_cast<PFNGLGETQUERYOBJECTI64V>(
            gapic::GetGfxProcAddress("glGetQueryObjecti64v", false));
    glGetQueryObjectui64v = reinterpret_cast<PFNGLGETQUERYOBJECTUI64V>(
            gapic::GetGfxProcAddress("glGetQueryObjectui64v", false));
    glGenQueriesEXT = reinterpret_cast<PFNGLGENQUERIESEXT>(
            gapic::GetGfxProcAddress("glGenQueriesEXT", false));
    glBeginQueryEXT = reinterpret_cast<PFNGLBEGINQUERYEXT>(
            gapic::GetGfxProcAddress("glBeginQueryEXT", false));
    glEndQueryEXT =
            reinterpret_cast<PFNGLENDQUERYEXT>(gapic::GetGfxProcAddress("glEndQueryEXT", false));
    glDeleteQueriesEXT = reinterpret_cast<PFNGLDELETEQUERIESEXT>(
            gapic::GetGfxProcAddress("glDeleteQueriesEXT", false));
    glIsQueryEXT =
            reinterpret_cast<PFNGLISQUERYEXT>(gapic::GetGfxProcAddress("glIsQueryEXT", false));
    glQueryCounterEXT = reinterpret_cast<PFNGLQUERYCOUNTEREXT>(
            gapic::GetGfxProcAddress("glQueryCounterEXT", false));
    glGetQueryivEXT = reinterpret_cast<PFNGLGETQUERYIVEXT>(
            gapic::GetGfxProcAddress("glGetQueryivEXT", false));
    glGetQueryObjectivEXT = reinterpret_cast<PFNGLGETQUERYOBJECTIVEXT>(
            gapic::GetGfxProcAddress("glGetQueryObjectivEXT", false));
    glGetQueryObjectuivEXT = reinterpret_cast<PFNGLGETQUERYOBJECTUIVEXT>(
            gapic::GetGfxProcAddress("glGetQueryObjectuivEXT", false));
    glGetQueryObjecti64vEXT = reinterpret_cast<PFNGLGETQUERYOBJECTI64VEXT>(
            gapic::GetGfxProcAddress("glGetQueryObjecti64vEXT", false));
    glGetQueryObjectui64vEXT = reinterpret_cast<PFNGLGETQUERYOBJECTUI64VEXT>(
            gapic::GetGfxProcAddress("glGetQueryObjectui64vEXT", false));
}

}  // namespace gfxapi
}  // namespace gapir
