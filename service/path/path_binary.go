////////////////////////////////////////////////////////////////////////////////
// Do not modify!
// Generated by codergen
////////////////////////////////////////////////////////////////////////////////

package path

import (
	"android.googlesource.com/platform/tools/gpu/binary"
	"android.googlesource.com/platform/tools/gpu/binary/any"
	"android.googlesource.com/platform/tools/gpu/binary/registry"
	"android.googlesource.com/platform/tools/gpu/binary/schema"
)

var Namespace = registry.NewNamespace()

func init() {
	registry.Global.AddFallbacks(Namespace)
	Namespace.Add((*ArrayIndex)(nil).Class())
	Namespace.Add((*Capture)(nil).Class())
	Namespace.Add((*Atoms)(nil).Class())
	Namespace.Add((*Atom)(nil).Class())
	Namespace.Add((*Blob)(nil).Class())
	Namespace.Add((*Device)(nil).Class())
	Namespace.Add((*Field)(nil).Class())
	Namespace.Add((*Hierarchy)(nil).Class())
	Namespace.Add((*ImageInfo)(nil).Class())
	Namespace.Add((*MapIndex)(nil).Class())
	Namespace.Add((*MemoryRange)(nil).Class())
	Namespace.Add((*Report)(nil).Class())
	Namespace.Add((*Slice)(nil).Class())
	Namespace.Add((*State)(nil).Class())
	Namespace.Add((*TimingInfo)(nil).Class())
}

var (
	binaryIDArrayIndex  = binary.ID{0x70, 0x45, 0xaf, 0xd6, 0x00, 0x20, 0x4c, 0x2c, 0x72, 0x36, 0x74, 0xc3, 0x2e, 0x1e, 0xb9, 0xf5, 0x4b, 0xe6, 0x68, 0xfb}
	binaryIDCapture     = binary.ID{0x0e, 0x9b, 0xe3, 0x62, 0x7b, 0x67, 0x5b, 0xe0, 0x44, 0x7b, 0x7b, 0x21, 0xa1, 0x50, 0x6d, 0x1b, 0xfb, 0xc7, 0x47, 0xe6}
	binaryIDAtoms       = binary.ID{0x17, 0x6b, 0x7c, 0x08, 0xc2, 0x36, 0x2c, 0xe3, 0x1b, 0x17, 0xd5, 0x3a, 0x0a, 0x98, 0x5b, 0x76, 0xc5, 0x1d, 0x01, 0xa7}
	binaryIDAtom        = binary.ID{0x58, 0x11, 0xbe, 0x6d, 0xfc, 0xe2, 0xe0, 0x12, 0x6c, 0x42, 0x55, 0x2d, 0xf3, 0x5e, 0x11, 0x1a, 0xc1, 0x6b, 0xfe, 0x3b}
	binaryIDBlob        = binary.ID{0xc4, 0x1a, 0xa9, 0xab, 0x6b, 0x4f, 0x1b, 0xf8, 0x21, 0x87, 0xdc, 0x25, 0x6a, 0x17, 0x4f, 0x6f, 0xc8, 0x1a, 0x61, 0xd7}
	binaryIDDevice      = binary.ID{0xe6, 0xca, 0x6e, 0x0d, 0xa7, 0xca, 0xb0, 0xcb, 0xd5, 0x9e, 0x18, 0xa1, 0xcb, 0x83, 0xc8, 0x93, 0xef, 0xec, 0x12, 0xc0}
	binaryIDField       = binary.ID{0xd2, 0x4f, 0x7f, 0x64, 0xec, 0x81, 0x92, 0x06, 0x6c, 0x25, 0x60, 0xfa, 0x5a, 0x0b, 0x9c, 0x6e, 0x73, 0xf7, 0x4b, 0x4c}
	binaryIDHierarchy   = binary.ID{0x9f, 0x69, 0x5b, 0x87, 0x7f, 0x8b, 0x5c, 0xa3, 0xaa, 0x9d, 0xb4, 0x6e, 0xa9, 0x39, 0x2f, 0xd5, 0x77, 0x92, 0x8a, 0xad}
	binaryIDImageInfo   = binary.ID{0x25, 0x45, 0x6f, 0xb8, 0x75, 0x8a, 0x10, 0x97, 0x84, 0x5d, 0x5a, 0x6e, 0xa0, 0xe0, 0xe1, 0x2f, 0x2c, 0x8e, 0x55, 0xcb}
	binaryIDMapIndex    = binary.ID{0x0d, 0x46, 0x56, 0xf3, 0x1d, 0xba, 0xf9, 0xd8, 0x5e, 0xcf, 0xcc, 0x0e, 0x84, 0x93, 0x38, 0x5b, 0xbb, 0xd2, 0xec, 0xde}
	binaryIDMemoryRange = binary.ID{0x68, 0x6d, 0x8f, 0xe1, 0x8b, 0x34, 0x52, 0xbf, 0xaa, 0x0b, 0x3c, 0xe0, 0x08, 0xe6, 0xd7, 0x06, 0x6e, 0x60, 0x09, 0xdd}
	binaryIDReport      = binary.ID{0xc3, 0x15, 0x30, 0x12, 0xb4, 0xa6, 0x7e, 0x71, 0x3a, 0xa3, 0xec, 0xb5, 0x93, 0x21, 0xf6, 0x2f, 0xd2, 0xf1, 0x4f, 0xa9}
	binaryIDSlice       = binary.ID{0xd2, 0x2a, 0x0c, 0x1e, 0x91, 0x2e, 0x6b, 0x8d, 0xc1, 0xde, 0x05, 0xf2, 0x17, 0x1e, 0xf4, 0x42, 0x3b, 0x12, 0xd9, 0x76}
	binaryIDState       = binary.ID{0xf1, 0xa4, 0x19, 0x51, 0x01, 0xc4, 0xe2, 0x90, 0xc2, 0xca, 0x28, 0x00, 0x17, 0x08, 0x72, 0xb9, 0x46, 0x3a, 0xd1, 0x7b}
	binaryIDTimingInfo  = binary.ID{0xad, 0xc7, 0xb9, 0x1b, 0xaa, 0x9c, 0x98, 0xc2, 0x28, 0xb0, 0xdc, 0x3a, 0x6f, 0xdb, 0xa8, 0xff, 0x9c, 0xb6, 0x9a, 0x89}
)

type binaryClassArrayIndex struct{}

func (*ArrayIndex) Class() binary.Class {
	return (*binaryClassArrayIndex)(nil)
}
func doEncodeArrayIndex(e binary.Encoder, o *ArrayIndex) error {
	if o.Array != nil {
		if err := e.Object(o.Array); err != nil {
			return err
		}
	} else if err := e.Object(nil); err != nil {
		return err
	}
	if err := e.Uint64(o.Index); err != nil {
		return err
	}
	return nil
}
func doDecodeArrayIndex(d binary.Decoder, o *ArrayIndex) error {
	if obj, err := d.Object(); err != nil {
		return err
	} else if obj != nil {
		o.Array = obj.(Path)
	} else {
		o.Array = nil
	}
	if obj, err := d.Uint64(); err != nil {
		return err
	} else {
		o.Index = uint64(obj)
	}
	return nil
}
func doSkipArrayIndex(d binary.Decoder) error {
	if _, err := d.SkipObject(); err != nil {
		return err
	}
	if _, err := d.Uint64(); err != nil {
		return err
	}
	return nil
}
func (*binaryClassArrayIndex) ID() binary.ID      { return binaryIDArrayIndex }
func (*binaryClassArrayIndex) New() binary.Object { return &ArrayIndex{} }
func (*binaryClassArrayIndex) Encode(e binary.Encoder, obj binary.Object) error {
	return doEncodeArrayIndex(e, obj.(*ArrayIndex))
}
func (*binaryClassArrayIndex) Decode(d binary.Decoder) (binary.Object, error) {
	obj := &ArrayIndex{}
	return obj, doDecodeArrayIndex(d, obj)
}
func (*binaryClassArrayIndex) DecodeTo(d binary.Decoder, obj binary.Object) error {
	return doDecodeArrayIndex(d, obj.(*ArrayIndex))
}
func (*binaryClassArrayIndex) Skip(d binary.Decoder) error { return doSkipArrayIndex(d) }
func (*binaryClassArrayIndex) Schema() *schema.Class       { return schemaArrayIndex }

var schemaArrayIndex = &schema.Class{
	TypeID:  binaryIDArrayIndex,
	Package: "path",
	Name:    "ArrayIndex",
	Fields: []schema.Field{
		{Declared: "Array", Type: &schema.Interface{Name: "Path"}},
		{Declared: "Index", Type: &schema.Primitive{Name: "uint64", Method: schema.Uint64}},
	},
}

type binaryClassCapture struct{}

func (*Capture) Class() binary.Class {
	return (*binaryClassCapture)(nil)
}
func doEncodeCapture(e binary.Encoder, o *Capture) error {
	if err := e.ID(o.ID); err != nil {
		return err
	}
	return nil
}
func doDecodeCapture(d binary.Decoder, o *Capture) error {
	if obj, err := d.ID(); err != nil {
		return err
	} else {
		o.ID = binary.ID(obj)
	}
	return nil
}
func doSkipCapture(d binary.Decoder) error {
	if err := d.SkipID(); err != nil {
		return err
	}
	return nil
}
func (*binaryClassCapture) ID() binary.ID      { return binaryIDCapture }
func (*binaryClassCapture) New() binary.Object { return &Capture{} }
func (*binaryClassCapture) Encode(e binary.Encoder, obj binary.Object) error {
	return doEncodeCapture(e, obj.(*Capture))
}
func (*binaryClassCapture) Decode(d binary.Decoder) (binary.Object, error) {
	obj := &Capture{}
	return obj, doDecodeCapture(d, obj)
}
func (*binaryClassCapture) DecodeTo(d binary.Decoder, obj binary.Object) error {
	return doDecodeCapture(d, obj.(*Capture))
}
func (*binaryClassCapture) Skip(d binary.Decoder) error { return doSkipCapture(d) }
func (*binaryClassCapture) Schema() *schema.Class       { return schemaCapture }

var schemaCapture = &schema.Class{
	TypeID:  binaryIDCapture,
	Package: "path",
	Name:    "Capture",
	Fields: []schema.Field{
		{Declared: "ID", Type: &schema.Primitive{Name: "binary.ID", Method: schema.ID}},
	},
}

type binaryClassAtoms struct{}

func (*Atoms) Class() binary.Class {
	return (*binaryClassAtoms)(nil)
}
func doEncodeAtoms(e binary.Encoder, o *Atoms) error {
	if o.Capture != nil {
		if err := e.Object(o.Capture); err != nil {
			return err
		}
	} else if err := e.Object(nil); err != nil {
		return err
	}
	return nil
}
func doDecodeAtoms(d binary.Decoder, o *Atoms) error {
	if obj, err := d.Object(); err != nil {
		return err
	} else if obj != nil {
		o.Capture = obj.(*Capture)
	} else {
		o.Capture = nil
	}
	return nil
}
func doSkipAtoms(d binary.Decoder) error {
	if _, err := d.SkipObject(); err != nil {
		return err
	}
	return nil
}
func (*binaryClassAtoms) ID() binary.ID      { return binaryIDAtoms }
func (*binaryClassAtoms) New() binary.Object { return &Atoms{} }
func (*binaryClassAtoms) Encode(e binary.Encoder, obj binary.Object) error {
	return doEncodeAtoms(e, obj.(*Atoms))
}
func (*binaryClassAtoms) Decode(d binary.Decoder) (binary.Object, error) {
	obj := &Atoms{}
	return obj, doDecodeAtoms(d, obj)
}
func (*binaryClassAtoms) DecodeTo(d binary.Decoder, obj binary.Object) error {
	return doDecodeAtoms(d, obj.(*Atoms))
}
func (*binaryClassAtoms) Skip(d binary.Decoder) error { return doSkipAtoms(d) }
func (*binaryClassAtoms) Schema() *schema.Class       { return schemaAtoms }

var schemaAtoms = &schema.Class{
	TypeID:  binaryIDAtoms,
	Package: "path",
	Name:    "Atoms",
	Fields: []schema.Field{
		{Declared: "Capture", Type: &schema.Pointer{Type: &schema.Struct{Name: "Capture", ID: (*Capture)(nil).Class().ID()}}},
	},
}

type binaryClassAtom struct{}

func (*Atom) Class() binary.Class {
	return (*binaryClassAtom)(nil)
}
func doEncodeAtom(e binary.Encoder, o *Atom) error {
	if o.Atoms != nil {
		if err := e.Object(o.Atoms); err != nil {
			return err
		}
	} else if err := e.Object(nil); err != nil {
		return err
	}
	if err := e.Uint64(o.Index); err != nil {
		return err
	}
	return nil
}
func doDecodeAtom(d binary.Decoder, o *Atom) error {
	if obj, err := d.Object(); err != nil {
		return err
	} else if obj != nil {
		o.Atoms = obj.(*Atoms)
	} else {
		o.Atoms = nil
	}
	if obj, err := d.Uint64(); err != nil {
		return err
	} else {
		o.Index = uint64(obj)
	}
	return nil
}
func doSkipAtom(d binary.Decoder) error {
	if _, err := d.SkipObject(); err != nil {
		return err
	}
	if _, err := d.Uint64(); err != nil {
		return err
	}
	return nil
}
func (*binaryClassAtom) ID() binary.ID      { return binaryIDAtom }
func (*binaryClassAtom) New() binary.Object { return &Atom{} }
func (*binaryClassAtom) Encode(e binary.Encoder, obj binary.Object) error {
	return doEncodeAtom(e, obj.(*Atom))
}
func (*binaryClassAtom) Decode(d binary.Decoder) (binary.Object, error) {
	obj := &Atom{}
	return obj, doDecodeAtom(d, obj)
}
func (*binaryClassAtom) DecodeTo(d binary.Decoder, obj binary.Object) error {
	return doDecodeAtom(d, obj.(*Atom))
}
func (*binaryClassAtom) Skip(d binary.Decoder) error { return doSkipAtom(d) }
func (*binaryClassAtom) Schema() *schema.Class       { return schemaAtom }

var schemaAtom = &schema.Class{
	TypeID:  binaryIDAtom,
	Package: "path",
	Name:    "Atom",
	Fields: []schema.Field{
		{Declared: "Atoms", Type: &schema.Pointer{Type: &schema.Struct{Name: "Atoms", ID: (*Atoms)(nil).Class().ID()}}},
		{Declared: "Index", Type: &schema.Primitive{Name: "uint64", Method: schema.Uint64}},
	},
}

type binaryClassBlob struct{}

func (*Blob) Class() binary.Class {
	return (*binaryClassBlob)(nil)
}
func doEncodeBlob(e binary.Encoder, o *Blob) error {
	if err := e.ID(o.ID); err != nil {
		return err
	}
	return nil
}
func doDecodeBlob(d binary.Decoder, o *Blob) error {
	if obj, err := d.ID(); err != nil {
		return err
	} else {
		o.ID = binary.ID(obj)
	}
	return nil
}
func doSkipBlob(d binary.Decoder) error {
	if err := d.SkipID(); err != nil {
		return err
	}
	return nil
}
func (*binaryClassBlob) ID() binary.ID      { return binaryIDBlob }
func (*binaryClassBlob) New() binary.Object { return &Blob{} }
func (*binaryClassBlob) Encode(e binary.Encoder, obj binary.Object) error {
	return doEncodeBlob(e, obj.(*Blob))
}
func (*binaryClassBlob) Decode(d binary.Decoder) (binary.Object, error) {
	obj := &Blob{}
	return obj, doDecodeBlob(d, obj)
}
func (*binaryClassBlob) DecodeTo(d binary.Decoder, obj binary.Object) error {
	return doDecodeBlob(d, obj.(*Blob))
}
func (*binaryClassBlob) Skip(d binary.Decoder) error { return doSkipBlob(d) }
func (*binaryClassBlob) Schema() *schema.Class       { return schemaBlob }

var schemaBlob = &schema.Class{
	TypeID:  binaryIDBlob,
	Package: "path",
	Name:    "Blob",
	Fields: []schema.Field{
		{Declared: "ID", Type: &schema.Primitive{Name: "binary.ID", Method: schema.ID}},
	},
}

type binaryClassDevice struct{}

func (*Device) Class() binary.Class {
	return (*binaryClassDevice)(nil)
}
func doEncodeDevice(e binary.Encoder, o *Device) error {
	if err := e.ID(o.ID); err != nil {
		return err
	}
	return nil
}
func doDecodeDevice(d binary.Decoder, o *Device) error {
	if obj, err := d.ID(); err != nil {
		return err
	} else {
		o.ID = binary.ID(obj)
	}
	return nil
}
func doSkipDevice(d binary.Decoder) error {
	if err := d.SkipID(); err != nil {
		return err
	}
	return nil
}
func (*binaryClassDevice) ID() binary.ID      { return binaryIDDevice }
func (*binaryClassDevice) New() binary.Object { return &Device{} }
func (*binaryClassDevice) Encode(e binary.Encoder, obj binary.Object) error {
	return doEncodeDevice(e, obj.(*Device))
}
func (*binaryClassDevice) Decode(d binary.Decoder) (binary.Object, error) {
	obj := &Device{}
	return obj, doDecodeDevice(d, obj)
}
func (*binaryClassDevice) DecodeTo(d binary.Decoder, obj binary.Object) error {
	return doDecodeDevice(d, obj.(*Device))
}
func (*binaryClassDevice) Skip(d binary.Decoder) error { return doSkipDevice(d) }
func (*binaryClassDevice) Schema() *schema.Class       { return schemaDevice }

var schemaDevice = &schema.Class{
	TypeID:  binaryIDDevice,
	Package: "path",
	Name:    "Device",
	Fields: []schema.Field{
		{Declared: "ID", Type: &schema.Primitive{Name: "binary.ID", Method: schema.ID}},
	},
}

type binaryClassField struct{}

func (*Field) Class() binary.Class {
	return (*binaryClassField)(nil)
}
func doEncodeField(e binary.Encoder, o *Field) error {
	if o.Struct != nil {
		if err := e.Object(o.Struct); err != nil {
			return err
		}
	} else if err := e.Object(nil); err != nil {
		return err
	}
	if err := e.String(o.Name); err != nil {
		return err
	}
	return nil
}
func doDecodeField(d binary.Decoder, o *Field) error {
	if obj, err := d.Object(); err != nil {
		return err
	} else if obj != nil {
		o.Struct = obj.(Path)
	} else {
		o.Struct = nil
	}
	if obj, err := d.String(); err != nil {
		return err
	} else {
		o.Name = string(obj)
	}
	return nil
}
func doSkipField(d binary.Decoder) error {
	if _, err := d.SkipObject(); err != nil {
		return err
	}
	if err := d.SkipString(); err != nil {
		return err
	}
	return nil
}
func (*binaryClassField) ID() binary.ID      { return binaryIDField }
func (*binaryClassField) New() binary.Object { return &Field{} }
func (*binaryClassField) Encode(e binary.Encoder, obj binary.Object) error {
	return doEncodeField(e, obj.(*Field))
}
func (*binaryClassField) Decode(d binary.Decoder) (binary.Object, error) {
	obj := &Field{}
	return obj, doDecodeField(d, obj)
}
func (*binaryClassField) DecodeTo(d binary.Decoder, obj binary.Object) error {
	return doDecodeField(d, obj.(*Field))
}
func (*binaryClassField) Skip(d binary.Decoder) error { return doSkipField(d) }
func (*binaryClassField) Schema() *schema.Class       { return schemaField }

var schemaField = &schema.Class{
	TypeID:  binaryIDField,
	Package: "path",
	Name:    "Field",
	Fields: []schema.Field{
		{Declared: "Struct", Type: &schema.Interface{Name: "Path"}},
		{Declared: "Name", Type: &schema.Primitive{Name: "string", Method: schema.String}},
	},
}

type binaryClassHierarchy struct{}

func (*Hierarchy) Class() binary.Class {
	return (*binaryClassHierarchy)(nil)
}
func doEncodeHierarchy(e binary.Encoder, o *Hierarchy) error {
	if o.Capture != nil {
		if err := e.Object(o.Capture); err != nil {
			return err
		}
	} else if err := e.Object(nil); err != nil {
		return err
	}
	return nil
}
func doDecodeHierarchy(d binary.Decoder, o *Hierarchy) error {
	if obj, err := d.Object(); err != nil {
		return err
	} else if obj != nil {
		o.Capture = obj.(*Capture)
	} else {
		o.Capture = nil
	}
	return nil
}
func doSkipHierarchy(d binary.Decoder) error {
	if _, err := d.SkipObject(); err != nil {
		return err
	}
	return nil
}
func (*binaryClassHierarchy) ID() binary.ID      { return binaryIDHierarchy }
func (*binaryClassHierarchy) New() binary.Object { return &Hierarchy{} }
func (*binaryClassHierarchy) Encode(e binary.Encoder, obj binary.Object) error {
	return doEncodeHierarchy(e, obj.(*Hierarchy))
}
func (*binaryClassHierarchy) Decode(d binary.Decoder) (binary.Object, error) {
	obj := &Hierarchy{}
	return obj, doDecodeHierarchy(d, obj)
}
func (*binaryClassHierarchy) DecodeTo(d binary.Decoder, obj binary.Object) error {
	return doDecodeHierarchy(d, obj.(*Hierarchy))
}
func (*binaryClassHierarchy) Skip(d binary.Decoder) error { return doSkipHierarchy(d) }
func (*binaryClassHierarchy) Schema() *schema.Class       { return schemaHierarchy }

var schemaHierarchy = &schema.Class{
	TypeID:  binaryIDHierarchy,
	Package: "path",
	Name:    "Hierarchy",
	Fields: []schema.Field{
		{Declared: "Capture", Type: &schema.Pointer{Type: &schema.Struct{Name: "Capture", ID: (*Capture)(nil).Class().ID()}}},
	},
}

type binaryClassImageInfo struct{}

func (*ImageInfo) Class() binary.Class {
	return (*binaryClassImageInfo)(nil)
}
func doEncodeImageInfo(e binary.Encoder, o *ImageInfo) error {
	if err := e.ID(o.ID); err != nil {
		return err
	}
	return nil
}
func doDecodeImageInfo(d binary.Decoder, o *ImageInfo) error {
	if obj, err := d.ID(); err != nil {
		return err
	} else {
		o.ID = binary.ID(obj)
	}
	return nil
}
func doSkipImageInfo(d binary.Decoder) error {
	if err := d.SkipID(); err != nil {
		return err
	}
	return nil
}
func (*binaryClassImageInfo) ID() binary.ID      { return binaryIDImageInfo }
func (*binaryClassImageInfo) New() binary.Object { return &ImageInfo{} }
func (*binaryClassImageInfo) Encode(e binary.Encoder, obj binary.Object) error {
	return doEncodeImageInfo(e, obj.(*ImageInfo))
}
func (*binaryClassImageInfo) Decode(d binary.Decoder) (binary.Object, error) {
	obj := &ImageInfo{}
	return obj, doDecodeImageInfo(d, obj)
}
func (*binaryClassImageInfo) DecodeTo(d binary.Decoder, obj binary.Object) error {
	return doDecodeImageInfo(d, obj.(*ImageInfo))
}
func (*binaryClassImageInfo) Skip(d binary.Decoder) error { return doSkipImageInfo(d) }
func (*binaryClassImageInfo) Schema() *schema.Class       { return schemaImageInfo }

var schemaImageInfo = &schema.Class{
	TypeID:  binaryIDImageInfo,
	Package: "path",
	Name:    "ImageInfo",
	Fields: []schema.Field{
		{Declared: "ID", Type: &schema.Primitive{Name: "binary.ID", Method: schema.ID}},
	},
}

type binaryClassMapIndex struct{}

func (*MapIndex) Class() binary.Class {
	return (*binaryClassMapIndex)(nil)
}
func doEncodeMapIndex(e binary.Encoder, o *MapIndex) error {
	if o.Map != nil {
		if err := e.Object(o.Map); err != nil {
			return err
		}
	} else if err := e.Object(nil); err != nil {
		return err
	}
	if o.Key != nil {
		var boxed binary.Object
		boxed, err := any.Box(o.Key)
		if err != nil {
			return err
		}
		if err := e.Variant(boxed); err != nil {
			return err
		}
	} else if err := e.Variant(nil); err != nil {
		return err
	}
	return nil
}
func doDecodeMapIndex(d binary.Decoder, o *MapIndex) error {
	if obj, err := d.Object(); err != nil {
		return err
	} else if obj != nil {
		o.Map = obj.(Path)
	} else {
		o.Map = nil
	}
	if boxed, err := d.Variant(); err != nil {
		return err
	} else if boxed != nil {
		if o.Key, err = any.Unbox(boxed); err != nil {
			return err
		}
	} else {
		o.Key = nil
	}
	return nil
}
func doSkipMapIndex(d binary.Decoder) error {
	if _, err := d.SkipObject(); err != nil {
		return err
	}
	if _, err := d.SkipVariant(); err != nil {
		return err
	}
	return nil
}
func (*binaryClassMapIndex) ID() binary.ID      { return binaryIDMapIndex }
func (*binaryClassMapIndex) New() binary.Object { return &MapIndex{} }
func (*binaryClassMapIndex) Encode(e binary.Encoder, obj binary.Object) error {
	return doEncodeMapIndex(e, obj.(*MapIndex))
}
func (*binaryClassMapIndex) Decode(d binary.Decoder) (binary.Object, error) {
	obj := &MapIndex{}
	return obj, doDecodeMapIndex(d, obj)
}
func (*binaryClassMapIndex) DecodeTo(d binary.Decoder, obj binary.Object) error {
	return doDecodeMapIndex(d, obj.(*MapIndex))
}
func (*binaryClassMapIndex) Skip(d binary.Decoder) error { return doSkipMapIndex(d) }
func (*binaryClassMapIndex) Schema() *schema.Class       { return schemaMapIndex }

var schemaMapIndex = &schema.Class{
	TypeID:  binaryIDMapIndex,
	Package: "path",
	Name:    "MapIndex",
	Fields: []schema.Field{
		{Declared: "Map", Type: &schema.Interface{Name: "Path"}},
		{Declared: "Key", Type: &any.Any{}},
	},
}

type binaryClassMemoryRange struct{}

func (*MemoryRange) Class() binary.Class {
	return (*binaryClassMemoryRange)(nil)
}
func doEncodeMemoryRange(e binary.Encoder, o *MemoryRange) error {
	if o.After != nil {
		if err := e.Object(o.After); err != nil {
			return err
		}
	} else if err := e.Object(nil); err != nil {
		return err
	}
	if err := e.Uint64(o.Pool); err != nil {
		return err
	}
	if err := e.Uint64(o.Address); err != nil {
		return err
	}
	if err := e.Uint64(o.Size); err != nil {
		return err
	}
	return nil
}
func doDecodeMemoryRange(d binary.Decoder, o *MemoryRange) error {
	if obj, err := d.Object(); err != nil {
		return err
	} else if obj != nil {
		o.After = obj.(*Atom)
	} else {
		o.After = nil
	}
	if obj, err := d.Uint64(); err != nil {
		return err
	} else {
		o.Pool = uint64(obj)
	}
	if obj, err := d.Uint64(); err != nil {
		return err
	} else {
		o.Address = uint64(obj)
	}
	if obj, err := d.Uint64(); err != nil {
		return err
	} else {
		o.Size = uint64(obj)
	}
	return nil
}
func doSkipMemoryRange(d binary.Decoder) error {
	if _, err := d.SkipObject(); err != nil {
		return err
	}
	if _, err := d.Uint64(); err != nil {
		return err
	}
	if _, err := d.Uint64(); err != nil {
		return err
	}
	if _, err := d.Uint64(); err != nil {
		return err
	}
	return nil
}
func (*binaryClassMemoryRange) ID() binary.ID      { return binaryIDMemoryRange }
func (*binaryClassMemoryRange) New() binary.Object { return &MemoryRange{} }
func (*binaryClassMemoryRange) Encode(e binary.Encoder, obj binary.Object) error {
	return doEncodeMemoryRange(e, obj.(*MemoryRange))
}
func (*binaryClassMemoryRange) Decode(d binary.Decoder) (binary.Object, error) {
	obj := &MemoryRange{}
	return obj, doDecodeMemoryRange(d, obj)
}
func (*binaryClassMemoryRange) DecodeTo(d binary.Decoder, obj binary.Object) error {
	return doDecodeMemoryRange(d, obj.(*MemoryRange))
}
func (*binaryClassMemoryRange) Skip(d binary.Decoder) error { return doSkipMemoryRange(d) }
func (*binaryClassMemoryRange) Schema() *schema.Class       { return schemaMemoryRange }

var schemaMemoryRange = &schema.Class{
	TypeID:  binaryIDMemoryRange,
	Package: "path",
	Name:    "MemoryRange",
	Fields: []schema.Field{
		{Declared: "After", Type: &schema.Pointer{Type: &schema.Struct{Name: "Atom", ID: (*Atom)(nil).Class().ID()}}},
		{Declared: "Pool", Type: &schema.Primitive{Name: "uint64", Method: schema.Uint64}},
		{Declared: "Address", Type: &schema.Primitive{Name: "uint64", Method: schema.Uint64}},
		{Declared: "Size", Type: &schema.Primitive{Name: "uint64", Method: schema.Uint64}},
	},
}

type binaryClassReport struct{}

func (*Report) Class() binary.Class {
	return (*binaryClassReport)(nil)
}
func doEncodeReport(e binary.Encoder, o *Report) error {
	if o.Capture != nil {
		if err := e.Object(o.Capture); err != nil {
			return err
		}
	} else if err := e.Object(nil); err != nil {
		return err
	}
	return nil
}
func doDecodeReport(d binary.Decoder, o *Report) error {
	if obj, err := d.Object(); err != nil {
		return err
	} else if obj != nil {
		o.Capture = obj.(*Capture)
	} else {
		o.Capture = nil
	}
	return nil
}
func doSkipReport(d binary.Decoder) error {
	if _, err := d.SkipObject(); err != nil {
		return err
	}
	return nil
}
func (*binaryClassReport) ID() binary.ID      { return binaryIDReport }
func (*binaryClassReport) New() binary.Object { return &Report{} }
func (*binaryClassReport) Encode(e binary.Encoder, obj binary.Object) error {
	return doEncodeReport(e, obj.(*Report))
}
func (*binaryClassReport) Decode(d binary.Decoder) (binary.Object, error) {
	obj := &Report{}
	return obj, doDecodeReport(d, obj)
}
func (*binaryClassReport) DecodeTo(d binary.Decoder, obj binary.Object) error {
	return doDecodeReport(d, obj.(*Report))
}
func (*binaryClassReport) Skip(d binary.Decoder) error { return doSkipReport(d) }
func (*binaryClassReport) Schema() *schema.Class       { return schemaReport }

var schemaReport = &schema.Class{
	TypeID:  binaryIDReport,
	Package: "path",
	Name:    "Report",
	Fields: []schema.Field{
		{Declared: "Capture", Type: &schema.Pointer{Type: &schema.Struct{Name: "Capture", ID: (*Capture)(nil).Class().ID()}}},
	},
}

type binaryClassSlice struct{}

func (*Slice) Class() binary.Class {
	return (*binaryClassSlice)(nil)
}
func doEncodeSlice(e binary.Encoder, o *Slice) error {
	if o.Array != nil {
		if err := e.Object(o.Array); err != nil {
			return err
		}
	} else if err := e.Object(nil); err != nil {
		return err
	}
	if err := e.Uint64(o.Start); err != nil {
		return err
	}
	if err := e.Uint64(o.End); err != nil {
		return err
	}
	return nil
}
func doDecodeSlice(d binary.Decoder, o *Slice) error {
	if obj, err := d.Object(); err != nil {
		return err
	} else if obj != nil {
		o.Array = obj.(Path)
	} else {
		o.Array = nil
	}
	if obj, err := d.Uint64(); err != nil {
		return err
	} else {
		o.Start = uint64(obj)
	}
	if obj, err := d.Uint64(); err != nil {
		return err
	} else {
		o.End = uint64(obj)
	}
	return nil
}
func doSkipSlice(d binary.Decoder) error {
	if _, err := d.SkipObject(); err != nil {
		return err
	}
	if _, err := d.Uint64(); err != nil {
		return err
	}
	if _, err := d.Uint64(); err != nil {
		return err
	}
	return nil
}
func (*binaryClassSlice) ID() binary.ID      { return binaryIDSlice }
func (*binaryClassSlice) New() binary.Object { return &Slice{} }
func (*binaryClassSlice) Encode(e binary.Encoder, obj binary.Object) error {
	return doEncodeSlice(e, obj.(*Slice))
}
func (*binaryClassSlice) Decode(d binary.Decoder) (binary.Object, error) {
	obj := &Slice{}
	return obj, doDecodeSlice(d, obj)
}
func (*binaryClassSlice) DecodeTo(d binary.Decoder, obj binary.Object) error {
	return doDecodeSlice(d, obj.(*Slice))
}
func (*binaryClassSlice) Skip(d binary.Decoder) error { return doSkipSlice(d) }
func (*binaryClassSlice) Schema() *schema.Class       { return schemaSlice }

var schemaSlice = &schema.Class{
	TypeID:  binaryIDSlice,
	Package: "path",
	Name:    "Slice",
	Fields: []schema.Field{
		{Declared: "Array", Type: &schema.Interface{Name: "Path"}},
		{Declared: "Start", Type: &schema.Primitive{Name: "uint64", Method: schema.Uint64}},
		{Declared: "End", Type: &schema.Primitive{Name: "uint64", Method: schema.Uint64}},
	},
}

type binaryClassState struct{}

func (*State) Class() binary.Class {
	return (*binaryClassState)(nil)
}
func doEncodeState(e binary.Encoder, o *State) error {
	if o.After != nil {
		if err := e.Object(o.After); err != nil {
			return err
		}
	} else if err := e.Object(nil); err != nil {
		return err
	}
	return nil
}
func doDecodeState(d binary.Decoder, o *State) error {
	if obj, err := d.Object(); err != nil {
		return err
	} else if obj != nil {
		o.After = obj.(*Atom)
	} else {
		o.After = nil
	}
	return nil
}
func doSkipState(d binary.Decoder) error {
	if _, err := d.SkipObject(); err != nil {
		return err
	}
	return nil
}
func (*binaryClassState) ID() binary.ID      { return binaryIDState }
func (*binaryClassState) New() binary.Object { return &State{} }
func (*binaryClassState) Encode(e binary.Encoder, obj binary.Object) error {
	return doEncodeState(e, obj.(*State))
}
func (*binaryClassState) Decode(d binary.Decoder) (binary.Object, error) {
	obj := &State{}
	return obj, doDecodeState(d, obj)
}
func (*binaryClassState) DecodeTo(d binary.Decoder, obj binary.Object) error {
	return doDecodeState(d, obj.(*State))
}
func (*binaryClassState) Skip(d binary.Decoder) error { return doSkipState(d) }
func (*binaryClassState) Schema() *schema.Class       { return schemaState }

var schemaState = &schema.Class{
	TypeID:  binaryIDState,
	Package: "path",
	Name:    "State",
	Fields: []schema.Field{
		{Declared: "After", Type: &schema.Pointer{Type: &schema.Struct{Name: "Atom", ID: (*Atom)(nil).Class().ID()}}},
	},
}

type binaryClassTimingInfo struct{}

func (*TimingInfo) Class() binary.Class {
	return (*binaryClassTimingInfo)(nil)
}
func doEncodeTimingInfo(e binary.Encoder, o *TimingInfo) error {
	if err := e.ID(o.ID); err != nil {
		return err
	}
	return nil
}
func doDecodeTimingInfo(d binary.Decoder, o *TimingInfo) error {
	if obj, err := d.ID(); err != nil {
		return err
	} else {
		o.ID = binary.ID(obj)
	}
	return nil
}
func doSkipTimingInfo(d binary.Decoder) error {
	if err := d.SkipID(); err != nil {
		return err
	}
	return nil
}
func (*binaryClassTimingInfo) ID() binary.ID      { return binaryIDTimingInfo }
func (*binaryClassTimingInfo) New() binary.Object { return &TimingInfo{} }
func (*binaryClassTimingInfo) Encode(e binary.Encoder, obj binary.Object) error {
	return doEncodeTimingInfo(e, obj.(*TimingInfo))
}
func (*binaryClassTimingInfo) Decode(d binary.Decoder) (binary.Object, error) {
	obj := &TimingInfo{}
	return obj, doDecodeTimingInfo(d, obj)
}
func (*binaryClassTimingInfo) DecodeTo(d binary.Decoder, obj binary.Object) error {
	return doDecodeTimingInfo(d, obj.(*TimingInfo))
}
func (*binaryClassTimingInfo) Skip(d binary.Decoder) error { return doSkipTimingInfo(d) }
func (*binaryClassTimingInfo) Schema() *schema.Class       { return schemaTimingInfo }

var schemaTimingInfo = &schema.Class{
	TypeID:  binaryIDTimingInfo,
	Package: "path",
	Name:    "TimingInfo",
	Fields: []schema.Field{
		{Declared: "ID", Type: &schema.Primitive{Name: "binary.ID", Method: schema.ID}},
	},
}
